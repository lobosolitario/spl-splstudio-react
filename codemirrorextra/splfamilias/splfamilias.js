// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: https://codemirror.net/LICENSE

function ModuloLenguajeSplFamilias(tokens,  lenguajeExterno) {
    var modename = "splfamilias";
    let li = "splproductos";
    require("../splproductos/splproductos")
    global.controllerSplProductos(tokens)


    let le = lenguajeExterno;
    //lenguajeExterno = "css"
    //console.log("LenguajeInterno:" + lenguajeinterno + "  LenguajeExterno:" +lenguajeExterno )
    //var lenguajeExterno = "splfamiliasinterno";
    //lenguajeinterno = "";
    // CodeMirror, copyright (c) by Marijn Haverbeke and others
    // Distributed under an MIT license: https://codemirror.net/LICENSE
    // CodeMirror, copyright (c) by Marijn Haverbeke and others
    // Distributed under an MIT license: https://codemirror.net/LICENSE

    (function(mod) {
        if (typeof exports == "object" && typeof module == "object"){
            try{
                if(le == "none"){
                    mod(require("../../lib/codemirror"), 
                    require("../../addon/mode/multiplex"));
                }else{
                    mod(require("../../lib/codemirror"), require("../" + le + "/" + le + ""),
                    require("../../addon/mode/multiplex"));
                }

            }catch(error){

            }

        } // CommonJS
         
        else if (typeof define == "function" && define.amd){ // AMD
            try{
                if(le == "none"){
                    define(["../../addon/mode/multiplex"], mod);
                }else{
                    define(["../../lib/codemirror", "../" + le + "/" + le + "",
                    "../../addon/mode/multiplex"], mod);
                }

            }catch(error){

            }
        }else{ // Plain browser env
          mod(CodeMirror);
        }
      })(function(CodeMirror) {
        "use strict";
         
        

        CodeMirror.defineMode(modename, function(config, parserConfig) {
          var closeComment = parserConfig.closeComment || "--%>"
          return CodeMirror.multiplexingMode(CodeMirror.getMode(config, le), 
          {
            open: parserConfig.openComment || "<%--",
            close: closeComment,
            delimStyle: "comment",
            mode: {token: function(stream) {
              stream.skipTo(closeComment) || stream.skipToEnd()
              return "comment"
            }}
          }, 
          {
            open: parserConfig.open || parserConfig.scriptStartRegex || "<%=" ,
            close: parserConfig.close || parserConfig.scriptEndRegex || "%>",
            mode: CodeMirror.getMode(config, parserConfig.scriptingModeSpec)
          },
          {
            open: parserConfig.open || parserConfig.scriptStartRegex || "<%" ,
            close: parserConfig.close || parserConfig.scriptEndRegex || "%>",
            mode: CodeMirror.getMode(config, parserConfig.scriptingModeSpec)
          }
          );
        }, le);
      
        CodeMirror.defineMIME("splfamilias/" + le, {name: modename, scriptingModeSpec:li});
      });
}

new ModuloLenguajeSplFamilias([], "");

global.controllerSplFamilias = function(tok,  lenguajeExterno){
   new ModuloLenguajeSplFamilias(tok,  lenguajeExterno);
}