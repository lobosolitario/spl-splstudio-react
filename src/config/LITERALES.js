var LITERALES = {    
    nombreApp:{
        es:"SPL Studio",
        en:"SPL Studio"
    },
    commonsAceptar:{
        es:"Aceptar",
        en:"Acept"
    },
    commonsCancelar:{
        es:"Cancelar",
        en:"Cancell"
    },
    //MENUS
    menuProyecto:{
        es:"Proyecto",
        en:"Project"
    },
    menuEditar:{
        es:"Editar",
        en:"Edit"
    },
    menuVer:{
        es:"Ver",
        en:"View"
    },
    menuHerramientas:{
        es:"Herramientas",
        en:"Tools"
    },
    menuAyuda:{
        es:"Ayuda",
        en:"Help"
    },
    //Submenus de nuevo Proyecto
    menuFicheroSubmenuNuevoProyecto:{ 
        es:"Nuevo",
        en:"New"
    },
    menuFicheroSubmenuAbrirProyecto:{
        es:"Abrir",
        en:"Open"
    },
    menuFicheroSubmenuGuardar:{
        es:"Guardar",
        en:"Save"
    },
    menuFicheroSubmenuGuardarComo:{
        es:"Guardar como...",
        en:"Save as..."
    },
    menuFicheroSubmenuExportarProyecto:{
        es:"Exportar",
        en:"Export"
    },
    menuFicheroSubmenuImportarProyecto:{
        es:"Importar",
        en:"Import"
    },
    menuFicheroSubmenuPropiedadesProyecto:{
        es:"Propiedades",
        en:"Properties"
    },
    menuFicheroSubmenuEliminarProyecto:{
        es:"Eliminar",
        en:"Delete"
    },
    menuFicheroSubmenuCerrarProyecto:{
        es:"Cerrar",
        en:"Close"
    },
    menuEditarSubmenuPreferencias:{
        es:"Preferncias",
        en:"Settings"
    },
    menuAyudaSubmenuDocumentacion:{
        es:"Documentación",
        en:"Documentation"
    },
    menuAyudaSubmenuNotasVersion:{
        es:"Notas de versión",
        en:"Release notes"
    },
    menuAyudaSubmenuAcercaDe:{
        es:"Acerca de {nombreApp}",
        en:"About {nombreApp}"
    },
    //Secciones
    seccionPropiedades:{
        es:"Propiedades",
        en:"Properties"
    },
    seccionEliminar:{
        es:"Eliminar",
        en:"Delete"
    },
    seccionSeleccionarFichero:{
        es:"Seleccionar fichero",
        en:"Select file"
    },
    //EDITOR DE LENGUAJE
    editorDeLenguajeTituloVentana:{
        es:"Editar {nombreNodo}",
        en:"Edit {nombreNodo}"
    },
    editorDeLenguajeNombre:{
        es:"Palabra reservada",
        en:"Reserved word"
    },
    editorDeLenguajeValorDefecto:{
        es:"Valor por defecto",
        en:"Default value"
    },
    editorDeLenguajeDescripcion:{
        es:"Descripción",
        en:"Description"
    },
    crearNuevaReferencia:{
        es:"Crear nueva palabra reservada o referencia",
        en:"Create new reserved word or reference"
    },
    eliminarReferencia:{
        es:"¿Desea eliminar la referencia {ref}?",
        en:"Do you want to delete the reference {ref}?"
    },
    eliminarPalabraReservada:{
        es:"¿Desea eliminar la palabra reservada {ref}?",
        en:"Do you want to delete the reserved word {ref}?"
    },

    //EDITOR DE PLANTILLAS
    formularioNuevaPlantillaTitulo:{
        es:"Nueva plantilla",
        en:"New template"
    },
    formularioNuevaPlantillaNombre:{
        es:"Nombre",
        en:"Name"
    },
    formularioNuevaPlantillaAncla:{
        es:"Ancla",
        en:"Anchor"
    },
    formularioNuevaPlantillaGrupo:{
        es:"Familia",
        en:"Family"
    },
    eliminarPlantilla:{
        es:"¿Desea eliminar la plantilla {nombrePlantilla}?",
        en:"Do you want to delete the template {nombrePlantilla}?"
    },
    formularioSubirPlantillaTitulo:{
        es:"Importar prototipo",
        en:"Import prototype"
    },
    formularioSubirPlantillaFormularioPrototipo:{
        es:"Prototipo",
        en:"Prototype"
    },
    //EDITOR DE Productos
    formularioNuevoProductoTitulo:{
        es:"Nuevo producto",
        en:"New product"
    },
    formularioNuevaProductoNombre:{
        es:"Nombre",
        en:"Name"
    },
    formularioNuevaProductoGrupo:{
        es:"Grupo",
        en:"Group"
    },
    eliminarProducto:{
        es:"¿Desea eliminar el producto {nombreProducto}?",
        en:"Do you want to delete the product {nombreProducto}?"
    },
    //PROYECTOS
    formularioNuevoProyectoTitulo:{
        es:"Nuevo proyecto",
        en:"New project"
    },
    formularioEditarProyectoTitulo:{
        es:"Línea de productos: {linea}",
        en:"Product line: {linea}"
    },
    formularioProyectosNombre:{
        es:"Nombre",
        en:"Name"
    },
    formularioProyectosUsuario:{
        es:"Usuario",
        en:"User"
    },
    formularioProyectosCreacion:{
        es:"Creado",
        en:"Created"
    },
    formularioProyectosModificacion:{
        es:"Modificado",
        en:"Modified"
    },
    panelEliminarProyectoMensajeEliminar:{
        es:"¿Desea eliminar el proyecto {nombreProyecto}?",
        en:"Do you want to delete the project {nombreProyecto}?"
    },
    panelTipoUsuarioPersonal:{
        es:"Personal",
        en:"Personal"
    },
    formularioEliminarProyecto:{
        es:"Eliminar proyecto",
        en:"Delete project"
    },
    formularioEliminarProyectoBoton:{
        es:"Eliminar",
        en:"Delete"
    },
    //HOME
    panelHomeBienvenido:{
        es:"Bienvenido a {nombreApp}",
        en:"Welcome to {nombreApp}"
    },
    panelHomeTexto1:{
        es:`
        SPL Studio es un software enfocado a la construcción de líneas de productos de software. 
        Esta aplicación basada en tecnología web, aporta las herramientas necesarias para llevar a 
        cabo esta tarea del modo mas sencillo posible, haciendo que la implantación de este tipo de 
        metodología en diferentes proyectos sea muy rápida. De esta forma, se amplia el abanico de 
        posibilidades donde pueden ser aprovechadas las múltiples ventajas que ofrece el desarrollo 
        basado en líneas de productos de software.`,

        en:`
        SPL Studio is a software focused on building software product lines. This application based 
        on web technology provides the necessary tools to carry out this task in the simplest way possible,
         making the implementation of this type of methodology in different projects very fast. In this way,
          the range of possibilities where the multiple advantages offered by development based on software
           product lines can be exploited.`
    },
    panelHomeTexto2:{
        es:`
        SPL Studio se ha construido sobre una arquitectura basada en microservicios con una interfaz de 
        usuario implementada en React, con ello se consigue un rendimiento similar al que pudiese tener 
        una aplicación nativa. Además de permitir su integración con otro tipo de tecnologías.`,
        
        en:`
        SPL Studio has been built on an architecture based on microservices with a user interface implemented
         in React, thereby achieving a performance similar to that of a native application. In addition to
          allowing its integration with other types of technologies.`
    },
    //CONFIGURACION
    panelConfiguracionConfiguracion:{
        es:"Configuración",
        en:"Settings"
    },
    panelConfiguracionTema:{
        es:"Seleccionar tema:",
        en:"Select theme"
    },
    panelConfiguracionIdioma:{
        es:"Seleccionar idioma:",
        en:"Select language"
    },
    // ----------------------------------------  LOGIN -------------------------------------------
    loginUsuario:{
        es:"E-mail",
        en:"E-mail"
    },
    loginPass:{
        es:"Contraseña",
        en:"Password"
    },
    loginPerder:{
        es:"Ovidé mi contraseña",
        en:"Forget my password"
    },
    loginIniciaSesion:{
        es:"Iniciar sesión",
        en:"Sign in"
    },
    loginRegistrate:{
        es:"Registro",
        en:"Sign up"
    },
    loginTerminos:{
        es:"Términos",
        en:"Terms"
    },
    loginDemo:{
        es:"Demostración",
        en:"Demonstration"
    },
    registroNombreUsuario:{
        es:"Usuario",
        en:"Username"
    },
    registroPass:{
        es:"Contraseña",
        en:"Password"
    },
    registroRepitaPass:{
        es:"Repita la contraseña",
        en:"Repeat password "
    },
    // ----------------------------------------  COMUNES -------------------------------------------
    mensajeCargando:{
        es:"Cargando...",
        en:"Loading..."
    },
    mensajeCompilando:{
        es:"Compilando...",
        en:"Compiling..."
    },
    mensajeGuardando:{
        es:"Guardando...",
        en:"Saving..."
    },
    // ----------------------------------------  ERRORES -------------------------------------------
    ERROR_DEF_01:{
        es:"Se ha producido un error",
        en:"An error has occurred"
    },
    ERROR_DEF_02:{
        es:"La entrada no es válida",
        en:"The input is not valid"
    },
    ERROR_IN_01:{
        es:"El texto no es válido",
        en:"The text is not valid"
    },
    ERROR_IN_02:{
        es:"El texto no puede ser vacío",
        en:"The text can not be empty"
    },
    ERROR_INPR_01:{
        es:"La palabra reservada no puede ser vacía",
        en:"The reserved word can not be empty"
    },
    ERROR_INPR_02:{
        es:"La palabra reservada sólo puede contener caracteres alfanumericos y guión bajo",
        en:"The reserved word can only contain alphanumeric characters and underscore"
    },
    ERROR_INPR_03:{
        es:"La palabra reservada no puede comenzar con un número",
        en:"The reserved word can not start with a number"
    },
    ERROR_INPR_04:{
        es:"El máximo de caracteres permitidos es {ncaracteres}",
        en:"The maximum characters allowed is {ncharacters}"
    },
    ERROR_INPR_05:{
        es:"No se permiten mayusculas para definir la palabra reservada",
        en:"Capital letters are not allowed to define the reserved word"
    },
    ERROR_LOGIN_01:{
        es:"Usuario o contraseña incorrectos",
        en:"Incorrect username or password"
    },
}

export default LITERALES