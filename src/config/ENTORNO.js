export const ENTORNO_ACTUAL = "tst";

export const ENTORNO = {
    loc : {
        backendhost: "http://127.0.0.1",
        getwayport: "8095",
        compilerport: "3101",
    },
    tst : {
        backendhost: "http://jocedo.duckdns.org",
        getwayport: "8095",
        compilerport: "3101",
    },
}

export function getVariableEntorno(variable){
    return ENTORNO[ENTORNO_ACTUAL][variable];
}