var paleta_drk = {
    c1 : "#1e1e1e",
    c2 : "#363636",
    c3 : "#525252",
    c4 : "#00e7c1",
    c5 : "white",
    c6 : "#c9c9c9",
    c7 : "red"
}

var paleta_lth = {
    c1 : "#B56D21",
    c2 : "#FAEDE1",
    c3 : "#FCC490",
    c4 : "#1E609E",
    c5 : "black",
    c6 : "",
    c7 : "red"
}

var paleta_rdt = {
    c1 : "#211F12",
    c2 : "#414535",
    c3 : "#545238",
    c4 : "#d36135",
    c5 : "white",
    c6 : "",
    c7 : "red"
}

var paleta_wth = {
    c1 : "white",
    c2 : "black",
    c3 : "#D1CDCD",
    c4 : "#211F12",
    c5 : "",
    c6 : "",
    c7 : "red"
}


var TEMA = {    
    color1:{
        //Cabecera
        drk:paleta_drk.c1, 
        lth:paleta_lth.c1,
        rdt:paleta_rdt.c1,
        wht:paleta_wth.c1
    },
    color2:{
        //Color central y fondo del editor
        drk:paleta_drk.c2,
        lth:paleta_lth.c2,
        rdt:paleta_rdt.c2,
        wht:paleta_wth.c1
    },
    color3:{
        //Color de la barra de edicion y botonera lateral
        drk:paleta_drk.c3,
        lth:paleta_lth.c3,
        rdt:paleta_rdt.c3,
        wht:paleta_wth.c3
    },
    color4:{
        //Pie
        drk:paleta_drk.c1, 
        lth:paleta_lth.c1,
        rdt:paleta_rdt.c1,
        wht:paleta_wth.c2
    },
    logo:{
        //Color de los iconos
        drk:paleta_drk.c4,
        lth:paleta_lth.c4,
        rdt:paleta_rdt.c4,
        wht:paleta_wth.c4
    },
    iconos1:{
        //Color de los iconos
        drk:paleta_drk.c5,
        lth:paleta_lth.c5,
        rdt:paleta_rdt.c5,
        wht:paleta_wth.c4
    },
    iconos2:{
        //Color de los iconos activados
        drk:paleta_drk.c4,
        lth:paleta_lth.c4,
        rdt:paleta_rdt.c4,
        wht:paleta_wth.c4
    },
    textos:{
        //Color general de los textos
        drk:paleta_drk.c5,
        lth:paleta_lth.c5,
        rdt:paleta_rdt.c5,
        wht:paleta_wth.c4
    },
    textosInverso:{
        //Color general de los textos
        drk:paleta_drk.c2,
        lth:paleta_lth.c2,
        rdt:paleta_rdt.c2,
        wht:paleta_wth.c1
    },
    error:{
        //Color general de los textos
        drk:paleta_drk.c7,
        lth:paleta_lth.c7,
        rdt:paleta_rdt.c7,
        wht:paleta_wth.c7
    },
    // ---------------------------------------------------   MENUS --------------------------------------------------

    textosMenu1:{
        //Color de los textos de los menus
        drk:paleta_drk.c6,
        lth:paleta_lth.c3,
        rdt:paleta_rdt.c4,
        wht:paleta_wth.c4
    },
    textosMenu2:{
        //Color de los textos de los menus resaltados
        drk:paleta_drk.c5,
        lth:paleta_lth.c5,
        rdt:paleta_rdt.c5,
        wht:paleta_wth.c4
    },
    // ----------------------------------------------   EDITOR DE LENGUAJE ------------------------------------------
    //Comunes a todos los nodos
    colorNodoSeleccionado:{
        //Color de los nodos del editor de lenguajes
        drk:paleta_drk.c3,
        lth:paleta_lth.c3,
        rdt:paleta_rdt.c3,
        wht:paleta_wth.c3
    }, 
    conexionesNodos:{
        //Color de los nodos del editor de lenguajes
        drk:paleta_drk.c6,
        lth:paleta_lth.c5,
        rdt:paleta_rdt.c5,
        wht:paleta_wth.c4
    }, 
    //--- Nodo Basico ---
    colorNodo:{
        //Color de los nodos del editor de lenguajes
        drk:paleta_drk.c3,
        lth:paleta_lth.c3,
        rdt:paleta_rdt.c3,
        wht:paleta_wth.c3
    },
    textosNodo:{
        //Color de los textos de los nodos del editor de lenguaje
        drk:paleta_drk.c6,
        lth:paleta_lth.c5,
        rdt:paleta_rdt.c5,
        wht:paleta_wth.c4
    },
    bordeNodo:{
        //Color de los bordes de los nodos del editor de lenguaje
        drk:paleta_drk.c3,
        lth:paleta_lth.c3,
        rdt:paleta_rdt.c3,
        wht:paleta_wth.c3
    },
    //--- Nodo seleccionado --- 
    bordeNodoSeleccionado:{
        //Color de los textos de los nodos del editor de lenguaje
        drk:paleta_drk.c4,
        lth:paleta_lth.c4,
        rdt:paleta_rdt.c4,
        wht:paleta_wth.c4
    },
    //--- Nodo root --- 
    colorNodoRoot:{
        //Color de los textos de los nodos del editor de lenguaje
        drk:paleta_drk.c1, 
        lth:paleta_lth.c1,
        rdt:paleta_rdt.c1,
        wht:paleta_wth.c1
    },
    bordeNodoRoot:{
        //Color de los textos de los nodos del editor de lenguaje
        drk:paleta_drk.c6,
        lth:paleta_lth.c3,
        rdt:paleta_rdt.c4,
        wht:paleta_wth.c4
    },
    textosNodoRoot:{
        //Color de los textos de los nodos del editor de lenguaje
        drk:paleta_drk.c6,
        lth:paleta_lth.c3,
        rdt:paleta_rdt.c5,
        wht:paleta_wth.c4
    },
    //--- Nodo Referencia --- 
    colorNodoReferencia:{
        //Color de los textos de los nodos del editor de lenguaje
        drk:paleta_drk.c2, 
        lth:paleta_lth.c2,
        rdt:paleta_rdt.c2,
        wht:paleta_wth.c1
    },
    bordeNodoReferencia:{
        //Color de los bordes de los nodos del editor de lenguaje
        drk:paleta_drk.c6,
        lth:paleta_lth.c6,
        rdt:paleta_rdt.c5,
        wht:paleta_wth.c4
    },
    textosNodoReferencia:{
        //Color de los textos de los nodos del editor de lenguaje
        drk:paleta_drk.c6,
        lth:paleta_lth.c6,
        rdt:paleta_rdt.c5,
        wht:paleta_wth.c4
    },
    //
    editorNodoInterior:{
        //Color del fondo del editor de nodos, se coloca en la botonera la teral y el contenedor principal
        drk:paleta_drk.c2,
        lth:paleta_lth.c2,
        rdt:paleta_rdt.c2,
        wht:paleta_wth.c1
    },
    // ----------------------------------------------   EXPLORADOR FICHEROS ------------------------------------------
    colorIconoCarpeta1:{
        //El icono de carpetas del explorador de ficheros tiene dos colores este es el principal
        drk:"#ffdb3d",
        lth:"#ffdb3d",
        rdt:"#ffdb3d",
        wht:"#ffdb3d",
    },
    colorIconoCarpeta2:{
        //El icono de carpetas del explorador de ficheros tiene dos colores este es el secundario
        drk:"#dea209",
        lth:"#dea209",
        rdt:"#dea209",
        wht:"#dea209",
    },
    colorIconoFichero1:{
        //El icono de ficheros del explorador de ficheros tiene dos colores este es el principal
        drk:"#f2f2f2",
        lth:"#f2f2f2",
        rdt:"#f2f2f2",
        wht:"#f2f2f2",
    },
    colorIconoFichero2:{
        //El icono de ficheros del explorador de ficheros tiene dos colores este es el secundario
        drk:"#c7c7c7",
        lth:"#c7c7c7",
        rdt:"#c7c7c7",
        wht:"#c7c7c7",
    },
    textosFicheros:{
        //El icono de ficheros del explorador de ficheros tiene dos colores este es el principal
        drk:paleta_drk.c5,
        lth:paleta_lth.c5,
        rdt:paleta_rdt.c5,
        wht:paleta_wth.c4
    },
    textosFicherosSeleccionado:{
        //El icono de ficheros del explorador de ficheros tiene dos colores este es el secundario
        drk:paleta_drk.c4,
        lth:paleta_lth.c4,
        rdt:paleta_rdt.c4,
        wht:paleta_wth.c4
    }
    
    // ----------------------------------------------   VARIOS ------------------------------------------
    ,
    colorBotonCompilar:{
        //El icono de ficheros del explorador de ficheros tiene dos colores este es el secundario
        drk:"#00c434",
        lth:"#00c434",
        rdt:"#00c434",
        wht:"#00c434",
    },
    colorBotonCompilarSeleccionado:{
        //El icono de ficheros del explorador de ficheros tiene dos colores este es el secundario
        drk:"#00de3b",
        lth:"#00de3b",
        rdt:"#00de3b",
        wht:"#00de3b",
    }
}

export default TEMA