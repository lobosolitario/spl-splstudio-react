var PARAMETROS = {   
    entorno:"dev",
    apis:{
        gatewayEndpoint : 'localhost:8095/api/'
    },
    editor:{
        menuLateral:{
            tamanyoIconos : 25,
            minimoExpandido : 300,
            maximoExpandido :600,
            anchoBotonera : 50
        },
        menus:{
            sizeMenus : "medium",
            sizeSubMenus : "medium",
            margenConLogo : 50
        },
        altoBarraMenu : 30,
        altoCabecera : 50,
        altoPie : 30
    },
    editorLenguaje : {
        maximoCaracteresNodo : 20
    },
    botoneras : {
        espaciadoBotonerasPaneles:20,
        tamanyoIconos:20,
    },
    explorador : {
        altoGruposFicheros : 15,
        separacionGruposFicheros :3
    },
    varios : {
        tamanyoTextoMensajes: 25,
    },
    textos : {
        tamanyoTextos1: 35, //Titulo
        tamanyoTextos2: 30,  //Seccion
        tamanyoTextos3: 25,  //Seccion
        tamanyoTextos3: 20  //Seccion
    },
    intersticiales : {
        maxAncho: 800,
        minAncho: 400
    },
    botones : {
        alto: 25,
        ancho: 100
    },
    conexion:{
        dev:{
            apiUrl:"localhost",
            apiPort:"5101"
        }
    }
}

export default PARAMETROS