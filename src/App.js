
import './App.css';

import {render} from 'react-dom'
import LayoutCreator from './components/componentes-layout/LayoutCreator/LayoutCreator'

import {TemaProvider} from './hooks/useTema'
import {LiteralesProvider} from './hooks/useLiterales'
import {ProyectoProvider} from './hooks/useProyecto'
import {Enrutador} from './hooks/useEnrutador'
import {ApiProvider} from './hooks/useApi';
import {UsuarioProvider} from './hooks/useUsuario';
import { CookiesProvider } from 'react-cookie';

function App() {
  return (
      <CookiesProvider>
            <ApiProvider>
                  <UsuarioProvider>
                        <TemaProvider>
                              <LiteralesProvider>
                                    <ProyectoProvider>
                                          <Enrutador></Enrutador>
                                    </ProyectoProvider>
                              </LiteralesProvider>
                        </TemaProvider>
                  </UsuarioProvider>  
            </ApiProvider>
      </CookiesProvider>
  );
}

export default App;
