function CreadorDibujo(lenguaje){
    var maxX = 0;
    var maxY = 0;
    var id = 0;

    this.crearDibujo = function(){
        var dibujo = new Dibujo();
        id = 0;

        calculaElementosRecursivo(lenguaje.nodo, 0 , 0, dibujo)
        dibujo.x = maxX + 1;
        dibujo.y = maxY + 1;
        return dibujo;
    }

    function calculaElementosRecursivo(nodo, offset, nivel, dibujo){
        var anchoHijos = 0;
        var conexiones = [];

        //Cada uno de los hijos se dibuja a partir del ofset actual, teniendo en cuenta el tamanyo de sus hijos
        for(var i = 0; i < nodo.hijos.length; i++){
            var elementoHijo = calculaElementosRecursivo(nodo.hijos[i], offset + anchoHijos, nivel + 1, dibujo)
            anchoHijos = anchoHijos + elementoHijo.total;
            conexiones.push(elementoHijo)
        }

        //Si no hay ancho para los hijos, se tiene en cuenta el ancho del propio nodo
        if(anchoHijos == 0){
           anchoHijos = 1; 
        }

        //Se calcula la posicion en funcion del espacio que tiene 
        var mipos = offset +  Math.ceil(anchoHijos/2) - 1

        //Se actualizan los maximos
        if(mipos > maxX){ maxX = mipos;  }
        if(nivel > maxY){ maxY = nivel;  }

        var e01 = crearElemento(dibujo,  nodo ,mipos, nivel, conexiones)
        e01.total = anchoHijos;
        return e01;
    }

    function crearElemento(dibujo, nodo, x, y, conexiones){
        var el1 = new ElementoDibujo();
        el1.nodo = nodo;
        el1.x = x;
        el1.y = y; 
        el1.id = id;
        if(conexiones != undefined){
             el1.conexiones = conexiones;   
        }
        dibujo.elementos.push(el1);
        id = id + 1;
        return el1;
    }
}


function ElementoDibujo() {
    this.id = -1;
    this.x = null;
    this.y = null;
    this.conexiones = [];
    this.nodo = null;
}

function Dibujo() {
    this.x = 0;
    this.y = 0;
    this.elementos = [];
}

export default CreadorDibujo;