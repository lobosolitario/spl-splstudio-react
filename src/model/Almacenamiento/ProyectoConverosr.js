import Inicializador from "../../utils/Inicializador";
import Proyecto from "../Proyecto/Proyecto";
import ElementosConversor from "./ElementosConversor";

const ProyectoConversor = function(){
    const elementosConversor = new ElementosConversor();
    const inicializador = new Inicializador();

    this.getInfo = function(proyecto){
   
       
        let proyectoSerializable = {
            id : proyecto.id,
            nombre : proyecto.nombre,
            elementos : elementosConversor.getElementos(proyecto),
            eliminados : proyecto.eliminados
        }
      
        return proyectoSerializable;
    }


    //------------------------------------------------------------------------------------------------------//


    this.setInfo = function(infoProyecto, infoElementos){
        console.log("Se va a cargar la informacin de los elementos")
        //Se obtienen los elementos del proyecto
        let [lenguaje, productos, plantillas] = elementosConversor.setElementos(infoProyecto, infoElementos)

        //Inicializar el proyecto
        let proy = inicializador.inicializarProyecto(infoProyecto.nombre)
        proy.id = infoProyecto.id;
        proy.lenguaje = lenguaje;
        proy.productos = productos;
        proy.plantillas = plantillas;
        
        return proy;
    }

}

export default ProyectoConversor;