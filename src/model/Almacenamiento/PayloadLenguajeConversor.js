
import Inicializador from "../../utils/Inicializador";
import OperacionesLenguaje from "../Operaciones/OperacionesLenguaje";
import Lenguaje from "../Proyecto/Lenguaje";
import Nodo from "../Proyecto/Nodo";

const PayloadLenguajeConversor = function(){
    


    this.getPayload = function(lenguaje){
        //El payload para lenguaje se compone de un array de nodos
        var nodosSerializable = [];
        var listaNodos = lenguaje.operaciones().getArrayNodos();
        for(var i = 0; i < listaNodos.length; i++){
            nodosSerializable.push(getJSONNodo(listaNodos[i]))
        }
        
       
        return nodosSerializable;
    }

    function getJSONNodo(nodo){
        var nodoSerializable = {
            id : nodo.id,
            nombre : nodo.nombre,
            hijos : getHijosNodo(nodo),
            esReferencia : nodo.esReferencia,
            esRoot : nodo.esRoot,
            descripcion : nodo.descripcion,
            defecto : nodo.defecto
        }
        return nodoSerializable;
    }

    function getHijosNodo(nodo){
        var hijosSerializable = [];
        for(var i = 0; i < nodo.hijos.length; i++){
            hijosSerializable.push(nodo.hijos[i].id)
        }
        return hijosSerializable;
    }


    //------------------------------------------------------------------------------------------------------//



    this.setPayload = function(lenguaje, payload){
        if(payload != null){
            var root = crearNodo(payload, "root", null);
            if(root != null){
                lenguaje.nodo = root;
            }else{
                lenguaje.nodo = new Lenguaje().nodo;
            }
        }

    }


    function crearNodo(listaNodos, id, padre){
        var data = listaNodos.filter(nodo => nodo.id == id)[0];
        if(id == "root"){
            data = listaNodos.filter(nodo => nodo.esRoot)[0];
        }
        
        if(data){
            var nodo = new Nodo();
            nodo.id = data.id;
            nodo.nombre = data.nombre;
            nodo.esReferencia = data.esReferencia;
            nodo.esRoot = data.esRoot;
            nodo.descripcion = data.descripcion;
            nodo.defecto = data.defecto;
            nodo.hijos = [];
            nodo.padre = padre;
            for(var i = 0; i < data.hijos.length; i++){
                var hijo = crearNodo(listaNodos, data.hijos[i] , nodo);
                if(hijo != null){
                    nodo.hijos.push(hijo)
                }
            }
            return nodo;
        }
        return null;
    }

}

export default PayloadLenguajeConversor;