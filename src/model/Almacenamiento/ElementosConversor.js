import Encriptador from "../../utils/Encriptador";
import Inicializador from "../../utils/Inicializador";
import Lenguaje from "../Proyecto/Lenguaje";
import Plantilla from "../Proyecto/Plantilla";
import Producto from "../Proyecto/Producto";
import PayloadLenguajeConversor from "./PayloadLenguajeConversor";

const ElementosConversor = function(){
    

    var conversorPayloadLenguaje = new PayloadLenguajeConversor();
    var encriptador = new Encriptador();

    var LENGUAJE = 0;
    var PRODUCTO = 1;
    var PLANTILLA = 2;

    var mapTipo = ["LENGUAJE","PRODUCTO","PLANTILLA"]

    let codificacionPayload = [];
    let decodificacionPayload = [];
    
    this. getElementos= function(proyecto){
        console.log("Transformando para guardar")

        var elementos = [];

        //Plantillas
        for(var i= 0; i < proyecto.plantillas.length; i++){
            let plantilla = crearElemento(proyecto.plantillas[i], 2)
            if(plantilla != null){
                elementos.push(plantilla)
            }
            
        }

        //Productos
        for(var i= 0; i < proyecto.productos.length; i++){
            let producto = crearElemento(proyecto.productos[i], 1)
            if(producto != null){
                elementos.push(producto)
            }
            
        }

        //Lenguaje
        var lenguaje  =  crearElemento(proyecto.lenguaje, 0)
        if(lenguaje != null){
            elementos.push(lenguaje)
        }

        //var lenguaje  = conversorLenguaje.getJSON(proyecto.lenguaje)
        console.log("ELEMENTOS A GUARDAR")
        console.log(elementos)

        return elementos;
    }

    function crearElemento(elemento, tipo){
        let payload = codificacionPayload[tipo](elemento);
        let payloadCodificado = JSON.stringify(payload);

        let infoElemento = {
            id : elemento.id,
            nombre : elemento.nombre,
            tipo : tipo,
            grupo : elemento.grupo,
            payload : payloadCodificado,
            //eliminado : (elemento.eliminado == true)
        }
        return infoElemento;
    }


    codificacionPayload[LENGUAJE] = function(elemento){
        var payload = {
            lenguaje : encriptador.encriptar( JSON.stringify(conversorPayloadLenguaje.getPayload(elemento)))
        }
        
        console.log(payload)
        return payload;
    }

    codificacionPayload[PLANTILLA] = function(elemento){
        var payload = {
            code :  "" + encriptador.encriptar(elemento.code),
            ancla : elemento.ancla
          }
        console.log(payload.code)
        return payload;
    }

    codificacionPayload[PRODUCTO] = function(elemento){
        var payload = {
            code :   encriptador.encriptar(elemento.code)
          }
        return payload;
    }
    //-------------------------------------------------------------------------------------------------------------------------------//

    this.setElementos= function(infoProyecto, infoElementos){
         //Se crean los elementos desde los datos obtenidos del servicio
        console.log("Transformando para cargar")

        var elementos = [];

        var lenguaje = lenguaje = new Lenguaje();
        var productos = [];
        var plantillas = [];

        for(var i = 0; i < infoElementos.length; i++){
            var elemento = infoElementos[i];
            console.log("tipo -> " + elemento.tipo)
            switch (elemento.tipo) {
                case mapTipo[LENGUAJE]:
                    setInformacion(infoProyecto, elemento, lenguaje)
                    break;
                case  mapTipo[PRODUCTO]:
                    let producto =  new Producto();
                    setInformacion(infoProyecto, elemento, producto)
                    productos.push(producto)
                    break;

                case  mapTipo[PLANTILLA]:
                    let plantilla =  new Plantilla();
                    setInformacion(infoProyecto, elemento, plantilla)
                    plantillas.push(plantilla)
                    break;
                default:
                    break;
            }

        }


        elementos.push(lenguaje);
        elementos.push(productos);
        elementos.push(plantillas);

        return elementos;
    }


    function setInformacion(infoProyecto, infoElemento, elemento){
        elemento.id = infoElemento.id;
        elemento.nombre = infoElemento.nombre;
        elemento.grupo = infoElemento.grupo;
        elemento.tipo = infoElemento.tipo;

        let payloadDesencriptado = JSON.parse(infoElemento.payload)
        decodificacionPayload[infoElemento.tipo](infoProyecto, payloadDesencriptado, elemento)
    }


    decodificacionPayload[mapTipo[LENGUAJE]] = function(infoProyecto, payload, elemento){
        //console.log(payload.lenguaje)
        //console.log(encriptador.desencriptar(payload.lenguaje))
        conversorPayloadLenguaje.setPayload(elemento, JSON.parse(encriptador.desencriptar(payload.lenguaje)));
    }

    decodificacionPayload[mapTipo[PLANTILLA]] = function(infoProyecto, payload, elemento){
        //console.log(payload.code)
        elemento.code =  encriptador.desencriptar(payload.code);
        elemento.ancla = payload.ancla;
        //console.log(elemento.code)
    }

    decodificacionPayload[mapTipo[PRODUCTO]] = function(infoProyecto, payload, elemento){
        elemento.code = encriptador.desencriptar(payload.code);
    }

}

export default ElementosConversor;