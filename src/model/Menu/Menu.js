function Menu () {

    let menu = [];


    this.crearMenu = function(id_padre, i_id, i_literal, i_func, i_habilitado){
        if(i_habilitado == true){
            i_habilitado = function(){return true}
        }
        if(i_habilitado == false){
            i_habilitado = function(){return false}
        }
        if(i_func == null){
            i_func = function(){}
        }

        let nuevomenu = {   
            id:i_id,
            literal:i_literal,
            fun:i_func,
            habilitado:i_habilitado,
            submenus:[]
        }
        let padre = getMenuById(id_padre)
        if(padre != null){
            padre.submenus.push(nuevomenu)
        }else{
            menu.push(nuevomenu)
        }
    }
    function getMenuById(id){
        if(id == null){
            return null;
        }
        for(var i = 0; i < menu.length; i++){
            let menuEncontrado = getMenuByIdRecursivo(menu[i], id)
            if(menuEncontrado != null){
                return menuEncontrado;
            }
        }
        return null;
    }

    function getMenuByIdRecursivo(menu, id){
        if(menu.id == id){
            return menu;
        }

        for(var i = 0; i < menu.submenus.length; i++){
            var submenu = getMenuByIdRecursivo(menu.submenus[i], id)
            if(submenu != null){
                return submenu;
            }
        }

        return null;
    }


    this.getMenu = function(){
        return menu;
    }
}

export default Menu;