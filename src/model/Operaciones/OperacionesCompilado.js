function OperacionesCompilado(i_compilado){
    var compilado = i_compilado;
    var self = this;

    this.compiladoById = function(id){
        if(id == null || compilado == null){
            return null;
        }
        for(var i = 0; i < compilado.compilaciones.length; i++){
            if(compilado.compilaciones[i].id == id){
                return compilado.compilaciones[i]
            }
        }
        return null;
    }

    this.correspondeCompiladoProductoPlantilla = function(id, idProducto, idPlantilla){
        if(id == null || idProducto == null ||idPlantilla == null ){
            return false;
        }
        var compiladoactual = self.compiladoById(id);
        if(compiladoactual == null){
            return false
        }
        if(compiladoactual.idProducto == idProducto && compiladoactual.idPlantilla == idPlantilla){
            return true;
        }
        return false;
    }

    this.getPrimerProductoPlantilla = function( idProducto, idPlantilla){
        if(idProducto == null ||idPlantilla == null ){
            return null;
        }
        return self.compiladoById(idProducto + idPlantilla + "_0");
    }

    this.getPrimerCompiladoSinSeleccion = function(){
        if(compilado == null){
            return null;
        }
        if(compilado.compilaciones.length > 0){
            return compilado.compilaciones[0]
        }
        return null;
    }

    this.getTextoErroresCompilacion = function(compiladoactual){
        console.log("Se va a buscar errores")
        let errores = "";
        for (let i = 0; i < compiladoactual.logs.length; i++) {
            const error = compiladoactual.logs[i];
            errores = errores + getFormatoError(error)
        }
        return errores;
    }

    function getFormatoError(error){
        return  error.log + "\n";
    }

    this.hayErrorDeCompilacion = function(compiladoactual){
        console.log("Se va a buscar errores")
        for (let i = 0; i < compiladoactual.logs.length; i++) {
            const error = compiladoactual.logs[i];
            if(error.type == 4){
                return true
            }
        }
        return false;
    }
}

export default OperacionesCompilado