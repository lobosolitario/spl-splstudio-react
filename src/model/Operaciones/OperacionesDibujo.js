function OperacionesDibujo(i_dibujo){
    let self = this;
    
    const dibujo = i_dibujo;



    this.getElementoDibujoByNodo = function(nodo){
        if(dibujo){
            for(var i = 0; i < dibujo.elementos.length; i++){
                if(dibujo.elementos[i].nodo == nodo){
                    return dibujo.elementos[i];
                }
            }
        }
    
        return null;
    }

    this.getIdFuncionalidadInterna = function(x, y){
        return "nodoX" + x + "Y" + y;
    }

    this.getIdConexionFuncionalidadInterna = function(x, y){
        return "conexionX" + x + "Y" + y;
    }

    this.getElementoDibujo = function(y, x) {
        if(dibujo){
            for(var i = 0; i < dibujo.elementos.length; i++){
                if(dibujo.elementos[i].x == x && dibujo.elementos[i].y == y){
                    return dibujo.elementos[i];
                }
            }
        }
    
        return null;
    }

    //Devuelve ele nodo que conecta con el nodo recibido por parametro
    this.getElementoConexion = function ( x, y){
        if(dibujo){
            for(var i = 0; i < dibujo.elementos.length; i++){
                for(var j = 0; j < dibujo.elementos[i].conexiones.length; j ++){
                    if(dibujo.elementos[i].conexiones[j].x == x && dibujo.elementos[i].conexiones[j].y == y){
                        return dibujo.elementos[i];
                    }
                }
            }
        }
        return null;
    }

}

export default OperacionesDibujo