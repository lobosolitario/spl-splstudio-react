import Commons from "../Commons";
import Lenguaje from "../Proyecto/Lenguaje";
import Nodo from "../Proyecto/Nodo";

function OperacionesProyecto(i_proyecto){
    let self = this;
    
    let proyecto = i_proyecto;

    this.eliminarPlantilla = (plantilla) => {     
         
        var index =  proyecto.plantillas.indexOf(plantilla);
        if(index >= 0){
            proyecto.plantillas.splice(index, 1)
        }
        
        proyecto.eliminados.push(plantilla.id)
    }


    this.eliminarProducto = (producto) => {     
         
        var index =  proyecto.productos.indexOf(producto);
        if(index >= 0){
            proyecto.productos.splice(index, 1)
        }
        
        proyecto.eliminados.push(producto.id)
    }


}

export default OperacionesProyecto