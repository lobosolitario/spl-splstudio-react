import Commons from "../Commons";
import Lenguaje from "../Proyecto/Lenguaje";
import Nodo from "../Proyecto/Nodo";

function OperacionesLenguaje(i_lenguaje){
    let self = this;
    
    const lenguaje = i_lenguaje;

    const commons = new Commons();

    /* Crea crea un lenguaje inicial */
    this.inicializarLenguaje = function(){
        let l = new Lenguaje();

        return l;
    }


    this.crearNodo = function(nombre, nombrePadre){
        //console.log("Creando nodo " + nombre + " en " + nombrePadre);
        let padre = getNodoByName(nombrePadre);
        if(padre != null && padre != undefined){
            padre.hijos.push(crearNodo(padre, nombre, false))
        }

    }

    this.crearReferencia = function(nodo, referencia){
        if(nodo != null  && nodo != undefined && !nodo.esReferencia){
            if(contieneReferencia(nodo, referencia)){   
                console.log("--Ya contiene la referencia")
            }else{
                console.log("--creando nueva referencia")
                var nuevoNodo = crearNodo(nodo, referencia)
                nodo.hijos.push(nuevoNodo)
                return nuevoNodo;
            }
        }
        return null;
    }



    this.eliminarNodo = function(nodo){
        console.log("Eliminando nodo " + nodo.nombre)
        console.log(nodo)
        if(nodo.esReferencia){
            fuerzaEliminacionNodo(nodo)
        }else{
            //Se buscan las referencias y la primera que se encuentre se convierte en principal
            var referencia = this.getNodoReferenciaByName(nodo.nombre)
            if(referencia != null){
                self.convertirEnPrincipal(referencia)
                fuerzaEliminacionNodo(referencia)
            }else{
                fuerzaEliminacionNodo(nodo)
            }

        }


    }


    function contieneReferencia(nodo, nombre){
        var encontrada = false;
        nodo.hijos.forEach(hijo => {
            if(hijo.nombre == nombre){
                encontrada = true;
                return true;
            }
        });
        return encontrada;
    }

    function fuerzaEliminacionNodo(nodoEliminar){
        console.log("Forzando eliminacion")
        var nuevosHijos = [];
        var padre = nodoEliminar.padre;
        padre.hijos.forEach(nodo => {
            if(nodo != nodoEliminar){
                nuevosHijos.push(nodo);
            }
        });
        padre.hijos = nuevosHijos;
    }

    this.convertirEnPrincipal = function(nodo){
        console.log("convertirEnPrincipal")
        if(nodo.esReferencia){
            var original = self.getNodoOriginalByName(nodo.nombre);
            if(original != null){
                console.log("Se va a intercambiar")
                interCambiarNodos(nodo, original)
            }else{
                console.log("No hay original")
            }
        }else{
            console.log("No es referencia")
        }
    }

    this.modificarNodo = function(nombre, nodo){
        console.log("Modificando nodo " + nombre)
    }


    this.setReferenciasNodo = function(nombreNodo, referencias){
        console.log("Creando referencias de nodo " + nombreNodo)
        console.log(referencias)
        var nodoOriginal = self.getNodoOriginalByName(nombreNodo);
        if(nodoOriginal != null){
            var hijosAnteriores = nodoOriginal.hijos;
            nodoOriginal.hijos = [];

        }
        //Se eliminan las referencias del nodo
        //Para cada referencia
            //Si el elemento 


            //Si la referencia no existe se crea como nuevo nodo
    }

    this.actualizarNombreNodo = function(nodo, nuevoNombre){
        if(getNodoByName(nuevoNombre) == null){
            var antiguoNombre = nodo.nombre;
            console.log("Buscando nodos con nomnre " + antiguoNombre)
            var nodos = self.getArrayNodos();
            nodos.forEach(nodo => {
                if(nodo.nombre == antiguoNombre){
                    nodo.nombre =  nuevoNombre;
                }
            });
         }
    }

    this.isNodoRecursivo = function(nodo){
        var nodopadre = nodo.padre;
        while(nodopadre != null && !nodopadre.esRoot){
            if(nodopadre.nombre == nodo.nombre){
                return true;
            }
            nodopadre = nodopadre.padre;
        }
        return false;
    }


    function interCambiarNodos(nodo1, nodo2){
       var nodoAux = nodo1;
       var padre1Aux = nodo1.padre
       var padre2Aux = nodo2.padre
       //Se reemplaza el nodo1 por el nodo2
       reemplazarNodo(padre1Aux, nodo1, nodo2)
       //Se reemplaza el nodo2 por el nodo 1
       reemplazarNodo(padre2Aux, nodo2, nodo1)
    }

    function reemplazarNodo(padre, nodo, nodoreemplazo){

        for(var i = 0; i < padre.hijos.length; i++){
            if(padre.hijos[i] == nodo){
                nodoreemplazo.padre = padre;
                padre.hijos[i] = nodoreemplazo;  
                return
            }
        }
        console.log("No se ha encontrado reemplazo")
        console.log(nodo)
        console.log(nodoreemplazo)
    }

    function crearNodo(padre, nombre){
        let nodo = new Nodo();
        nodo.nombre = nombre;
        nodo.lenguaje = lenguaje;
        nodo.esReferencia = getNodoByName(nombre) != null;
        nodo.esRoot = false;
        nodo.padre = padre;
        return nodo;
    }


    function getNodoById(id){
        let nodos = self.getArrayNodos();
        for(var i = 0; i < nodos.length; i++){
            if(nodos[i].id == id){
                return nodos[i];
            }
        }
        return null;
    }


    function getNodoByName(nombre){
        let nodos = self.getArrayNodos();
        for(var i = 0; i < nodos.length; i++){
            if(nodos[i].nombre == nombre){
                return nodos[i];
            }
        }
        return null;
    }


    this.getNodoOriginalByName = function(nombre){
        let nodos = self.getArrayNodos();
        for(var i = 0; i < nodos.length; i++){
            if(nodos[i].nombre == nombre && !nodos[i].esReferencia){
                return nodos[i];
            }
        }
        return null;
    }


    this.getNodoReferenciaByName = function(nombre){
        let nodos = self.getArrayNodos();
        for(var i = 0; i < nodos.length; i++){
            if(nodos[i].nombre == nombre && nodos[i].esReferencia){
                return nodos[i];
            }
        }
        return null;
    }


    /* Devuelve el arbol de nodos en forma de array */
    this.getArrayNodos = function(){
        let nodos = new Array();
        getArrayNodosRecursivo(lenguaje.nodo, nodos);
        return nodos;
    }

    function getArrayNodosRecursivo(nodo, nodos){
        nodos.push(nodo);
        for(var i = 0; i < nodo.hijos.length; i++){
            getArrayNodosRecursivo(nodo.hijos[i], nodos)
        }
    }

    this.getPalabrasReservadas = function(){
        let palabrasReservadas = [];
        let nodos = self.getArrayNodos();
        for (let i = 0; i < nodos.length; i++) {
            if(!palabrasReservadas.includes(nodos[i].nombre)){
                palabrasReservadas.push(nodos[i].nombre)
            }
        }
        return palabrasReservadas;
    }
}

export default OperacionesLenguaje