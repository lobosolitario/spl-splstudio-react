import Commons from "../Commons";

const Plantilla = function(plantilla){
    let self = this;
    
    let commons = new Commons();

    //Id de la plantilla
    this.id = commons.generaID("PLANT");

    this.grupo = "";

    this.code = "";

    this.ancla = "";

    this.modificado = false;

    this.nombre = "";

}

export default Plantilla