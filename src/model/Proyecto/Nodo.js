import Commons from "../Commons";

function Nodo() {
    const commons = new Commons();

    this.id = commons.generaID("NODO");
    this.lenguaje = null;
    this.nombre = "";
    this.hijos = [];
    this.esReferencia = false;
    this.esRoot = false;
    this.padre = null;
    this.descripcion = "";
    this.defecto=""
}
export default Nodo;