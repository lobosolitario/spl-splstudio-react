import Commons from "../Commons";
import OperacionesProyecto from "../Operaciones/OperacionesProyecto";
import Lenguaje from "./Lenguaje";

const Proyecto = function(proyecto) {
    var self = this;
    const commons = new Commons();

    this.id = -1;
    this.nombre = "";   
  

    this.lenguaje = undefined;
    this.plantillas = new Array();
    this.productos = new Array(); 
    this.eliminados = new Array(); 

    if(proyecto){
        self.id = proyecto.id;
        self.nombre = proyecto.nombre;    
    
        self.lenguaje = proyecto.lenguaje;
        self.plantillas = proyecto.plantillas;
        self.productos = proyecto.productos; 
        self.eliminados = proyecto.eliminados; 

    }

    
    this.getLenguaje = function(){
        return self.lenguaje;
    }

    this.getPlantilla = function(id){
        for(var i = 0; i < self.plantillas.length; i++){
            if(self.plantillas[i].id == id){
                return self.plantillas[i];
            }
        }
        return null
    }

    this.getProducto = function(id){
        for(var i = 0; i < self.productos.length; i++){
            if(self.productos[i].id == id){
                return self.productos[i];
            }
        }
        return null
    }


    this.operaciones = function(){
        return new OperacionesProyecto(this)
      }


}

export default Proyecto