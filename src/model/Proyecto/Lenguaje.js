import OperacionesLenguaje from '../Operaciones/OperacionesLenguaje';
import Nodo from './Nodo'
import Commons from "../Commons";

function Lenguaje(initData) {
    let self = this;

    const commons = new Commons();

    //Id del lenguaje
    this.id = commons.generaID("LENG");;

    //Nodo root
    this.nodo = null;
    

    init();

    /*Inicializa el lenguaje*/
    function init(){
        self.nodo = new Nodo();
        self.nodo.nombre = "root";
        self.nodo.lenguaje = this;
        self.nodo.esReferencia = false;
        self.nodo.esRoot = true;
    }
    

    this.operaciones = function(){
      return new OperacionesLenguaje(this)
    }
}

export default Lenguaje