import OperacionesCompilado from "../Operaciones/OperacionesCompilado";

function Compilado(){
    var self = this;
    this.compilaciones = [];
    this.errores = []

    this.setData = function(data){
        self.compilaciones = data.proyectoCompilado.compilados;
        self.errores = data.erroresCompilacion;
    }

    this.operaciones = function(){
        return new OperacionesCompilado(this)
    }
}
export default Compilado
