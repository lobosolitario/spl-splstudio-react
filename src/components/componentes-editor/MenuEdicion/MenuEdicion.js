import React, {useState, useEffect, useMemo} from 'react'
import {TemaProvider, useTema} from '../../../hooks/useTema'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'
import Menu from '../../componentes-menu/Menu/Menu.js'
import MenuItem from '../../componentes-menu/MenuItem/MenuItem.js'
import UtilidadLiterales from '../../../utils/UtilidadLiterales'
import PARAMETROS from '../../../config/PARAMETROS.js'

const  MenuEdicion  = (props) => {
    const {getLiteral} = useLiterales();

    const style={
        height: "100%",
        width: "100%",
        alignItems: "center",
        display: "flex",
        paddingLeft: "50px"
    }


    const distanciaAlTop = PARAMETROS.editor.altoCabecera;

  

    return (
        <div style={style}>
            <Menu nombre={getLiteral("menuFichero")} top={distanciaAlTop}>
                <MenuItem nombre={getLiteral("menuFicheroSubmenuNuevoProyecto")} accion="NUEVO_PROYECTO"></MenuItem>
                <MenuItem nombre={getLiteral("menuFicheroSubmenuAbrirProyecto")} accion="ABRIR_PROYECTO"></MenuItem>
                <MenuItem nombre={getLiteral("menuFicheroSubmenuGuardar")} accion="ABRIR_PROYECTO"></MenuItem>
                <MenuItem nombre={getLiteral("menuFicheroSubmenuGuardarComo")} accion="ABRIR_PROYECTO"></MenuItem>
                <MenuItem nombre={getLiteral("menuFicheroSubmenuImportarProyecto")} accion="ABRIR_PROYECTO"></MenuItem>
                <MenuItem nombre={getLiteral("menuFicheroSubmenuExportarProyecto")} accion="ABRIR_PROYECTO"></MenuItem>
                <MenuItem nombre={getLiteral("menuFicheroSubmenuPropiedadesProyecto")} accion="ABRIR_PROYECTO"></MenuItem>
                <MenuItem nombre={getLiteral("menuFicheroSubmenuCerrarProyecto")} accion="CERRAR_PROYECTO"></MenuItem>
            </Menu>
            <Menu  nombre={getLiteral("menuEditar")} top={distanciaAlTop}> 
                
            </Menu>
            <Menu  nombre={getLiteral("menuVer")} top={distanciaAlTop}> 

            </Menu>
            <Menu  nombre={getLiteral("menuHerramientas")} top={distanciaAlTop}> 

            </Menu>
            <Menu  nombre={getLiteral("menuAyuda")} top={distanciaAlTop}> 

            </Menu>
        </div>      
    );
}

export default MenuEdicion;