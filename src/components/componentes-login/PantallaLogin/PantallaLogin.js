import React, {useState, useEffect, useMemo} from 'react'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'
import { useTema } from '../../../hooks/useTema'
import { useUsuario } from '../../../hooks/useUsuario';
import Separador from '../../componentes-atomicos/Separador/Separador';
import BotonGenerico from '../../componentes-botones/BotonGenerico/BotonGenerico';
import BarraIdioma from '../../componentes-configuracion/BarraIdioma/BarraIdioma';
import SelectorIdioma from '../../componentes-configuracion/SelectorIdioma/SelectorIdioma';
import Logo from '../../componentes-iconos/Logo/Logo';
import BarraLinks from '../BarraLinks/BarraLinks';
import FormularioLogin from '../FormularioLogin/FormularioLogin';

const  PantallaLogin = (props) => {
    const {getColor} = useTema();

    
    const style = {
        display: "flex",
        alignItems : "center",
        flexDirection:'column',
        height : "100%",
        width : "100%",
        color: getColor("textos"),
        paddingBottom : "60px",
        boxSizing: "border-box"
    }
    
 

    return(<div className="pantallaLogin" style={style}>
        <BarraIdioma></BarraIdioma>
        <Logo modo={2} size={200}></Logo>
        <Separador altura="20px"></Separador>
        <FormularioLogin></FormularioLogin>
        <BarraLinks></BarraLinks>
        </div>)
}

PantallaLogin.defaultProps = {
}





export default PantallaLogin;