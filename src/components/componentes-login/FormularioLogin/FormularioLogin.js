import React, {useState, useEffect, useMemo} from 'react'
import { useLiterales } from '../../../hooks/useLiterales'
import { useTema } from '../../../hooks/useTema'
import { useUsuario } from '../../../hooks/useUsuario'
import Separador from '../../componentes-atomicos/Separador/Separador'
import { useHistory } from 'react-router';

import InputTextBig from '../../componentes-formularios/InputTextBig/InputTextBig'
import BotonBigForm from '../../componentes-botones/BotonBigForm/BotonBigForm'

 const FormularioLogin = (props) => {
    const {getColor} = useTema()
    const {getLiteral} = useLiterales();
    const {login} = useUsuario();
    const history = useHistory();

    const [user, setUser] = useState("");
    const [pass, setPass] = useState("");

    const [errorUser, setErrorUser] = useState("");
    const [errorPass, setErrorPass] = useState("");

    const onUserChangeHandler = (key , value) => {
        setUser(value)
    }

    const onPassChangeHandler = (key , value) => {
        setPass(value)
    }

    useEffect(() => {
        eliminarError();
    }, [user,pass])

    const marcarError = () => {
        setErrorUser(" ")
        setErrorPass("ERROR_LOGIN_01")
    }

    const eliminarError = () => {
        setErrorUser("")
        setErrorPass("")
    }

    const style = {
        display : "flex",
        flexDirection: "column",
        alignItems: "center",
        height: "100%",
        width: "100%",
        paddingBottom: "60px"
    }


    const onClickHandler = () => {
        
        login(user, pass)
        history.push('/gestion/home')
        

        //marcarError();
    }

    return (
        <div style={style} className="formularioLogin">
        <InputTextBig
            titulo={getLiteral("loginUsuario")}
            onChange={onUserChangeHandler}     
            color={"transparent"}
            colorTexto={getColor("textos")}
            colorTitulo={getColor("textos")}
            borderColor= {getColor("iconos2")}
            error={errorUser}>
        </InputTextBig>   

        <Separador altura="10px"></Separador>
        <InputTextBig
            titulo={getLiteral("loginPass")}
            onChange={onPassChangeHandler}     
            color={"transparent"}
            colorTexto={getColor("textos")}
            colorTitulo={getColor("textos")}
            borderColor= {getColor("iconos2")}
            error={getLiteral(errorPass)}>
        </InputTextBig>  

         <Separador altura="30px"></Separador>
         <BotonBigForm
            texto={getLiteral("loginIniciaSesion")}
            onClick={onClickHandler}
            color={getColor("iconos2")}
            colorh={getColor("iconos2")}
            colorTexto={getColor("textosInverso")}
         ></BotonBigForm>
        </div>
    )
}   


export default FormularioLogin