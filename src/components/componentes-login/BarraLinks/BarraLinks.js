import React, {useState, useEffect, useMemo} from 'react'
import {TemaProvider, useTema} from '../../../hooks/useTema'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'
import SelectorIdioma from '../../componentes-configuracion/SelectorIdioma/SelectorIdioma'
import Link from '../../componentes-atomicos/Link/Link'
import Separador from '../../componentes-atomicos/Separador/Separador'
import { useHistory } from 'react-router';

const BarraLinks = (props) =>  {
    const {getColor} = useTema();
    const {getLiteral} = useLiterales();
    const history = useHistory();

    const styleBarraSuperior = {
        display :"flex",
        justifyContent: "center",
        width: "100%",
        padding: "10px",
        boxSizing: "border-box",
    }

    let color = getColor("textos")
    let colorh = getColor("iconos2")

    const onLinkRegistrarHandler = () => {
        history.push('/registro')
    }

    const onLinkOlvideHandler = () => {
        alert("Ovlide")
    }

    const onLinkTerminos = () => {
        alert("Terminos")
    }

    const onLinkDemo = () => {
        alert("Demo")
    }

    return (
        <div style={styleBarraSuperior}>
            <Link texto={getLiteral("loginRegistrate")} accion={onLinkRegistrarHandler}  color={color} colorh={colorh} ></Link>
            <Separador horizontal altura="25px"></Separador>
            <Link texto={getLiteral("loginPerder")} accion={onLinkOlvideHandler}  color={color} colorh={colorh} ></Link>
            <Separador horizontal altura="25px"></Separador>
            <Link texto={getLiteral("loginDemo")} accion={onLinkDemo}  color={color} colorh={colorh} ></Link>
            <Separador horizontal altura="25px"></Separador>
            <Link texto={getLiteral("loginTerminos")} accion={onLinkTerminos}  color={color} colorh={colorh} ></Link>

        </div>
    )
}


export default  BarraLinks;
