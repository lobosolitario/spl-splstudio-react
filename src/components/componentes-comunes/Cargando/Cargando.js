import React, {useState, useEffect, useMemo} from 'react'
import {TemaProvider, useTema} from '../../../hooks/useTema'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'

import Logo from '../../componentes-iconos/Logo/Logo.js';
import SelectorColores from '../../../utils/SelectorColores'
import './Cargando.css'

const Cargando = (props) => {
    const {getColor} = useTema();
    const {getLiteral} = useLiterales();

    let texto = props.texto;
    if(props.texto == null){
        texto = getLiteral("mensajeCargando")
    }
    

    const style = {
        display :"flex",
        flexDirection : "column",
        justifyContent: "center",
        alignItems: "center",
        height : "100%",
        width : "100%"
    }

    let margenInterno = 2;

    const interior = {
        width: (props.tam + (props.tam / margenInterno)) + "px",
        height:(props.tam + (props.tam / margenInterno))  + "px",
        borderRadius : "100%",
        borderTop:"solid 5px " + getColor("iconos2"),
        boxSizing : "borderBox"
    }




    let diferencia = (props.tam + (props.tam / margenInterno)) - props.tam
    const contenedorLogo = {
        marginTop : "-" + (props.tam + diferencia/2) + "px",
    }


    
    const styleTexto = {
        fontSize : props.tamText + "px",
        color: getColor("iconos2"),
        marginTop : diferencia  + props.tamText
    }

    return (
        <div style={style}>
            <div className="cargandoRotacion" style={interior}>
            </div>
            <div style={contenedorLogo}>
                <Logo size={props.tam} modo="2"></Logo> 
            </div>
            <div style={styleTexto}> 
                {texto}
            </div>
        </div>
    )
    
}

Cargando.defaultProps = {
    tam : 70,
    tamText : 12,
    texto : null
}


export default Cargando