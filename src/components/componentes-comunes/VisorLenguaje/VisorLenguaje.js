import React, {useState, useEffect, useRef} from 'react'
import {animateScroll as scroll} from 'react-scroll';
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'
import {useProyecto} from '../../../hooks/useProyecto'
import {useTema} from '../../../hooks/useTema';

import Lenguaje from '../../../model/Proyecto/Lenguaje'
import CreadorDibujo from '../../../model/PresentacionLenguaje/CreadorDibujo'
import './VisorLenguaje.css'
import Intersticial from '../Intersticial/Intersticial';
import EditorNodo from '../../componentes-editornodos/EditorNodo/EditorNodo';
import MenuContextual from '../../componentes-menucontextual/MenuContextual/MenuContextual';
import Icono from '../../componentes-iconos/Icono/Icono';
import OperacionesDibujo from '../../../model/Operaciones/OperacionesDibujo';
import MensajeAceptarCancelar from '../MensajeAceptarCancelar/MensajeAceptarCancelar';
import PanelReferencias from '../../componentes-editornodos/PanelReferencias/PanelReferencias';




const VisorLenguaje = (props) => {
    const {getColor} = useTema();
    const {proyecto, updateLenguaje} = useProyecto();
    const {getLiteral} = useLiterales();

    const [dibujo, setDibujo] = useState(null)
    const [nodoSeleccionado, setNodoSeleccionado] = useState(null)



    let operacionesDibujo = new OperacionesDibujo(dibujo);
    let lenguaje = proyecto.lenguaje;


    

    let esNodoRecursivo = false;
    if(nodoSeleccionado != null){
        esNodoRecursivo = lenguaje.operaciones().isNodoRecursivo(nodoSeleccionado.nodo)
    }

    //0 No se muestra ninguna pantalla adiccional
    //1 Muestra el dialogo de edicion de un nodo
    //2 Muestra el dialogo de eliminacion de un nodo
    //3 Muestra el dialogo de nueva referencia
    const [modo, setModo] = useState(0);


    const scrollToRef = () =>{

        const el1 = document.querySelector('.contenedorVisorLenguaje')
        const ancho = el1.getBoundingClientRect().right;
        const alto = el1.getBoundingClientRect().bottom;

        const posX = ((nodoSeleccionado.x + 1) * configuracion[props.size].ancho) + ((nodoSeleccionado.x * configuracion[props.size].espacioHorizontal) * 2)
        const posY = ((nodoSeleccionado.y + 1) * configuracion[props.size].alto) + ((nodoSeleccionado.y * configuracion[props.size].espacioVertical))

    
        el1.scrollTo({
            behavior: 'smooth',
            left: posX - (ancho/2),
            top : posY - (alto/2)
           })   
    } 

    useEffect(() => {
        const creadorDibujo = new CreadorDibujo(lenguaje)
        const d = creadorDibujo.crearDibujo()
        setDibujo(d);

    }, [proyecto])

    useEffect(() => {
        if(nodoSeleccionado != null){
            scrollToRef()
        }    
    }, [nodoSeleccionado])

    /*
    useEffect(() => {
        if(nodoSeleccionado != null){
            var nuevoElementoSeleccionado = operacionesDibujo.getElementoDibujoByNodo(nodoSeleccionado.nodo)
            setNodoSeleccionado(nuevoElementoSeleccionado)
        }    
    }, [dibujo])
    */
   
    const onSelectionNodeHandler = (id, x , y) =>{
        var elementoDibujo = operacionesDibujo.getElementoDibujo( y , x);
        setNodoSeleccionado(elementoDibujo)
    }

    const onDesSeleccionarNodoHandler = () => {
        setModo(0)
        setNodoSeleccionado(null)
    }
 

    const onCloseIntersticialHandler = () => {
        setModo(0)
        //setNodoSeleccionado(null)
    }


    const onActualizaNodo = (datos) => {
        lenguaje.operaciones().actualizarNombreNodo(nodoSeleccionado.nodo, datos.nombre)
        nodoSeleccionado.nodo.descripcion = datos.descripcion;
        nodoSeleccionado.nodo.defecto = datos.defecto;
    }


    const onNuevo = () =>{
        setModo(3)
    }

    const onEditar = () =>{
        setModo(1)
    }

    const onEliminar = () =>{
        setModo(2)
    }


    const onConfirmarEliminar = () =>{
        lenguaje.operaciones().eliminarNodo(nodoSeleccionado.nodo)
        updateLenguaje(lenguaje)
        setModo(0)
    }

    const onGoref = (e) =>{
        e.stopPropagation();
        var original = lenguaje.operaciones().getNodoOriginalByName(nodoSeleccionado.nodo.nombre);

        var elementoDibujoOriginal = operacionesDibujo.getElementoDibujoByNodo(original);

        setNodoSeleccionado(elementoDibujoOriginal)

    }

    const onPrincipal = () =>{
        if(nodoSeleccionado != null){
            lenguaje.operaciones().convertirEnPrincipal(nodoSeleccionado.nodo);
            updateLenguaje(lenguaje)

        }
    }

    const onNuevaReferencia = (nombre) => {
        lenguaje.operaciones().crearReferencia(nodoSeleccionado.nodo, nombre)
        updateLenguaje(lenguaje)
        setModo(0)
    }



    const lineas = new Array();
    if(dibujo != null){
        for (var i = 0; i < dibujo.y; i++){
            lineas.push(
                <VisorLineaNodos 
                    key={"Linea" +i }
                    id={"Linea" +i }
                    size={props.size}
                    onClick={onSelectionNodeHandler} 
                    editable={props.editable} 
                    nodoSeleccionado={nodoSeleccionado} 
                    esNodoRecursivo={esNodoRecursivo}
                    y={i} 
                    dibujo={dibujo}
                    onNuevo={onNuevo}
                    onEditar={onEditar}
                    onEliminar={onEliminar}
                    onGoref={onGoref}
                    onPrincipal={onPrincipal}
                    
                    > 
                    
                </VisorLineaNodos>
             )
        }
    }


    const style = {
        borderTop: "solid 1px " +  getColor("color2"),
        borderLeft: "solid 1px " +  getColor("color2"),
        height:props.height,
        width: props.width
    }

    const seleccionado = (nodoSeleccionado != null);

    if(seleccionado){
        switch (modo) {
            case 1:
                return (
                    <>
                    <div  className="contenedorVisorLenguaje nonSelectable" style={style}>
            
                    {lineas}
                    
                    </div>
                         <Intersticial 
                         onClose={onCloseIntersticialHandler} 
                         visible={seleccionado}
                         color={getColor("color3")}
                         colorInterior={getColor("color2")}
                         >
                             <EditorNodo 
                                onClose={onCloseIntersticialHandler} 
                                nodo={nodoSeleccionado.nodo} 
                                onActualizaNodo={onActualizaNodo}
                             ></EditorNodo>
                        </Intersticial>
                    </>
                )
            case 2:
                let literalEliminacion = "eliminarPalabraReservada"
                if(nodoSeleccionado.nodo.esReferencia){
                   literalEliminacion = "eliminarReferencia"
                }

                return (
                    <>
                    <div  className="contenedorVisorLenguaje nonSelectable" style={style}>
            
                    {lineas}
                    
                    </div>
                            <Intersticial 
                            onClose={onCloseIntersticialHandler} 
                            visible={seleccionado}
                            color={getColor("color3")}
                            colorInterior={getColor("color2")}
                            >
                               <MensajeAceptarCancelar 
                                    mensaje={getLiteral(literalEliminacion,{ref:nodoSeleccionado.nodo.nombre, ref2 : "prueba2"})}
                                    onAceptar={onConfirmarEliminar}
                                    onCancelar={onDesSeleccionarNodoHandler}
                                    size={30}
                                    colorTexto={getColor("textos")}
                                    colorBotones={getColor("textos")}
                                ></MensajeAceptarCancelar>
                        </Intersticial>
                    </>
                )
            case 3:

            case 4:
                return (
                    <>
                    <div  className="contenedorVisorLenguaje nonSelectable" style={style}>
            
                    {lineas}
                    
                    </div>
                            <Intersticial 
                            onClose={onCloseIntersticialHandler} 
                            visible={seleccionado}
                            color={getColor("color3")}
                            colorInterior={getColor("color2")}
                            
                            >
                                <PanelReferencias
                                    referencias={lenguaje.operaciones().getArrayNodos()}
                                    onNuevaReferencia = {onNuevaReferencia}
                                    onCancelar={onCloseIntersticialHandler} 
                                ></PanelReferencias>
                        </Intersticial>
                    </>
                )
            default:
                break;
        }
    }
    return (
        <>
            <div onClick={onDesSeleccionarNodoHandler} className="contenedorVisorLenguaje nonSelectable" style={style}>
                {lineas}
            </div>
        </>
    )

}

VisorLenguaje.defaultProps = {
    height : "80%",
    width : "100%",
    editable: true,
    size : 1
};




/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
/*                                                             VisorLineaNodos                                                                     */
/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
const VisorLineaNodos = (props) => {

    const columnas = new Array();
    const conexiones = new Array();
    
    const operacionesDibujo = new OperacionesDibujo(props.dibujo);

    if(props.dibujo != null){
        //Creando nodos
        for (var i = 0; i < props.dibujo.x; i++){
            let id = operacionesDibujo.getIdFuncionalidadInterna(i, props.y);
            const elemento = operacionesDibujo.getElementoDibujo(props.y, i);
            if(elemento != null){
                columnas.push(<VisorNodo key={id} id={id} nodoSeleccionado={props.nodoSeleccionado} onClick={props.onClick} size={props.size} nodo={elemento.nodo} y={props.y} x={i} dibujo={props.dibujo}
                onNuevo={props.onNuevo}
                onEditar={props.onEditar}
                onEliminar={props.onEliminar}
                onGoref={props.onGoref}
                onPrincipal={props.onPrincipal}
                esNodoRecursivo={props.esNodoRecursivo}
                ></VisorNodo>)
            }else{
                columnas.push(<VisorVacio  key={id} id={id} size={props.size} y={props.y} x={i} dibujo={props.dibujo}></VisorVacio>)
            }
        }
        //Creando conexiones
        for (var i = 0; i < props.dibujo.x; i++){
            let id = operacionesDibujo.getIdConexionFuncionalidadInterna(i, props.y);
            conexiones.push(<VisorConexion key={id} id={id} size={props.size} x={i} y={props.y} dibujo={props.dibujo} ></VisorConexion>)
        }
    }

    const style = {
        height: configuracion[props.size].espacioVertical
    }

    return (
        <div key={props.id} className="lineaVisorLenguaje">
            <div className="LineaCentralNodos">
                {columnas}
            </div> 
            <div  style={style} className="InterLineaSuperior interLinea">
                {conexiones}
            </div>
        </div>
    )
}




/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
/*                                                             VisorConexion                                                                     */
/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
const VisorConexion = (props) => {
    const {getColor} = useTema({});

    const operacionesDibujo = new OperacionesDibujo(props.dibujo);

    var marcadoSuperior = esMarcadoSuperior(props.dibujo, props.x, props.y);
    var marcadoInferior = esMarcadoInferior(props.dibujo, props.x, props.y);
    var marcadoIzquierdo = esMarcadoIzquierdo(props.dibujo, props.x, props.y);;
    var marcadoDerecho = esMarcadoDerecho(props.dibujo, props.x, props.y);

    //Estilo para el cuadrante superior Izquierdo
    const styleSI = {}
    //Estilo para el cuadrante superior Derecho 
    const styleSD = {}
    //Estilo para el cuadrante Inferior Izquierdo (Marcado Inferior)
    const styleII = {}
    //Estilo para el cuadrante Inferior Derecho 
    const styleID = {}

    if(marcadoSuperior){
        styleSI.borderRight = "solid 1px " + getColor("conexionesNodos")
    }
    if(marcadoDerecho){
        styleSD.borderBottom = "solid 1px " + getColor("conexionesNodos")
    }
    if(marcadoIzquierdo){
        styleSI.borderBottom = "solid 1px " + getColor("conexionesNodos")
    }
    if(marcadoInferior){
        styleII.borderRight = "solid 1px " + getColor("conexionesNodos")
    }


    function esMarcadoSuperior(dibujo, x , y){
        //Si tiene conexiones se realiza el marcado superior
        const elemento = operacionesDibujo.getElementoDibujo( y, x);
        if(elemento != null){
            return elemento.conexiones.length > 0;
        }
        return false;
    }
    
    function esMarcadoInferior(dibujo, x , y){
        //Si existe alguna conexion que apunte al elemento con y + 1
        const elemento =  operacionesDibujo.getElementoConexion( x, y + 1);
        if(elemento != null){
            return true;
        }
        return false;
    }
    
    function esMarcadoDerecho(dibujo, x , y){
        //Se obienen el elemento que conectan con el nodo inferior y los nodos a la iquierda del inferior
        for(var i = x; i >= 0 ; i--){
            const elemento = operacionesDibujo.getElementoConexion( i, y + 1);
            //Si se encuentra un elemento que conecta a la derecha
            if(elemento != null && elemento.x > x){
                return true;
            }
        }
        
        for(var i = (x + 1); i < dibujo.x ; i++){
            const elemento = operacionesDibujo.getElementoConexion( i, y + 1);
            //Si se encuentra un elemento que conecta a la derecha
            if(elemento != null && elemento.x <= x){
                return true;
            }
        }
        
        return false;
    }
    
    function esMarcadoIzquierdo(dibujo, x , y){
        //Se obienen el elemento que conectan con el nodo inferior y los nodos a la derecha del inferior
        for(var i = (x - 1); i >= 0 ; i--){
            const elemento = operacionesDibujo.getElementoConexion( i, y + 1);
            //Si se encuentra un elemento que conecta a la derecha
            if(elemento != null && elemento.x >= x){
                return true;
            }
        }
        
        
        for(var i = x ; i < dibujo.x ; i++){
            const elemento = operacionesDibujo.getElementoConexion( i, y + 1);
            //Si se encuentra un elemento que conecta a la derecha
            if(elemento != null && elemento.x < x){
                return true;
            }
        }
        
    
        return false;
    }
    
 



    return (
        <div className="visorConexion">
            
            <div  className="conexionSuperior conexionInterior">
                 <div style={styleSI} className="conexionSuperiorIzquierda conexionInteriorInterior"></div>
                 <div style={styleII} className="conexionInferiorIzquierda conexionInteriorInterior"></div>
            </div>
            <div  className="conexionInferior conexionInterior">
                 <div style={styleSD}  className="conexionSuperiorDerecha conexionInteriorInterior"></div>
                 <div style={styleID}  className="conexionInferiorDerecha conexionInteriorInterior"></div>
            </div>
        </div>
    )
}


/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
/*                                                             VisorNodo                                                                     */
/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
const VisorNodo = (props) => {

    const {getColor} = useTema({});

    const [color, setColor] = useState("bordeNodo")
    

    const operacionesDibujo = new OperacionesDibujo(props.dibujo);

    let onClickHandler = (e) =>{
        e.stopPropagation();
        try{
            props.onClick(operacionesDibujo.getIdFuncionalidadInterna(props.x, props.y), props.x,  props.y, e.target);
        }catch(e){

        }
        return false;
    }

    
    const hoverIn = (e) =>{
        setColor("bordeNodoSeleccionado")
    }

    const hoverOut = () =>{
        setColor("bordeNodo")
    }


    // ---------------------------- Estilos ------------------------------------
    const style = {
        width: configuracion[props.size].ancho + "px",
        height: configuracion[props.size].alto + "px",
        marginLeft:configuracion[props.size].espacioHorizontal + "px",
        marginRight: configuracion[props.size].espacioHorizontal + "px",
        fontSize: configuracion[props.size].tamanyoLetra,
        backgroundColor: getColor("colorNodo"),
        border:"solid " + configuracion[props.size].anchoBorde + " " + getColor(color),
        color: getColor("textosNodo"),
        cursor: "pointer"
    }


    const styleEdicion = {
        display : "none",
        height :  "calc(100% - " + configuracion[props.size].alto + "px)",
        width : "100%",
        justifyContent : "space-evenly",
        alignItems : "flex-end"
    }

    const styleExtra = {
        display : "none",
        height :  "calc(100% - " + configuracion[props.size].alto + "px)",
        width : "100%"
    }

    const styleExtraInteriorIzqauierda = {
        height : "100%",
        width : "50%",
        borderRight : "solid 1px " +  getColor("conexionesNodos"),
    }
    const styleExtraInteriorDerecha = {
        height : "100%",
        width : "50%",
        borderRight : "solid 1px transparent",
    }

    // ------------------------------------------------------------------------
   
    let editable = false;
    let lineaEditable = false;
    let referencia = false;
    let tieneHijos = false;
    let esRoot = false;
    if(props.nodoSeleccionado != null){
        editable = (props.nodoSeleccionado.x == props.x && props.nodoSeleccionado.y == props.y );
        lineaEditable = ( props.nodoSeleccionado.y == props.y);
    }
    if(props.nodo != null){
        esRoot = props.nodo.esRoot;
        referencia =  props.nodo.esReferencia;
        tieneHijos =  props.nodo.hijos.length > 0;
        //console.log(props.x + "," + props.y + props.nodo.nombre + " -> tieneHijos: " + tieneHijos)
    }

    // ------------------------------------------------------------------------


    //Nodo root
    if(esRoot){
        style.backgroundColor =  getColor("colorNodoRoot");
        if(color == "bordeNodo"){
            style.border =  getColor("bordeNodoRoot");  
        }
        //style.border =  getColor("bordeNodoRoot");
        style.color =  getColor("textosNodoRoot");
    }

    //Nodos referencia
    if (referencia){
        style.backgroundColor =  getColor("colorNodoReferencia");
        style.color = getColor("textosNodoReferencia");
        if(color == "bordeNodo"){
            style.border = "solid " + configuracion[props.size].anchoBorde + " " + getColor("bordeNodoReferencia");
        }
        
    }


    //Nodos editable (Nodo seleccionado)
    if(editable){
        style.border = "solid " + configuracion[props.size].anchoBorde + " " + getColor("bordeNodoSeleccionado");
        style.height = "70px";
        styleEdicion.display = "flex";
    }

    //linea inferior para los nodos de la misma linea que tienen hijos
    if(lineaEditable && !editable && tieneHijos){        
        styleExtra.display = "flex";
    }


    const id = operacionesDibujo.getIdFuncionalidadInterna(props.x, props.y);

    return (
        <div>
            <div key={props.id} onClick={onClickHandler} style={style} className={"visorNodoExterior visorNodoValido visorNodoValido" + id} onMouseEnter={hoverIn} onMouseLeave={hoverOut} >
                <div id={id}className="visorNodoInterior">{props.nodo.nombre}</div>
                <div style={styleEdicion} className="edicionNodo">
                    <BarraIconosVisorNodo
                            nuevo = {!referencia}
                            onNuevo = {props.onNuevo}

                            editar = {!referencia  && !esRoot}
                            onEditar = {props.onEditar}

                            //Si es root no se puede eliminar
                            //Si es referencia se puede eliminar
                            //Si no es referencia y no tiene hijos se puede eliminar
                            eliminar = {((!referencia  && !tieneHijos) || (referencia) ) && !esRoot}
                            onEliminar = {props.onEliminar}

                            goref  = {referencia}
                            onGoref = {props.onGoref}

                            principal = {referencia && !props.esNodoRecursivo}
                            onPrincipal = {props.onPrincipal}
                    
                    ></BarraIconosVisorNodo>
                </div>
            </div>
            <div style={styleExtra} className="espacioExtraNodo">
                    <div style={styleExtraInteriorIzqauierda}>

                    </div>
                    <div  style={styleExtraInteriorDerecha}>

                    </div>
            </div>

            
        </div>
    )
}




const BarraIconosVisorNodo = (props) =>  {
    const {getColor} = useTema({});

    const style = {
        height: "100%",
       width: "100%",
    }

    const iconos = [];

    if(props.nuevo){
        iconos.push( <Icono key="add" icon="add" onClick={props.onNuevo}  size="20"  color={getColor("textosNodo")} colorh={getColor("bordeNodoSeleccionado")}></Icono>)
    }

    if(props.editar){
        iconos.push( <Icono key="edit" icon="edit" onClick={props.onEditar}  size="20"  color={getColor("textosNodo")} colorh={getColor("bordeNodoSeleccionado")}></Icono>)
    }

    if(props.goref){
        iconos.push( <Icono key="farrow" icon="farrow" onClick={props.onGoref}  size="20"  color={getColor("textosNodo")} colorh={getColor("bordeNodoSeleccionado")}></Icono>)
    }

    if(props.principal){
        iconos.push( <Icono key="room" icon="room" onClick={props.onPrincipal}  size="20"  color={getColor("textosNodo")} colorh={getColor("bordeNodoSeleccionado")}></Icono>)
    }
    
    if(props.eliminar){
        iconos.push( <Icono key="delete" icon="delete" onClick={props.onEliminar}  size="20"  color={getColor("textosNodo")} colorh={getColor("bordeNodoSeleccionado")}></Icono>)
    }

    return (
       <>
        {iconos}
       </>
    )
}
BarraIconosVisorNodo.defaultProps = {
    nuevo : false,
    editar : false,
    eliminar : false,
    goref : false,
    principal : false,
}




/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
/*                                                                VisorVacio                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
const VisorVacio = (props) => {
    const operacionesDibujo = new OperacionesDibujo(props.dibujo);

    const style = {
        width: configuracion[props.size].ancho + "px",
        height: configuracion[props.size].alto+ "px",
        marginLeft:configuracion[props.size].espacioHorizontal + "px",
        marginRight: configuracion[props.size].espacioHorizontal + "px",
        fontSize: configuracion[props.size].tamanyoLetra,
    }

    return (
        <div  key={props.id} style={style} id={operacionesDibujo.getIdFuncionalidadInterna(props.x, props.y)} className="visorNodoExterior"></div>
    )
}

/*-------------------------------------------------------------------------------------------------------------------------------------------------*/
/*                                                                COMMONS                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------------------------------*/


const configuracion = {
    0:{ 
        ancho: 100,
        alto: 20,
        espacioVertical:10,
        espacioHorizontal:2,
        anchoBorde:"2px",
        anchoConexiones:"2px",
        tamanyoLetra:"8px",
        alturaExpandido:"200px"
    },
    1:{ 
        ancho: 150,
        alto: 30,
        espacioVertical:20,
        espacioHorizontal:4,
        anchoBorde:"2px",
        anchoConexiones:"2px",
        tamanyoLetra:"12px",
        alturaExpandido:"400px"
    },
    2:{ 
        ancho: 200,
        alto: 40,
        espacioVertical:30,
        espacioHorizontal:6,
        anchoBorde:"2px",
        anchoConexiones:"2px",
        tamanyoLetra:"16px",
        alturaExpandido:"600px"
    },
    3:{ 
        ancho: 250,
        alto: 50,
        espacioVertical:40,
        espacioHorizontal:8,
        anchoBorde:"2px",
        anchoConexiones:"2px",
        tamanyoLetra:"20px",
        alturaExpandido:"800px"
    },
    4:{ 
        ancho: 300,
        alto: 60,
        espacioVertical:50,
        espacioHorizontal:10,
        anchoBorde:"2px",
        anchoConexiones:"2px",
        tamanyoLetra:"24px",
        alturaExpandido:"1000px"
    }
}

export default VisorLenguaje