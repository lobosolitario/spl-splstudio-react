import React, {useState, useEffect, useMemo} from 'react'
import Titulo from '../../componentes-atomicos/Titulo/Titulo'
import BotoneraInferior from '../../componentes-botones/BotoneraInferior/BotoneraInferior'
import LayoutContenedorBotoneras from '../../componentes-layout/LayoutContenedorBotoneras/LayoutContenedorBotoneras'
import {useLiterales} from '../../../hooks/useLiterales'
import Separador from '../../componentes-atomicos/Separador/Separador'
import PARAMETROS from '../../../config/PARAMETROS'
import Mensaje from '../Mensaje/Mensaje'

const MensajeAceptarCancelar = (props) =>  {
    const{getLiteral} = useLiterales();



    const onBotonHandler = (accion) => {
        console.log(accion)
        switch (accion) {
            case "Aceptar":
                props.onAceptar();
            case "Cancelar":
                props.onCancelar();
                break;
            default:
                break;
        }
    }

    
    const botonesInferiores = [
        {
            id : "Cancelar",
            icono: "",
            texto : getLiteral("commonsCancelar"),
        },
        {
            id : "Aceptar",
            icono: "",
            texto : getLiteral("commonsAceptar"),
        },
        
    ]

    return (
        <LayoutContenedorBotoneras
        titulo={<></>}
        separadorSuperior={ <Separador altura={props.separacionSuperior} color="none"></Separador>}
        sepraadorInferior={<Separador altura={props.separacionInferior} color="none"></Separador>}
        botoneraInferior={<BotoneraInferior colorBoton={props.colorBotones} botones={botonesInferiores} onClick={onBotonHandler}></BotoneraInferior>}
        altoContenedorTitulo={0}
        panel={<Mensaje mensaje={props.mensaje} color={props.colorTexto}></Mensaje>}
    >
        
    </LayoutContenedorBotoneras>
    )
}

MensajeAceptarCancelar.defaultProps = {
    colorTexto : "white",
    colorBotones : "blue",
    mensaje : "",
    size : 30
}


export default MensajeAceptarCancelar
