
import React, { Component, useState } from 'react'
import { Resizable, ResizableBox } from 'react-resizable';
import {Link, useParams} from 'react-router-dom';
import {useEnrutador} from '../../../hooks/useEnrutador'

const VisorDoblePanel =  (props) => {

    const {layout, panel, panelLateral, menu, menuLateral} = useEnrutador()

    const [ancho, setAncho] = useState(0);

    const styleContenedorPrincipal = {
        display: "block",
        height: "100%",
        width: "100%"
    }

    const stylePanelLateral = {
        height: "100%",
        float: "left"
    }
    

    const styleContenedorSecundario = {
        display: "flex",
        height: "100%"
    }
    
    const onResizeHandler = (ancho) => {
        //setAncho(ancho)
    }

    let menuLateralWithProps = <></>;



   let {id} = useParams()
   
   return(
   <div id="panelDobleVisor" style={styleContenedorPrincipal}>
        <div id="panelLateral"  style={stylePanelLateral}>
            { React.cloneElement(menuLateral, { onResize : onResizeHandler })}
        </div>
        <div id="panelCentral" style={styleContenedorSecundario}>
            { React.cloneElement(panel, { anchoMenuLateral : ancho })}
       </div>
    </div>)
}


export default VisorDoblePanel