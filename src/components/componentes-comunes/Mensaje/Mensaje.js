import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from "../../../config/PARAMETROS"

const Mensaje = (props) =>  {
    const style = {
        display : "flex",
        justifyContent:"center",
        alignItems:"center",
        height: "100%",
        width: "100%",
        color: props.color,
        fontSize : props.size + "px",
        textAlign:"center"
    }

    return (
       <div style={style}>
           {props.mensaje}
       </div>
    )
}

Mensaje.defaultProps={
    size : PARAMETROS.varios.tamanyoTextoMensajes
}


export default Mensaje