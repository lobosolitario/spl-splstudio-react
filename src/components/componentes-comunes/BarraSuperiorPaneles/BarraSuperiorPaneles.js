import React, { Component } from 'react'
import PARAMETROS from '../../../config/PARAMETROS.js'
import {useTema} from '../../../hooks/useTema';

const BarraSuperiorPaneles = (props) => {
    const {getColor} = useTema({});
    
    const style = {
        height: PARAMETROS.editor.altoBarraMenu + "px",
        width : "100%",
        backgroundColor: getColor("color3"),
        boxSizing : "border-box"
    }

    const styleInterno = {
        display : "flex",
        justifyContent : "flex-end",
        alignItems: "center",
        height: "100%",
        width: "100%",
        paddingLeft: "20px",
        paddingRight: "20px",
        boxSizing : "border-box"
     }
 

    return (
        <div className="barraSuperiorPaneles" style={style}>
            <div style={styleInterno}>
                {props.children}
            </div>
        </div>
    )
    
}

export default BarraSuperiorPaneles;