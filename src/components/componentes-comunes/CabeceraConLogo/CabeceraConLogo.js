import React, { Component } from 'react'
import Logo from '../../componentes-iconos/Logo/Logo';

import PARAMETROS from '../../../config/PARAMETROS.js';

const CabeceraConLogo = (props) =>  {
    const style = {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "start",
        paddingLeft: "20px",
        paddingRigth: "20px",
        height: "100%",
        width: "100%",
        boxSizing: "border-box"
    }
    

    const styleSeparador = {
        height: "100%",
        width: "100%",
    }
    
    return (
        <div style={style}>
            <Logo size={PARAMETROS.editor.altoCabecera - 10} ></Logo>
            <div style={styleSeparador}>
                {props.children}
            </div>
        </div>
    )
    
}

export default CabeceraConLogo