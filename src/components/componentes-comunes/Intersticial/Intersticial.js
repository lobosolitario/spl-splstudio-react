import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS';
import Icono from '../../componentes-iconos/Icono/Icono';


const Intersticial = (props) =>  {

    let dsp = "none";
    if(props.visible){
        dsp = "block";
    }

    
    var txtMargen = (props.margen) + "%";
    var txtMargenInverso = (100 - (props.margen * 2)) + "%";

    const style = {
        display: dsp,
        position:"relative",
        height: "100%",
        width: "100%",
        top : "-100%",
        transition: "opacity 2s"
    }

    const styleInterior = {
        display: "flex",
        flexDirection : "column",
        alignItems :"center",
        position: "relative",
        backgroundColor: props.colorInterior,
        opacity: 1,
        height: txtMargenInverso,
        width: txtMargenInverso,
        maxWidth : PARAMETROS.intersticiales.maxAncho + "px",
        marginLeft: "auto",
        marginRight: "auto",
        left: 0,
        right: 0,
        top: txtMargen,
        zIndex: 50, 
        minWidth: PARAMETROS.intersticiales.minAncho + "px"
    }
    
    const styleIntersticial = {
        backgroundColor: props.color,
        opacity: 0.8,
        height: "100%",
        width: "100%",
        position: "relative",
        top: "-" + txtMargenInverso,
        zIndex: 49
    }

    const styleBarraAspa = {
        display: "flex",
        height: props.aspa + "px", //PARAMETRIZAR
        width: "100%",
        flexDirection: "column",
        alignItems: "flex-end",
        padding:"5px",
        boxSizing:" border-box"
    }

    const styleContenedor = {
        display:"flex",
        alignItems: "center",
        justifyContent :"center",
        height: "calc(100% - " + (props.aspa  + 20) + "px)",
        width:  "calc(100% - " + 20 + "px)",
        //overflow:"auto"
    }
    
    return (
        
        <div className="Intersticial" style={style}>
            <div style={styleInterior}>
                <div style={styleBarraAspa}>
                    <Icono icon="close" onClick={props.onClose} /*onClickParams={props.menu}*/ size="25"  color={props.colorX} colorh={props.colorX}></Icono>
                </div>
                <div style={styleContenedor}>
                    {props.children}
                </div>
            </div>
            <div style={styleIntersticial}  onClick={props.onClose}>

            </div>
        </div>
    )
    
}


Intersticial.defaultProps = {
    color : "currentcolor",
    colorInterior: "white",
    colorX:"white",
    opacidad : 80,
    visible: false,
    margen : 8,
    aspa: 30,

    
  };

export default Intersticial

