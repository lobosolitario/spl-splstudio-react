import React, { Component } from 'react'
import SelectorColores from '../../../utils/SelectorColores.js'
import Combobox from '../../componentes-atomicos/Combobox/Combobox.js'

import {TemaProvider, useTema} from '../../../hooks/useTema'

export function SelectorTema(props){

    const {getColor, tema, setTema, temas} = useTema({});

    function  onChangeHandler(valor){
        setTema(valor)
    }


    const sc = new SelectorColores();


          
        
    return (
        <Combobox 
        opciones={temas} 
        valor={tema} 
        onChange={onChangeHandler} 
        nombre="temas"/>
    )
    
}

