import React, {useState, useEffect, useMemo} from 'react'
import {TemaProvider, useTema} from '../../../hooks/useTema'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'
import Combobox from '../../componentes-atomicos/Combobox/Combobox.js'
import ComboboxMini from '../../componentes-atomicos/ComboboxMini/ComboboxMini'
import SelectorIdioma from '../SelectorIdioma/SelectorIdioma'
const BarraIdioma = (props) =>  {

    const styleBarraSuperior = {
        display :"flex",
        justifyContent: "end",
        width: "100%",
        padding: "10px",
        boxSizing: "border-box",
    }

    return (
        <div style={styleBarraSuperior}>
            <SelectorIdioma mini={true}></SelectorIdioma>
        </div>
    )

    
}


export default  BarraIdioma;
