import React, {useState, useEffect, useMemo} from 'react'
import {TemaProvider, useTema} from '../../../hooks/useTema'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'
import Combobox from '../../componentes-atomicos/Combobox/Combobox.js'
import ComboboxMini from '../../componentes-atomicos/ComboboxMini/ComboboxMini'
const SelectorIdioma = (props) =>  {
    const { getLiteral, idioma, setIdioma, getIdiomas} = useLiterales();
    
    const onChangeHandler = valor => {
        setIdioma(valor)    
    }

    
    var opciones = getIdiomas();

    
    if(props.mini){
        return (
            <ComboboxMini opciones={opciones} valor={idioma} onChange={onChangeHandler} nombre="idiomas">
            </ComboboxMini>
        )
    }else{
        return (
            <Combobox opciones={opciones} valor={idioma} onChange={onChangeHandler} nombre="idiomas">
            </Combobox>
        )
    }

    
}

SelectorIdioma.defaultProps = {
    mini : false
}

export default  SelectorIdioma;
