import React, {useState, useEffect, useMemo} from 'react'
import { useHistory } from 'react-router-dom';
import PARAMETROS from '../../../config/PARAMETROS'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import SeparadorHorizontalBotonera from '../SeparadorHorizontalBotonera/SeparadorHorizontalBotonera';

const BotoneraMenuLateralPlantillas = (props) =>  {
    const {getColor} = useTema()
    const {guardaProyecto} = useProyecto()
    const history = useHistory();

    const onNuevaPlantilla = () => {
        history.push('/editor/plantillas/nueva')
    }
    const onUploadFile = () => {
        history.push('/editor/plantillas/subir')
    }


    return (
       <>   
           <Icono icon="addfile" onClick={onNuevaPlantilla}  size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
           <SeparadorHorizontalBotonera ancho={props.espaciado}></SeparadorHorizontalBotonera>

           <Icono icon="uploadfile" onClick={onUploadFile}  size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
       </>
    )
}

BotoneraMenuLateralPlantillas.defaultProps = {
    espaciado : PARAMETROS.botoneras.espaciadoBotonerasPaneles,
    tamanyoIconos : PARAMETROS.botoneras.tamanyoIconos 
}

export default BotoneraMenuLateralPlantillas
