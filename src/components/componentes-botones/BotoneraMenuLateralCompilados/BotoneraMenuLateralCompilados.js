import React, {useState, useEffect, useMemo} from 'react'
import { useHistory } from 'react-router-dom';
import PARAMETROS from '../../../config/PARAMETROS'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import SeparadorHorizontalBotonera from '../SeparadorHorizontalBotonera/SeparadorHorizontalBotonera';

const BotoneraMenuLateralCompilados = (props) =>  {
    const {getColor} = useTema()
    const {getURLDescarga} = useProyecto()
    const history = useHistory();

    const onDownload = () => {
        const url = getURLDescarga();
        window.location = url
    }



    return (
       <>   
           <Icono icon="clouddown" onClick={onDownload}  size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
       </>
    )
}

BotoneraMenuLateralCompilados.defaultProps = {
    espaciado : PARAMETROS.botoneras.espaciadoBotonerasPaneles,
    tamanyoIconos : PARAMETROS.botoneras.tamanyoIconos 
}

export default BotoneraMenuLateralCompilados
