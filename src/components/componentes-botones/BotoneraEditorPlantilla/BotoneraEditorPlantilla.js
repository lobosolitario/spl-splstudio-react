import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import BotonGuardar from '../BotonGuardar/BotonGuardar'
import BotonCompilar from '../BotonCompilar/BotonCompilar'
import SeparadorHorizontalBotonera from '../SeparadorHorizontalBotonera/SeparadorHorizontalBotonera'
import { useHistory } from 'react-router-dom';

const BotoneraEditorPlantilla = (props) =>  {

    const {getColor} = useTema()
    const {guardaProyecto} = useProyecto()
    const history = useHistory();


    
    const onSettingsHandler = () => {
        history.push("/editor/plantillas/opciones")
    }


    const onDeleteHandler = () => {
        history.push("/editor/plantillas/eliminar")
    }


    return (
       <>   
           <BotonCompilar   size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></BotonCompilar>


           <SeparadorHorizontalBotonera ancho={(props.espaciado * 2)} espaciador={true} color={getColor("iconos1")}></SeparadorHorizontalBotonera>

           <Icono icon="settings"  onClick={onSettingsHandler} size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
           <SeparadorHorizontalBotonera ancho={props.espaciado}></SeparadorHorizontalBotonera>
           <Icono icon="delete"  onClick={onDeleteHandler} size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>

           <SeparadorHorizontalBotonera ancho={(props.espaciado * 2)} espaciador={true} color={getColor("iconos1")}></SeparadorHorizontalBotonera>

           <BotonGuardar   size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></BotonGuardar>


       </>
    )
}

BotoneraEditorPlantilla.defaultProps = {
    espaciado : PARAMETROS.botoneras.espaciadoBotonerasPaneles,
    tamanyoIconos : PARAMETROS.botoneras.tamanyoIconos
}


export default BotoneraEditorPlantilla
