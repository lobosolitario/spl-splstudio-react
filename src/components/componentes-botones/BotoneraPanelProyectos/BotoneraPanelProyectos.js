import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import SeparadorHorizontalBotonera from '../SeparadorHorizontalBotonera/SeparadorHorizontalBotonera'


const BotoneraPanelProyectos = (props) =>  {

    const {getColor} = useTema()
    const {guardaProyecto} = useProyecto()




    return (
       <>
           <Icono icon="add" onClick={props.onNuevoProyecto}  size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
       </>
    )
}

BotoneraPanelProyectos.defaultProps = {
    espaciado : PARAMETROS.botoneras.espaciadoBotonerasPaneles,
    tamanyoIconos : PARAMETROS.botoneras.tamanyoIconos 
}


export default BotoneraPanelProyectos
