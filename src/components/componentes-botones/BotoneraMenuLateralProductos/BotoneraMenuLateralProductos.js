import React, {useState, useEffect, useMemo} from 'react'
import { useHistory } from 'react-router-dom';
import PARAMETROS from '../../../config/PARAMETROS'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import SeparadorHorizontalBotonera from '../SeparadorHorizontalBotonera/SeparadorHorizontalBotonera';

const BotoneraMenuLateralProductos = (props) =>  {
    const {getColor} = useTema()
    const {guardaProyecto} = useProyecto()
    const history = useHistory();

    const onNuevaPlantilla = () => {
        history.push('/editor/productos/nueva')
    }



    return (
       <>   
           <Icono icon="addfile" onClick={onNuevaPlantilla}  size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
       </>
    )
}

BotoneraMenuLateralProductos.defaultProps = {
    espaciado : PARAMETROS.botoneras.espaciadoBotonerasPaneles,
    tamanyoIconos : PARAMETROS.botoneras.tamanyoIconos 
}

export default BotoneraMenuLateralProductos
