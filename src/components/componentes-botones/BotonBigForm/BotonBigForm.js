import React, {useState, useEffect, useMemo} from 'react'


import './BotonBigForm.css';

const BotonBigForm = (props) =>  {
    
    const style = {
        background: props.color,
        color: props.colorTexto,
    }


    return (
        <button onClick={props.onClick} style={style} className="btnBigForm" type="button">{props.texto}</button> 
    );
    
}

BotonBigForm.defaultProps = {
    texto:"",
    color:"white",
    colorh:"",
    colorTexto :"black",
    ancho:"314px",
    alto:"50px",
}

export default BotonBigForm
