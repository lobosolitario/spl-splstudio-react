import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'


const SeparadorHorizontalBotonera = (props) => {
    
    const style = {
        display : "flex",
        height : "100%",
        width: props.ancho + "px"
    }

    const styleL = {
        height : "100%",
        width:"50%"
        
    }

    const styleR = {
        height : "100%",
        width:"calc(50% - 1px)",
        borderLeft : "solid 1px " + props.color,
        opacity : props.opacidad + "%"
    }


    if(props.espaciador){
        return(
            <div style={style}>
                <div style={styleL}></div>
                <div style={styleR}></div>
            </div>
        )
    }
    
    return(
        <div style={style}>
        </div>
    )

   
}

SeparadorHorizontalBotonera.defaultProps = {
    ancho :  PARAMETROS.botoneras.espaciadoBotonerasPaneles,
    espaciador : false,
    color : "white",
    opacidad : 30
}

export default SeparadorHorizontalBotonera