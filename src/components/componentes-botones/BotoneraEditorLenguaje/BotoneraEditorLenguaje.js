import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import BotonGuardar from '../BotonGuardar/BotonGuardar'
import SeparadorHorizontalBotonera from '../SeparadorHorizontalBotonera/SeparadorHorizontalBotonera'


const BotoneraEditorLenguaje = (props) =>  {

    const {getColor} = useTema()
    const {guardaProyecto} = useProyecto()


    const onGuardarHandler = () => {
        guardaProyecto();
    }

    const onGuardarTodoHandler = () => {
        
    }


    
    const onAcercarHandler = () => {
        
    }


    const onAlejarHandler = () => {
        
    }

    return (
        <>
            <SeparadorHorizontalBotonera ancho={(props.espaciado * 2)} espaciador={true} color={getColor("iconos1")}></SeparadorHorizontalBotonera>
            <BotonGuardar   size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></BotonGuardar>
        </>
    )

    //Opcion con botones acerar y alejar
    return (
       <>   
           <Icono icon="zoomin" onClick={onGuardarHandler}  size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
           <SeparadorHorizontalBotonera ancho={props.espaciado}></SeparadorHorizontalBotonera>
           <Icono icon="zoomout" onClick={onGuardarHandler}  size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>

           <SeparadorHorizontalBotonera ancho={(props.espaciado * 2)} espaciador={true} color={getColor("iconos1")}></SeparadorHorizontalBotonera>
           <BotonGuardar   size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></BotonGuardar>


       </>
    )
}

BotoneraEditorLenguaje.defaultProps = {
    espaciado : PARAMETROS.botoneras.espaciadoBotonerasPaneles,
    tamanyoIconos : PARAMETROS.botoneras.tamanyoIconos
}


export default BotoneraEditorLenguaje
