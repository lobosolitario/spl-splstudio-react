import BotonGenericoLateral from "../BotonGenericoLateral/BotonGenericoLateral";

const MiniBotoneraLateral = (props) =>  {


    const style = {
        display:"flex",
        width: "100%",
        height: "100%",
        backgroundColor: props.color,
        overflow : "hidden",
        flexDirection: "column",
    }



    const botones = props.botones.map((boton, index) =>
    <BotonGenericoLateral 
        key={"MiniBotoneraLateralBoton" + index} 
        onClick={props.onClick} 
        boton={boton}
        color={props.color}
        ancho = {props.anchoBotones}
        alto = {props.altoBotones}
        estilo  = {props.estilo}
        seleccionado  = {props.seleccion == boton.id}
        color  ={props.color}
        colorh  = {props.colorh}
        colorIconos  = {props.colorIconos}
        colorIconosh  = {props.colorIconosh}
    ></BotonGenericoLateral>

    );

    return (
        <div style={style} className="nonSelectable">
         {botones}
        </div>
    )
    
}

MiniBotoneraLateral.defaultProps = {
    color : "none",
    colorh: "none",
    colorIconos:"none",
    colorIconosh: "none",
};


export default MiniBotoneraLateral