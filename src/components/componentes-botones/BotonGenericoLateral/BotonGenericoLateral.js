import React, {useState, useEffect, useMemo} from 'react'

import Icono from "../../componentes-iconos/Icono/Icono";

const BotonGenericoLateral = (props) =>  {


    const onBotonHandler = () => {
        try{
            props.onClick(props.boton.id);
        }catch(error){

        }
    }
    
    const style = {
        display:"flex",
        width: props.ancho,
        height: props.alto,
        color: props.color,
        alignItems : "center",
        justifyContent : "center"
    }
    
    let colorIcono = props.colorIconos;
    if(props.seleccionado){
        style.backgroundColor = props.colorh;
        colorIcono = props.colorIconosh;
    }

    return (
        <div style={style} className="botonGenerico nonSelectable">
            
            <Icono icon={props.boton.icono} onClick={onBotonHandler} /*onClickParams={props.menu}*/ size="25"  color={colorIcono} colorh={props.colorIconosh}></Icono>
        
        </div>
    )
    
}

BotonGenericoLateral.defaultProps = {
    color : "none",
    colorh: "orange",
    colorIconos:"",
    colorIconosh: "",
    boton : null,
    ancho : "100%",
    alto : "50px",
    estilo : 0,
    seleccionado : false
};

export default BotonGenericoLateral