import React, {useState, useEffect, useMemo} from 'react'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import { useHistory } from 'react-router-dom';

const ToggleBotonCirculo = (props) =>  {
    const history = useHistory();

    const {getColor} = useTema()
    const {elminarCompilacion} = useProyecto()
  
  

    const style = {
        borderRadius : "50px",
        padding : props.padding + "px"
    }

    if(props.forzar){
        style.backgroundColor = getColor(props.colorBack)
        return (
            <div style={style}>
                <Icono cursor="default" icon={props.icon} onClick={props.onClick}  size={props.size}  color={getColor(props.colorForzado)} colorh={getColor(props.colorForzado)}></Icono>
            </div>
        );
    }

    if(props.activado){
        style.backgroundColor = getColor(props.colorBack)
        return (
            <div style={style}>
                <Icono icon={props.icon} onClick={props.onClick}  size={props.size}  color={getColor(props.colorh)} colorh={getColor(props.colorh)}></Icono>
            </div>
        );
    }

    style.backgroundColor = "transparent"
    return (
        <div style={style}>
            <Icono icon={props.icon} onClick={props.onClick}  size={props.size}  color={getColor(props.color)} colorh={getColor(props.colorh)}></Icono>
        </div>
    );
    
}

ToggleBotonCirculo.defaultProps = {
    icon : "",
    size : 20,
    activado : true,
    color: "iconos1",
    colorh : "iconos2",
    colorBack: "color2",
    colorForzado : "error",
    padding : 4,
    forzar : false
}

export default ToggleBotonCirculo
