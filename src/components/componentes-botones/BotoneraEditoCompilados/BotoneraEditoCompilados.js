import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import BotonGuardar from '../BotonGuardar/BotonGuardar'
import SeparadorHorizontalBotonera from '../SeparadorHorizontalBotonera/SeparadorHorizontalBotonera'
import { useHistory } from 'react-router-dom';
import BotonCompilar from '../BotonCompilar/BotonCompilar'
import ToggleBotonCirculo from '../ToggleBotonCirculo/ToggleBotonCirculo'

const BotoneraEditoCompilados = (props) =>  {

    const {getColor} = useTema()
    const {guardaProyecto} = useProyecto()
    const history = useHistory();


    
    const onPlantillaHandler = () => {
        history.push("/editor/plantillas/editar")
    }

    const onProductoHandler = () => {
        history.push("/editor/productos/editar")
    }


    const onToggleViewCompiladosHandler = () => {
        if(props.verBotonBugs){
            history.push("/editor/bugs")
        }else{
            history.push("/editor/compilados")
        }
    }


    let botonBugs =  <Icono icon="play"  onClick={onToggleViewCompiladosHandler} size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
    if(props.verBotonBugs){
         botonBugs =  <Icono icon="bug"  onClick={onToggleViewCompiladosHandler} size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
    }

    return (
       <>   


           <BotonCompilar   size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></BotonCompilar>

           <SeparadorHorizontalBotonera ancho={(props.espaciado * 2)} espaciador={true} color={getColor("iconos1")}></SeparadorHorizontalBotonera>



           <ToggleBotonCirculo forzar={props.erroCompilacion} onClick={onToggleViewCompiladosHandler} activado={!props.verBotonBugs} icon="bug"></ToggleBotonCirculo>

           <SeparadorHorizontalBotonera ancho={(props.espaciado * 2)} espaciador={true} color={getColor("iconos1")}></SeparadorHorizontalBotonera>

        
           <Icono icon="bag"  onClick={onProductoHandler} size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>
           <SeparadorHorizontalBotonera ancho={props.espaciado}></SeparadorHorizontalBotonera>
           <Icono icon="bagcode"  onClick={onPlantillaHandler} size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></Icono>

       </>
    )
}

BotoneraEditoCompilados.defaultProps = {
    espaciado : PARAMETROS.botoneras.espaciadoBotonerasPaneles,
    tamanyoIconos : PARAMETROS.botoneras.tamanyoIconos,
    verBotonBugs : true,
    erroCompilacion : false,
}


export default BotoneraEditoCompilados
