import PARAMETROS from "../../../config/PARAMETROS";
import BotonGenerico from "../BotonGenerico/BotonGenerico";

const BotoneraInferior = (props) =>  {


    const style = {
        display:"flex",
        width: "100%",
        height: "100%",
        justifyContent: "space-around",
        alignItems: props.alineamiento,
        backgroundColor: props.color
    }


    const botones = props.botones.map((boton, index) =>
    <BotonGenerico 
        key={"MiniBotoneraLateralBoton" + index} 
        onClick={props.onClick} 
        boton={boton}
        color={props.colorBoton}
        onClick={props.onClick}
        estilo  = {props.estilo}
    ></BotonGenerico>

    );
    return (
        <div style={style} className="botoneraInferior nonSelectable">
            {botones}
            
        </div>
    )
    
}

BotoneraInferior.defaultProps = {
    color : "transparent",
    colorBoton: "red",
    colorBotonh: "blue",
    estilo : 0,
    alineamiento : "center"
};

export default BotoneraInferior