import React, {useState, useEffect, useMemo} from 'react'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'

import './BotonGuardar.css'

const BotonGuardar = (props) =>  {
    const GUARDADO = 0
    const GUARDANDO = 1
    const MODIFICADO = 2
    const ERROR = 3


    const {getColor} = useTema()
    const {guardaProyecto, proyectoModificado} = useProyecto()
  
    const onGuardarHandler = () => {
        guardaProyecto();
    }


    
    switch (proyectoModificado) {
        case GUARDADO:

            const styleGuardado={
                opacity : "50%"
            }
            return (
                <span style={styleGuardado}>   
                        <Icono cursor="default" icon={props.icon}  size={props.size}  color={props.color} colorh={props.color}></Icono>
                </span>
            );


        case GUARDANDO:
            const styleContenedor={
                display : "flex",
                justifyContent : "center",
                alignItems : "center",
                height : props.size + "px",
                width : props.size + "px"
             }    
            const styleGuardando={
                opacity : "50%",
                borderTop: "3px solid " + props.color,
                borderLeft: "3px solid " + props.color,
                borderRadius : "100%",
                height : "10px",
                width : "10px"
             }
            return (
                <span style={styleContenedor} >
                    <div style={styleGuardando} className="botonGuardarRotacion">   
                            
                    </div>
                </span>
            );

        case ERROR:
            const styleError={
                opacity : "100%",
            }
            return (
                <span style={styleError}>   
                        <Icono icon={props.icon} onClick={onGuardarHandler}  size={props.size}  color={getColor("error")} colorh={props.colorh}></Icono>
                </span>
            );

        case MODIFICADO:
        default:
            const styleModificado={
                opacity : "100%"
            }
            return (
                <span style={styleModificado}>   
                        <Icono icon={props.icon} onClick={onGuardarHandler}  size={props.size}  color={props.color} colorh={props.colorh}></Icono>
                </span>
            );
    
        
    }
    

}

BotonGuardar.defaultProps = {
    icon : "cloudup"
}

export default BotonGuardar
