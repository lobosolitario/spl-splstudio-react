import PARAMETROS from "../../../config/PARAMETROS";

const BotonGenerico = (props) =>  {

    const onBotonHandler = (e) => {
        try{
            props.onClick(props.boton.id);
        }catch(error){

        }
    }

    const style = {
        display:"flex",
        width:  props.ancho + "px",
        height: props.alto + "px",
        color: props.color,
        cursor: "pointer",
        overflow : "hidden",
        border: "solid 1px " + props.color,
        justifyContent : "center",
        alignItems : "center"
    }

    return (
        <div style={style} onClick={onBotonHandler} className="botonGenerico nonSelectable">
            {props.boton.texto}
        </div>
    )
    
}

BotonGenerico.defaultProps = {
    color : "none",
    boton : null,
    ancho :  PARAMETROS.botones.ancho,
    alto : PARAMETROS.botones.alto,
    estilo : 0
};

export default BotonGenerico