import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import BotonGuardar from '../BotonGuardar/BotonGuardar'
import SeparadorHorizontalBotonera from '../SeparadorHorizontalBotonera/SeparadorHorizontalBotonera'
import { useHistory } from 'react-router-dom';

const BotoneraSoloGuardado = (props) =>  {

    const {getColor} = useTema()


    return (
       <>

           <SeparadorHorizontalBotonera ancho={(props.espaciado * 2)} espaciador={true} color={getColor("iconos1")}></SeparadorHorizontalBotonera>

           <BotonGuardar   size={props.tamanyoIconos}  color={getColor("iconos1")} colorh={getColor("iconos2")}></BotonGuardar>


       </>
    )
}

BotoneraSoloGuardado.defaultProps = {
    espaciado : PARAMETROS.botoneras.espaciadoBotonerasPaneles,
    tamanyoIconos : PARAMETROS.botoneras.tamanyoIconos
}


export default BotoneraSoloGuardado
