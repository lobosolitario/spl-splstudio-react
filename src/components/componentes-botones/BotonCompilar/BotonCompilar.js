import React, {useState, useEffect, useMemo} from 'react'
import { useProyecto } from '../../../hooks/useProyecto'
import { useTema } from '../../../hooks/useTema'
import Icono from '../../componentes-iconos/Icono/Icono'
import { useHistory } from 'react-router-dom';

const BotonCompilar = (props) =>  {
    const history = useHistory();



    const {getColor} = useTema()
    const {elminarCompilacion} = useProyecto()
  
    const onClickHandler = () => {
        elminarCompilacion()
        history.push("/editor/compilando")
    }


    return (
        <Icono icon="play2" onClick={onClickHandler}  size={props.size}  color={getColor("colorBotonCompilar")} colorh={getColor("colorBotonCompilarSeleccionado")}></Icono>
    );
    

}

export default BotonCompilar
