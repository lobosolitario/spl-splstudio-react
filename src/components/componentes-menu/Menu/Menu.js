import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS';
import { useTema } from '../../../hooks/useTema';
import MenuEstructura from '../../../model/Menu/Menu'

const Menu = (props) =>  {
    const [seleccionado, setSeleccionado] = useState(null);

    const onClickMenuNivel1 = (evento, id) =>{
        evento.stopPropagation();
        setSeleccionado(id)
    }

    const onClickMenuNivel2 = (evento, id) =>{
        cerrarMenu()
    }

    const onHoverInMenuNivel1 = (evento, id) =>{
        if(seleccionado != null &&  seleccionado != id){
            evento.stopPropagation();
            setSeleccionado(id)
        }

    }

    
    const onHoverOutMenuNivel1 = (evento, id) =>{

    }

    const cerrarMenu = () =>{
        setSeleccionado(null)
    }

    useEffect(() => {
        if(seleccionado != null){
            window.addEventListener("click", cerrarMenu);
        }else{
            window.removeEventListener("click", cerrarMenu);
        }
    }, [seleccionado])

    const style = {
       display : "flex",
       height: "100%",
       width: "100%",
       paddingLeft : props.paddingLeft + "px",
       paddingRight : props.paddingRight  + "px"
    }
    const menus = [];
    let menu = props.menu.getMenu();
   
    for(var i = 0; i < menu.length; i++){
        menus.push(<MenuLv1
            key={i} menu={menu[i] } 
            espaciado={props.espaciado} 
            abierto={(seleccionado == menu[i].id)} 
            top={props.top}
            onClick={onClickMenuNivel1}
            onClick2={onClickMenuNivel2}
            onHoverIn={onHoverInMenuNivel1}
            onHoverOut={onHoverOutMenuNivel1}
            ></MenuLv1>);
    }



    return (
       <div style={style} className="menu">
           {menus}
       </div>   
    )
}

Menu.defaultProps = {
    espaciado : 15,
    top: 50,
    paddingLeft : 0,
    paddingRight : 0
}


const MenuLv1 = (props) =>  {
    const {getColor} = useTema({});
    const [color, setColor] = useState("textosMenu1")

    const onClickMenuHandler = event => {
        try{
            props.onClick(event, props.menu.id);
        }catch(error){

        }
    }

    const onHoverInMenuHandler = event => {
        setColor( "textosMenu2");
        try{
            props.onHoverIn(event, props.menu.id);
        }catch(error){

        }
    }

    const onHoverOutMenuHandler = event => {
        
        setColor( "textosMenu1");
        
        try{
            props.onHoverOut(event, props.menu.id);
        }catch(error){

        }
    }

    const onClickContenedor = event => {
        event.stopPropagation();
    }

    const style = {
       display : "flex",
       height: "100%",
       alignItems: "center",
       marginLeft : props.espaciado + "px",
       marginRight : props.espaciado + "px",
       color: getColor(color),
       cursor: "default",
       fontSize: PARAMETROS.editor.menus.sizeMenus,
    }

    const styleSubmenus={
        position:"absolute",
        top: props.top + "px",
        //minHeight:"100px",
        minWidth:"200px",
        backgroundColor : getColor("color1"),
        zIndex :"101",
        color: "white",
        padding:"15px",
        display:"flex",
        flexDirection:"column",
        boxShadow: "2px 4px " + getColor("color3"),
        borderTop: "1px solid " + getColor("color3"),
        borderLeft: "1px solid " + getColor("color3"),
    }
    

    if(props.abierto){
        styleSubmenus.display = "flex"
        style.color = getColor("textosMenu2")
    }else{
        styleSubmenus.display = "none"
    }


    //Creacion de submenus
    const submenus = []
    for(var i = 0; i < props.menu.submenus.length; i++){
        submenus.push(
            <MenuLv2 key={i} menu={props.menu.submenus[i]} onClick={props.onClick2}></MenuLv2>
        )
    }

    return (
        <div style={style} className="menuLv1" >
            <div onClick={onClickMenuHandler} onMouseEnter={onHoverInMenuHandler} onMouseLeave={onHoverOutMenuHandler} >
                {props.menu.literal}
            </div>
            
            <div id={"menuContenedor" + props.nombre} style={styleSubmenus} onClick={onClickContenedor}>
                    {submenus}
            </div>
        </div>   
    )
    

}

const MenuLv2 = (props) =>  {
    const [color, setColor] = useState("textosMenu1")
    const [size, setSize] = useState("inherit")
    const {getColor} = useTema();

    const hoverIn = () => {
        setColor("textosMenu2");
        setSize("bold");
    }

    const hoverOut = () => {
        setColor("textosMenu1");
        setSize("inherit");
    }

    const onCLickHandler = (event) => {
        try{
            if(props.menu.habilitado()){
                props.onClick();
                props.menu.fun()
            }else{
                event.stopPropagation();
            }
            
        }catch(error){

        }
    }

    const style = {
        width: "100%",
        fontSize: PARAMETROS.editor.menus.sizeSubMenus,
        marginBottom: "15px",
        color : getColor(color),
        fontWeight: size,
        opacity : "100%"
    }

    if(!props.menu.habilitado()){
        style.opacity = "50%"
        style.color = "textosMenu1"
        style.fontWeight = "inherit"
    }

    return (
        <div style={style} onMouseEnter={hoverIn} onMouseLeave={hoverOut} onClick={onCLickHandler}>
            {props.menu.literal}
        </div> 
    )
}

export default Menu
