import React, {useState, useEffect, useMemo} from 'react'
import Menu from '../Menu/Menu'
import MenuEstructura from '../../../model/Menu/Menu'
import { useProyecto } from '../../../hooks/useProyecto'
import PARAMETROS from '../../../config/PARAMETROS'
import { useLiterales } from '../../../hooks/useLiterales'
import { useHistory } from 'react-router-dom';

const MenuEditor = (props) =>  {
    const {proyectoModificado, guardaProyecto, cerrarProyecto} = useProyecto();
    const {getLiteral} = useLiterales();
    const {idioma} = useLiterales();
    const history = useHistory();


    const style = {
       height: "100%",
       width: "100%",
    }

    const crearMenu = () =>{
        let menu = new MenuEstructura();
    
        //-----------------------------------------------------------------//
        menu.crearMenu(null, "PROYECTO", getLiteral("menuProyecto"), null,  true)
            //Submenus
            //--
            menu.crearMenu("PROYECTO", "ABRIRP",  getLiteral("menuFicheroSubmenuAbrirProyecto"), function(){
                console.log("Abrir")
            }, true)

            //--
            menu.crearMenu("PROYECTO", "GUARDARP",  getLiteral("menuFicheroSubmenuGuardar"), 
            function(){
                //Accion del menu
                guardaProyecto()
            }, 
            function(){
                //Habilitado del menu
                return (proyectoModificado != null && proyectoModificado > 1) ;
            })
            
            //--
            menu.crearMenu("PROYECTO", "EXPORTARP",  getLiteral("menuFicheroSubmenuExportarProyecto"), function(){
                console.log("Exportar")
            }, true)

            //--
            menu.crearMenu("PROYECTO", "IMPORTAR",  getLiteral("menuFicheroSubmenuImportarProyecto"), function(){
                console.log("Importar")
            }, true)

            //--
            menu.crearMenu("PROYECTO", "CERRARP",  getLiteral("menuFicheroSubmenuCerrarProyecto"), function(){
                cerrarProyecto();
                history.push("/gestion/proyectos")
            }, true)

        //-----------------------------------------------------------------//
        menu.crearMenu(null, "EDITAR",  getLiteral("menuEditar"), null, true)
            //Submenus
            //--
            menu.crearMenu("EDITAR", "PREFERENCIAS",  getLiteral("menuEditarSubmenuPreferencias"), 
            function(){
                //Accion del menu
                history.push("/editor/proyecto/preferencias")
            },true)
        //-----------------------------------------------------------------//
        menu.crearMenu(null, "AYUDA",  getLiteral("menuAyuda"), null, true)
            //Submenus
            //--
            menu.crearMenu("AYUDA", "DOCUMENTACION",  getLiteral("menuAyudaSubmenuDocumentacion"), 
            function(){
                //Accion del menu
                history.push("/editor/proyecto/documentacion")
            }, true)

            menu.crearMenu("AYUDA", "VERSION",  getLiteral("menuAyudaSubmenuNotasVersion"), 
            function(){
                //Accion del menu
                history.push("/editor/proyecto/notas")
            }, true)

            menu.crearMenu("AYUDA", "ACERCADE",  getLiteral("menuAyudaSubmenuAcercaDe", {nombreApp:getLiteral("nombreApp")}), 
            function(){
                //Accion del menu
                history.push("/editor/proyecto/acercade")
            }, true)
        //-----------------------------------------------------------------//
        return menu;
    }

    const [menuEditor, setMenuEditor] = useState(crearMenu());

    useEffect(() => {
        setMenuEditor(crearMenu())
    }, [proyectoModificado, idioma])


    return (
       <Menu 
       menu={menuEditor} 
       top={PARAMETROS.editor.altoCabecera}
       paddingLeft={PARAMETROS.editor.menus.margenConLogo}
       ></Menu>
    )
}








export default MenuEditor
