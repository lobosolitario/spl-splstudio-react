import React, {useState, useEffect, useMemo} from 'react'
import Formulario from '../../componentes-formularios/Formulario/Formulario'
import InputText from '../../componentes-formularios/InputText/InputText'
import TextArea from '../../componentes-formularios/TextArea/TextArea'
import { useTema } from '../../../hooks/useTema';
import { useLiterales } from '../../../hooks/useLiterales';

const PanelGeneral = (props) =>  {

    const {getColor} = useTema();
    const{getLiteral} = useLiterales();

    const style = {
        height: "100%",
        width: "100%",
        boxSizing: "border-box",

        padding: "10px"
    }

    
    

    const inputs = [
        <InputText key="0" 
                   onChange={props.onCambiaNombre} 
                   titulo={getLiteral("editorDeLenguajeNombre")}
                   defval={props.nombre}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>,
        /*
        <InputText key="1" 
                   onChange={props.onCambiaDefecto} 
                   titulo={getLiteral("editorDeLenguajeValorDefecto")}
                   defval={props.defecto}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>,
        */
        <TextArea 
                   key="2" 
                   onChange={props.onCambiaDescripcion} 
                   titulo={getLiteral("editorDeLenguajeDescripcion")}
                   defval={props.descripcion}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></TextArea>,
    ]

    
    return (
        <div style={style}>
            <Formulario inputs={inputs}></Formulario>
            
        </div>
    )
}

PanelGeneral.defaultProps = {
    nombre :"",
    descripcion :"",
    defecto : ""
}


export default PanelGeneral