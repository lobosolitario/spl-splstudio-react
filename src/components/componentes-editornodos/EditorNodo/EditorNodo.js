import React, {useState, useEffect, useMemo} from 'react'
import { useTema } from '../../../hooks/useTema';
import { useLiterales } from '../../../hooks/useLiterales';
import Nodo from '../../../model/Proyecto/Nodo';
import Icono from '../../componentes-iconos/Icono/Icono';
import Separador from '../../componentes-atomicos/Separador/Separador';
import Titulo from '../../componentes-atomicos/Titulo/Titulo';
import MiniBotoneraLateral from '../../componentes-botones/MiniBotoneraLateral/MiniBotoneraLateral';
import BotoneraInferior from '../../componentes-botones/BotoneraInferior/BotoneraInferior';
import LayoutContenedorBotoneras from '../../componentes-layout/LayoutContenedorBotoneras/LayoutContenedorBotoneras'
import PanelGeneral from '../PanelGeneral/PanelGeneral';
import PanelReferencias from '../PanelReferencias/PanelReferencias';

const EditorNodo = (props) =>  {

    
    const {getColor} = useTema();
    const{getLiteral} = useLiterales();
    

    const [panel, setPanel] = useState("General");

    const [cambioReferencia, setCambioReferencia] = useState(true);

    const [nodoNombre, setNodoNombre] = useState(props.nodo.nombre);
    const [nodoDescripcion, setNodoDescripcion] = useState(props.nodo.descripcion);
    const [nodoDefecto, setNodoDefecto] = useState(props.nodo.defecto);

    
    let listaNodos = []

    const onCambiaNombreHandler = (key, valor) =>{
        setNodoNombre(valor)
    }


    const onCambiaDescripcionHandler = (key, valor) =>{
        setNodoDescripcion(valor)
        
    }

    const onCambiaDefecto = (key, valor) =>{
        setNodoDefecto(valor)
    }

 

    const paneles = [];
    paneles["General"] = {
        opcion : "General",
        panel: <PanelGeneral 
                    nombre = {nodoNombre}
                    descripcion = {nodoDescripcion}
                    defecto = {nodoDefecto}
                    listaNodos={listaNodos}
                    onCambiaNombre={onCambiaNombreHandler}
                    onCambiaDescripcion={onCambiaDescripcionHandler}
                    onCambiaDefecto={onCambiaDefecto}
                ></PanelGeneral>
    }



    const botonesInferiores = [
        {
            id : "Cancelar",
            icono: "",
            texto : getLiteral("commonsCancelar"),
        },
        {
            id : "Aceptar",
            icono: "",
            texto : getLiteral("commonsAceptar"),
        },
        
    ]

    const botonesLaterales = [
        {
            id : "General",
            icono: "bag",
            texto : "",
        },
        {
            id : "Referencias",
            icono: "book",
            texto : "",
        },
        {
            id : "Opciones",
            icono: "bag",
            texto : "",
        },
    ]

 



    const onBotonHandler = (accion) => {
        console.log(accion)
        switch (accion) {
            case "Aceptar":
                var datos = {
                    nombre : nodoNombre,
                    descripcion : nodoDescripcion,
                    defecto : nodoDefecto,
                }
                props.onActualizaNodo(datos);
                props.onClose();
                break;

            case "Cancelar":
                props.onClose();
                break;
            case "General":
                setPanel("General")
                break;
            case "Referencias":
                setPanel( "Referencias")
                break;
            case "Opciones":
                setPanel( "Opciones")
                break;
            case "panelConfirmarEliminar":
                setPanel( "Opciones")
                break;
            default:
                break;
        }
    }






    if(props.nodo != null){
        return (
 
            <LayoutContenedorBotoneras
                titulo={<Titulo texto={getLiteral("editorDeLenguajeTituloVentana", {nombreNodo:props.nodo.nombre})}  tamanyo={2}></Titulo>}
                separadorSuperior={ <Separador altura={props.separacionSuperior} color="none"></Separador>}
                sepraadorInferior={<Separador altura={props.separacionInferior} color="none"></Separador>}
                botoneraInferior={<BotoneraInferior colorBoton={getColor("iconos1")} botones={botonesInferiores} onClick={onBotonHandler}></BotoneraInferior>}
                //color={ getColor("editorNodoInterior")}
                panel={paneles[panel].panel}
            >
                
            </LayoutContenedorBotoneras>
        )
    }else{
        return (
        <div>
            Error
        </div>
        )
    }

    
}

EditorNodo.defaultProps = {
    color : "currentcolor",
    colorInterior: "currentcolor",
    opacidad : 80
};



export default EditorNodo
