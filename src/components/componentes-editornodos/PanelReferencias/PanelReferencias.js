import React, {useState, useEffect, useMemo} from 'react'
import InputText from '../../componentes-formularios/InputText/InputText'
import Icono from '../../componentes-iconos/Icono/Icono'
import LayoutAceptarCancelar from '../../componentes-layout/LayoutAceptarCancelar/LayoutAceptarCancelar'
import {useTema} from '../../../hooks/useTema';
import Validador from '../../../utils/ValidadorDatos';
import {useLiterales} from '../../../hooks/useLiterales'
import Titulo from '../../componentes-atomicos/Titulo/Titulo';
import PARAMETROS from '../../../config/PARAMETROS';

const PanelReferencias = (props) =>  {

    const [valor ,setValor] = useState("")
    const [error ,setError] = useState({id:"",params:null})
    const [referencias ,setReferencias] = useState(props.referencias)
    const {getColor} = useTema();
    const{getLiteral} = useLiterales();

    let validador = new Validador();
    validador.validaNoVacio();
    validador.validaAlfanumericoConExtra(["_"]);
    validador.validaNoComenzarConNumeros();
    validador.validaMaximoCaracteres(20);
    validador.validaSinMayusculas();

    const handleChange = (event) => {
        setValor( event.target.value);
    }

    const onBotonHandler = () => {
        setValor("")
    }

    const onSeleccionaNodo = (nodo) => {
        setValor(nodo.nombre)
    }

    const onNuevaReferencia = () => {
        let valido = validador.validar(valor);
        if(valido){
            props.onNuevaReferencia(valor)
        }else{
            
            var errorPrincipal = validador.getErrores()[0];

            switch (errorPrincipal) {
                case 1:
                    setError({id:"ERROR_INPR_01",params:null})
                    break;
                case 5:
                    setError({id:"ERROR_INPR_03",params:null}) 
                    break;
                case 6:
                    setError({id:"ERROR_INPR_02",params:null}) 
                    break;
                case 7:
                    setError({id:"ERROR_INPR_04",params:{ncaracteres:String(PARAMETROS.editorLenguaje.maximoCaracteresNodo)}}) 
                    break;
                case 10:
                    setError({id:"ERROR_INPR_05",params:null}) 
                    break;
                default:
                    setError({id:"ERROR_DEF_02",params:null}) 
                    break;
            }
           
        }
    }
    
    useEffect(() => {
        
        if(error.id != ""){
            setError({id:"",params:null})
        }

    }, [valor])
    


    const styleInput = {
        height: "30px",
        width: "100%",
        backgroundColor : "transparent",
        boxSizing: "border-box",
        outline: "none",
        backgroundColor : getColor("color3"),
        color : getColor("textos"),
        textAlign: "center",
        fontSize: "large"
    }

    function compare( a, b ) {
        if ( a.nombre < b.nombre ){
          return -1;
        }
        if ( a.nombre > b.nombre ){
          return 1;
        }
        return 0;
    }
    
    //Se filtran los resultados
    var referenciasFiltradas = props.referencias.filter(function (el) {
        var fval = false;
        if(valor != null){
            fval = el.nombre.toUpperCase().includes(valor.toUpperCase())
        }
        return fval &&
               !el.esRoot &&
               !el.esReferencia
    });

    //Se ordenan los resultados
    referenciasFiltradas.sort( compare );

    const refs = referenciasFiltradas.map((referencia, index) =>
    <Referencia 
        key={"referencia" + index} 
        referencia={referencia}
        onSeleccionaNodo={onSeleccionaNodo}
        valor={valor}
    ></Referencia>

    );

    return (
        <LayoutAceptarCancelar
            panel={ <LayoutPanelReferencias
                    panelSuperior={refs}
                    barraInferior={<input style={styleInput} className="formularioInputText" type="text" autoComplete="off" name="nombreReferencia" maxLength={PARAMETROS.editorLenguaje.maximoCaracteresNodo}  value={valor} onChange={handleChange}/>}
                    boton={<Icono icon={"close"} onClick={onBotonHandler} size="25"  ></Icono>}
                    error={getLiteral(error.id, error.params)}    
                   
                    ></LayoutPanelReferencias>}
            colorBotones = {getColor("textos")}
            onAceptar={onNuevaReferencia}    
            titulo={<Titulo texto={getLiteral("crearNuevaReferencia")} tamanyo={2}></Titulo>}
            onCancelar={props.onCancelar} 
        ></LayoutAceptarCancelar>
    )
}

PanelReferencias.defaultProps = {
    referencias : [],
    altobarra : 30,
}

export default PanelReferencias

const LayoutPanelReferencias = (props) =>  {
    const {getColor} = useTema();

    const style = {
        display: "flex",
        flexDirection: "column",
        height: "100%",
        width: "100%",
        boxSizing: "border-box",
        alignItems :"center",
        
    }

    const styleBarra = {
        height: props.interiorBarra + "px",
        width:  "calc(100% - " +  props.anchoboton + "px)",
        backgroundColor : "blue",

    }


    const styleContenedorReferencias= {
        height: "calc(100% - " +  props.altobarra + "px)",
        width: "100%",
        backgroundColor : "transparent",
        overflow :"auto",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        borderBottom: "solid 1px white",
        //borderTop: "solid 1px white",
    }

    const styleBoton = {
        height : "100%",
        width : props.anchoboton + "px"
    }

    const styleContenedorBarra = {
        display : "flex",
        height: props.altobarra + "px",
        width:  "95%",
        alignItems :"center",
        flexDirection : "column",
        justifyContent : "center"
    }

    
    const styleError = {
        display : "flex",
        color: getColor("error"),
        height: "20px"
    }

    var error = <></>
    if(props.error != ""){
        //styleError.display = "flex"
    }

    return (
       <div  className="referenciasContenedorPrincipal"  style={style}>
            <div className="referenciasContenedorReferencias"   style={styleContenedorReferencias}>
                {props.panelSuperior}
            </div>
            <div  className="referenciasContenedorBarra"  style={styleContenedorBarra}>
                    {props.barraInferior}
                    <div style={styleError}>{props.error}</div>
            </div>
           
       </div>
    )
}

LayoutPanelReferencias.defaultProps = {
    altobarra : 80,
    anchoboton : 50,
    interiorBarra: 30
}



const Referencia = (props) =>  {
    const {getColor} = useTema();

    const [color, setColor] = useState("bordeNodo")

       
    const hoverIn = (e) =>{
        setColor("bordeNodoSeleccionado")
    }

    const hoverOut = () =>{
        setColor("bordeNodo")
    }

    
    const onClickHandler = (e) => {
        props.onSeleccionaNodo(props.referencia)
    }

    const style = {
       minHeight : "40px",

       cursor : "pointer",
       margin: "10px",

       border : getColor("colorNodo"),

       width: "300px",
       borderRadius: "15px",
       display: "flex",
       alignItems: "center",
       justifyContent: "center",
       color: getColor("textosNodo"),
       backgroundColor: getColor("bordeNodo"),
       overflow :"hidden",
       border :  "1px solid " + getColor(color)
    }
    //console.log("Ref")
    //console.log(props.referencia)

    if(props.valor == props.referencia.nombre ){
        style.border = "1px solid " + getColor("bordeNodoSeleccionado")
    }

    return (
       <div onClick={onClickHandler} style={style} onMouseEnter={hoverIn} onMouseLeave={hoverOut} >
           {props.referencia.nombre}
       </div>
    )
}



