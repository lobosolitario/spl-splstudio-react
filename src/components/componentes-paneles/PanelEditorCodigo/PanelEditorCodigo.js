import React, { Component } from 'react'
import Editor from '../../Editor/Editor'
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles';
import PARAMETROS from '../../../config/PARAMETROS.js'
import {Link, useParams} from 'react-router-dom';


//En este componente se implementa la
const PanelEditorCodigo = (props) => {
    
    let {producto} = useParams()
  
    const style = {
        display:"flex",
        flexDirection : "column",
        height: "100%",
        width: "100%",
        overflow: "auto"
    }
    
    const styleEditorContenedor = {
        height: "calc(100% - " + PARAMETROS.editor.altoBarraMenu + "px)",
        width: "100%",
        overflow: "auto"
    }

    return (
        <div style={style}>
            <BarraSuperiorPaneles>
                Botonera {producto}
            </BarraSuperiorPaneles>
            <div style={styleEditorContenedor}>
                <Editor texto={props.texto}></Editor>
            </div>
        </div>
    )
    
}

export default PanelEditorCodigo;