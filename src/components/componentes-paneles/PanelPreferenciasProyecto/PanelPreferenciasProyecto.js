import React, {useState, useEffect, useMemo} from 'react'
import InputText from '../../componentes-formularios/InputText/InputText'
import LayoutAceptarCancelarEnPanel from '../../componentes-layout/LayoutAceptarCancelarEnPanel/LayoutAceptarCancelarEnPanel'
import { useLiterales } from '../../../hooks/useLiterales';
import { useProyecto } from '../../../hooks/useProyecto';
import { useTema } from '../../../hooks/useTema'

import { useHistory } from 'react-router-dom';
import Formulario from '../../componentes-formularios/Formulario/Formulario';
import Seccion from '../../componentes-formularios/Seccion/Seccion';
import BotonConTexto from '../../componentes-formularios/BotonConTexto/BotonConTexto';

const PanelPreferenciasProyecto = (props) =>  {
    const history = useHistory();
    const {getColor} = useTema();
    const {getLiteral} = useLiterales();
    const {proyecto, modificaProyecto} = useProyecto();

    const style = {
        height: "100%",
       width: "100%",
    }

    
    //EVENTOS
    const onAceptar = () => {
        proyecto.nombre = nombre;
        modificaProyecto(proyecto)
        history.goBack()
    }

    const onCancelar = () => {
        history.goBack()
    }
    
    const onEliminar = () => {
        history.push("/editor/proyecto/eliminar")
    }

    
    //VALORES DE FORMULARIO
    const [nombre, setNombre] = useState("");
    const onChangeNombre = (key, value) =>{
        setNombre(value)
    }

    useEffect(() => {
        if(proyecto != null){
            setNombre(proyecto.nombre);
        }
    }, [proyecto])






    //FORMULARIO
    const inputs = [
        <InputText key="0" 
                   onChange={onChangeNombre} 
                   titulo={getLiteral("formularioProyectosNombre")}
                   defval={nombre}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>
    ]

    
    const getFormularios  = () => {
        return (
            <>
            <Seccion nombre={getLiteral("seccionPropiedades")}>
                <Formulario inputs={inputs}></Formulario>
            </Seccion>
            <Seccion inicioDesplegado={false} nombre={getLiteral("seccionEliminar")}>
                <BotonConTexto texto={getLiteral("formularioEliminarProyecto")} textoBoton={getLiteral("formularioEliminarProyectoBoton")} color="error" colorh="error" onClick={onEliminar}></BotonConTexto>
            </Seccion>
            </>
        )
    }

    
    return (
        
        <LayoutAceptarCancelarEnPanel
        onAceptar={onAceptar}
        onCancelar={onCancelar}
        titulo={getLiteral("formularioEditarProyectoTitulo", {linea: proyecto.nombre})}
        panel={getFormularios ()}
        ></LayoutAceptarCancelarEnPanel>
    )


}

export default PanelPreferenciasProyecto
