import React, {Component, useState, useEffect, useMemo} from 'react'
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles';
import ServiceSelector from '../../../services/ServiceSelector'
import Cargando from '../../componentes-comunes/Cargando/Cargando';
import {useTema} from '../../../hooks/useTema';
import {useProyecto} from '../../../hooks/useProyecto';
import {useApi} from '../../../hooks/useApi';
import { useHistory } from 'react-router-dom';
import { useLiterales } from '../../../hooks/useLiterales';
import Formulario from '../../componentes-formularios/Formulario/Formulario';
import InputText from '../../componentes-formularios/InputText/InputText';
import LayoutAceptarCancelar from '../../componentes-layout/LayoutAceptarCancelar/LayoutAceptarCancelar';
import Titulo from '../../componentes-atomicos/Titulo/Titulo';

const PanelNuevoProyecto = (props) => {
   
    const {getColor} = useTema();
    const{getLiteral} = useLiterales();

    const [nombre , setNombre] = useState("Nuevo Proyecto");

    const onCrearNuevoProyecto = () => {
        props.onAceptar(nombre)
    }

    const onCambiaNombre = (key, value) => {
        setNombre(value)
    }

    const style = {
        height: "100%",
        width: "100%",
        boxSizing: "border-box",

        padding: "10px"
    }

    const inputs = [
        <InputText key="0" 
                   onChange={onCambiaNombre} 
                   titulo={getLiteral("editorDeLenguajeNombre")}
                   defval={nombre}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>
        
    ]

    
    return (
        <LayoutAceptarCancelar
        panel={ <div style={style}>
                    <Formulario inputs={inputs}></Formulario>
                </div>}
        onAceptar={onCrearNuevoProyecto} 
        onCancelar={props.onCancelar} 
        titulo={<Titulo texto={getLiteral("formularioNuevoProyectoTitulo")} tamanyo={2}></Titulo>}
        colorBotones = {getColor("textos")}
        >

        </LayoutAceptarCancelar>
    )
    
}


export default PanelNuevoProyecto;

