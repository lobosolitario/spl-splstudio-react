import React, {useState, useEffect, useMemo} from 'react'

import { useLiterales } from '../../../hooks/useLiterales';
import { useProyecto } from '../../../hooks/useProyecto';
import { useTema } from '../../../hooks/useTema'
import Formulario from '../../componentes-formularios/Formulario/Formulario';
import InputText from '../../componentes-formularios/InputText/InputText'

import { useHistory } from 'react-router-dom';
import LayoutAceptarCancelarEnPanel from '../../componentes-layout/LayoutAceptarCancelarEnPanel/LayoutAceptarCancelarEnPanel';
import Seccion from '../../componentes-formularios/Seccion/Seccion';

const PanelNuevoProducto = (props) =>  {
    const history = useHistory();
    const {getColor} = useTema();
    const {getLiteral} = useLiterales();
    const {crearProducto, seleccionarProducto, seleccionProducto, updateProducto} = useProyecto();


    const esNuevo = () => {
        let modo = props.nuevo;
        if(seleccionProducto == null){ 
            //Si no se ha seleccionado una producto se coloca en modo nuevo
            return true;
        }
        return modo;
    }

   


    //VALIDADORES

    
    //VALORES DE FORMULARIO
    const [nombre, setNombre] = useState("");
    const onChangeNombre = (key, value) =>{
        setNombre(value)
    }

    const [grupo, setGrupo] = useState("");
    const onChangeGrupo = (key, value) =>{
        setGrupo(value)
    }


    useEffect(() => {
        if(esNuevo()){
            setNombre("Nueva producto");
            if(seleccionProducto != null){ 
                setGrupo(seleccionProducto.grupo);
            }else{
                setGrupo("");
            }
        }else{
            setNombre(seleccionProducto.nombre);
            setGrupo(seleccionProducto.grupo);
        }
    }, [props.nuevo, seleccionProducto])




    //EVENTOS
    const onAceptar = () => {
        if(esNuevo()){
            let producto = crearProducto(nombre,  grupo)
            seleccionarProducto(producto.id)
            history.push('/editor/productos/editar')
        }else{
            seleccionProducto.nombre = nombre;
            seleccionProducto.grupo = grupo;
            updateProducto(seleccionProducto)
            history.push('/editor/productos/editar')
        }

    }

    const onCancelar = () => {
        history.push('/editor/productos/editar')
    }
    



    //FORMULARIO
    const inputs = [
        <InputText key="0" 
                   onChange={onChangeNombre} 
                   titulo={getLiteral("formularioNuevaProductoNombre")}
                   defval={nombre}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>,
        <InputText key="1" 
                   onChange={onChangeGrupo} 
                   titulo={getLiteral("formularioNuevaProductoGrupo")}
                   defval={grupo}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>
    ]


    const getFormularios  = () => {
        return (
            <>
            <Seccion nombre={getLiteral("seccionPropiedades")}>
                <Formulario inputs={inputs}></Formulario>
            </Seccion>
            </>
        )
    }
    
    return (
        
        <LayoutAceptarCancelarEnPanel
        onAceptar={onAceptar}
        onCancelar={onCancelar}
        titulo={getLiteral("formularioNuevoProductoTitulo")}
        panel={ getFormularios()}
        ></LayoutAceptarCancelarEnPanel>
    )

}

PanelNuevoProducto.defautProps = {
    nuevo : true
}


export default PanelNuevoProducto
