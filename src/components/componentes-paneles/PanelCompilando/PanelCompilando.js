import React, {useState, useEffect, useMemo} from 'react'
import Cargando from '../../componentes-comunes/Cargando/Cargando'
import { useProyecto } from '../../../hooks/useProyecto'
import { useHistory , Redirect } from 'react-router-dom';

const PanelCompilando = (props) =>  {
    const GUARDADO = 0
    const GUARDANDO = 1
    const MODIFICADO = 2
    const ERROR = 3
    const {guardaProyecto, proyectoModificado, seleccionPlantilla, seleccionProducto, compilacion, compilar} = useProyecto()

    const history = useHistory();


    //0: No seleccionado
    //1: Guardando
    //2: Compilando
    //3: Finalizado
    //4: Error
    const [estadoCompilacion, setEstadoCompilacion] = useState(-1);

    const style = {
       height: "100%",
       width: "100%",
    }

    useEffect(() => {

        //Si no se ha seleccionado la plantilla o el producto
        if( (seleccionPlantilla == null) || (seleccionProducto == null) ){
            setEstadoCompilacion(0);
            return;
        }

        //Si el proyecto no ha sido guardado
        if(proyectoModificado != 0){
            setEstadoCompilacion(1) 
            return;
        }
        
        //Si se ha producido un error durante el guardado del proyecto
        if(proyectoModificado == 3){
            setEstadoCompilacion(4)
            return;
        }

        //Si todavía no se ha compilado el proyecto
        if(compilacion == null){
            setEstadoCompilacion(2)
            return;
        }
        
        //Estado de finalizacion, el proyecto ya se ha compilado
        setEstadoCompilacion(3)

    }, [proyectoModificado, seleccionPlantilla, seleccionProducto, compilacion])

    
    useEffect(() => {
        if(estadoCompilacion == 0){
            //No se realiza ninguna accion, se muestra el ayudante de seleccion de plantilla y de producto
            //TODO, eliminar el compilado del proyecto
        }
        if(estadoCompilacion == 1){
            //Se lanza el guardado del proyecto
            guardaProyecto();
        }
        if(estadoCompilacion == 2){
            //El proyecto ya esta guardado se comienza la compilacion
            compilar();
        }
    }, [estadoCompilacion])


    switch (estadoCompilacion) {
        case 0:
            return (
                <div style={style}>
                        No se ha seleccionado
                </div>
            )
        case 1: //Guardando
            return (
                <div style={style}>
                    <Cargando texto="Guardando"></Cargando>
                </div>
            )
        case 2: //Compilando
            return (
                <div style={style}>
                    <Cargando texto="Compilando"></Cargando>
                </div>
            )
        case 3: //Finalizado
            return (
                <div style={style}>
                    <Redirect to={props.destino} />
                </div>
            )   
        case 4: //Error
            return (
                <div style={style}>
                    Se ha producido un error
                </div>
            )
        case 0:
        default:
            return (
                <div style={style}>
                   
                </div>
             )
    }


}

export default PanelCompilando