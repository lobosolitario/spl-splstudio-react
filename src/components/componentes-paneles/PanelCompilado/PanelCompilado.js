import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS';
import { useProyecto } from '../../../hooks/useProyecto'
import BotonCompilar from '../../componentes-botones/BotonCompilar/BotonCompilar'
import BotoneraEditoCompilados from '../../componentes-botones/BotoneraEditoCompilados/BotoneraEditoCompilados';
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles';
import Editor from '../../Editor/Editor';

const PanelCompilado = (props) =>  {
    const {compilacion,seleccionCompilado } = useProyecto();
    const [code, setCode] = useState(null);
    const [errorCompilacion, setErrorCompilacion] = useState(false);

    const style = {
        display : "flex",
        flexDirection : "column",
        height: "100%",
        width: "100%",
        
     }
 
     const styleEditor = {
         display : "flex",
         height: "calc(100% - " + PARAMETROS.editor.altoBarraMenu + "px)",
         width: "100%",
         boxSizing : "border-box"
      }

    const onCodeChangeHandler = (editor, data, value) =>{

    }

 
    useEffect(() => {
        if(compilacion != null){
            var compilado = compilacion.operaciones().compiladoById(seleccionCompilado);
            if(compilado != null){
                if(compilacion.operaciones().hayErrorDeCompilacion(compilado)){
                    setErrorCompilacion(true)
                }else{
                    setErrorCompilacion(false)
                }
            }
        }
    }, [compilacion, seleccionCompilado])

    useEffect(() => {
        if(compilacion != null){
            var compilado = compilacion.operaciones().compiladoById(seleccionCompilado);
            if(compilado != null){

                if(props.vista != "bugs" && !errorCompilacion){
                    setCode(compilado.code)
                }else{
                    let texto = compilacion.operaciones().getTextoErroresCompilacion(compilado);
                    console.log(texto)
                    setCode(texto)
                }

            }
        }
    }, [compilacion, seleccionCompilado, props.vista, errorCompilacion])
    

    if(seleccionCompilado != null){
        return (
            <div className="panelEditorPlantillasContenedor" style={style}>
                <BarraSuperiorPaneles>
                    <BotoneraEditoCompilados erroCompilacion={errorCompilacion} verBotonBugs={props.vista != "bugs"}></BotoneraEditoCompilados>
                </BarraSuperiorPaneles>
                    <div style={styleEditor} className="panelEditorPlantillasEditor">
                    <Editor   onChange={onCodeChangeHandler} texto={code}></Editor>
                </div>
            </div>
         )
    }

    return( 
            <div className="panelEditorPlantillasContenedor" style={style}>
                <BarraSuperiorPaneles>
                   
                </BarraSuperiorPaneles>
                <div style={styleEditor} className="panelEditorPlantillasEditor">
                    Seleccionar compilado
                </div>
            </div>
    )
}

export default PanelCompilado
