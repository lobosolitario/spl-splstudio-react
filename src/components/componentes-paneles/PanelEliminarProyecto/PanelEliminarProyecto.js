import React, {useState, useEffect, useMemo} from 'react'
import { useLiterales } from '../../../hooks/useLiterales';
import { useProyecto } from '../../../hooks/useProyecto';
import { useTema } from '../../../hooks/useTema'
import { useHistory } from 'react-router-dom';
import Titulo from '../../componentes-atomicos/Titulo/Titulo';
import LayoutAceptarCancelarEnPanel from '../../componentes-layout/LayoutAceptarCancelarEnPanel/LayoutAceptarCancelarEnPanel';
import Mensaje from '../../componentes-comunes/Mensaje/Mensaje';



const PanelEliminarProyecto = (props) =>  {
    const history = useHistory();
    const {getColor} = useTema();
    const {getLiteral} = useLiterales();
    const {proyecto,eliminarProyecto} = useProyecto();


    const style = {
        height: "100%",
       width: "100%",
    }

   


    const getPanel = () => {

        const style = {
            display : "flex",
            justifyContent : "center",
            alignItems : "center",
            height: "100%",
            width: "100%"
         }

        return(
            <div style={style}><Mensaje mensaje={getLiteral("panelEliminarProyectoMensajeEliminar", {nombreProyecto:proyecto.nombre})} color={getColor("textos")} ></Mensaje></div>
        )
    } 
    
    //EVENTOS
    const onAceptar = () => {
        eliminarProyecto();
        history.push('/gestion/proyectos')
    }

    const onCancelar = () => {
        history.goBack();
    }

    return (
        <LayoutAceptarCancelarEnPanel
        onAceptar={onAceptar}
        onCancelar={onCancelar}
        titulo=""
        panel={getPanel()}
        ></LayoutAceptarCancelarEnPanel>
    )
}

export default PanelEliminarProyecto
