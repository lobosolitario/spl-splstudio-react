import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS';
import { useProyecto } from '../../../hooks/useProyecto'
import BotoneraEditorPlantilla from '../../componentes-botones/BotoneraEditorPlantilla/BotoneraEditorPlantilla';
import BotoneraSoloGuardado from '../../componentes-botones/BotoneraSoloGuardado/BotoneraSoloGuardado';
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles'
import Editor from '../../Editor/Editor';

const PanelEditorPlantillas = (props) =>  {
    const {updatePlantilla, seleccionPlantilla, proyecto} = useProyecto();
    const [code, setCode] = useState(null);
    const style = {
       display : "flex",
       flexDirection : "column",
       height: "100%",
       width: "100%",
       
    }

    const styleEditor = {
        display : "flex",
        height: "calc(100% - " + PARAMETROS.editor.altoBarraMenu + "px)",
        width: "100%",
        boxSizing : "border-box"
     }

    useEffect(() => {

        if(seleccionPlantilla != null){
            setCode(seleccionPlantilla.code)
        }
    }, [seleccionPlantilla])


    const onCodeChangeHandler = (editor, data, value) =>{
        
        if(seleccionPlantilla != null && code != null && value != seleccionPlantilla.code){
            seleccionPlantilla.code = value;
            updatePlantilla(seleccionPlantilla);
            
        }
        //console.log(value)
        //seleccionPlantilla.code = value;
        //updatePlantilla(seleccionPlantilla);
        //TODO actualizar la plantilla en useProyecto
    }

    let tokens = []
    if(proyecto != null && proyecto.lenguaje != null){
        tokens = proyecto.lenguaje.operaciones().getPalabrasReservadas();
    }
    
    if(seleccionPlantilla != null){
        return (
            <div className="panelEditorPlantillasContenedor" style={style}>
                <BarraSuperiorPaneles><BotoneraEditorPlantilla></BotoneraEditorPlantilla></BarraSuperiorPaneles>
                <div style={styleEditor} className="panelEditorPlantillasEditor">
                <Editor  lenguaje="splfamilias" lenguajeExterno="none" tokens={tokens} onChange={onCodeChangeHandler} texto={code}></Editor>
                </div>
            </div>
         )
    }

    return( 
            <div className="panelEditorPlantillasContenedor" style={style}>
                <BarraSuperiorPaneles><BotoneraSoloGuardado></BotoneraSoloGuardado></BarraSuperiorPaneles>
                <div style={styleEditor} className="panelEditorPlantillasEditor">
                    Seleccionar una plantilla
                </div>
            </div>
    )
}

export default PanelEditorPlantillas
