import React, {useState, useEffect, useMemo} from 'react'

import { useLiterales } from '../../../hooks/useLiterales';
import { useProyecto } from '../../../hooks/useProyecto';
import { useTema } from '../../../hooks/useTema'
import Formulario from '../../componentes-formularios/Formulario/Formulario';
import InputText from '../../componentes-formularios/InputText/InputText'

import { useHistory } from 'react-router-dom';
import LayoutAceptarCancelarEnPanel from '../../componentes-layout/LayoutAceptarCancelarEnPanel/LayoutAceptarCancelarEnPanel';
import Seccion from '../../componentes-formularios/Seccion/Seccion';
import SeleccionFichero from '../../componentes-formularios/SeleccionFichero/SeleccionFichero';
import JsZip from 'jszip' 

const PanelSubirPlantillas = (props) =>  {
    const history = useHistory();
    const {getColor} = useTema();
    const {getLiteral} = useLiterales();
    const {crearPlantilla, seleccionarPlantilla, seleccionPlantilla, updatePlantilla} = useProyecto();


    const esNuevo = () => {
        let modo = props.nuevo;
        if(seleccionPlantilla == null){ 
            //Si no se ha seleccionado una plantilla se coloca en modo nuevo
            return true;
        }
        return modo;
    }

   


    //VALIDADORES

    
    //VALORES DE FORMULARIO
    const [grupo, setGrupo] = useState("prototipo");
    const onChangeGrupo = (key, value) =>{
        setGrupo(value)
    }

    const [fichero, setFichero] = useState("");
    const onChangeFichero = (key, value) =>{
        setFichero(value)
    }

    function compare( a, b ) {
        if ( a.name < b.name ){
          return -1;
        }
        if ( a.name > b.name ){
          return 1;
        }
        return 0;
    }




    //EVENTOS
    const onAceptar = () => {
        const zip = new JsZip();
        zip.loadAsync(fichero).then(function(zip) {
            // you now have every files contained in the loaded zip
            //zip.sort(compare)
            zip.forEach(function (relativePath, zipEntry) {  // 2) print entries
                if(zipEntry.dir == false && !zipEntry.name.includes("/.git/") && zipEntry._data.compressedSize < 5000){
                    
                    zipEntry.async("text").then(function(ftext){
                        //console.log(ftext)
                        var filename = relativePath.replace(/^.*[\\\/]/, '')
                        var path = relativePath.substr(0, relativePath.lastIndexOf('/'));
                        crearPlantilla(relativePath, "root", grupo, getCode(path, filename, ftext))
                    });
                    
                }
            });
            
        });
        //console.log(fichero)        
    }

    function getCode(ruta, nombre ,  texto){
        let code = "";
        code = code + "<%\n"
        code = code + "    $cfg.path = \""+ruta+"\";\n"
        code = code + "    $cfg.name = \""+nombre+"\";\n"
        code = code + "%>\\\n\\\n"
        code = code + texto;
        return code
    }

    const onCancelar = () => {
        history.push('/editor/plantillas/editar')
    }
    



    //FORMULARIO
    const inputs = [

        <InputText key="0" 
                   onChange={onChangeGrupo} 
                   titulo={getLiteral("formularioNuevaPlantillaGrupo")}
                   defval={grupo}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>
    ]

    const inputs2 = [
        <SeleccionFichero  key="0" 
                   onChange={onChangeFichero} 
                   titulo={getLiteral("formularioSubirPlantillaFormularioPrototipo")}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></SeleccionFichero>
    ]



    const getFormularios  = () => {
        return (
            <>
            <Seccion nombre={getLiteral("seccionPropiedades")}>
                <Formulario inputs={inputs}></Formulario>

            </Seccion>
            <Seccion nombre={getLiteral("seccionSeleccionarFichero")}>
                <Formulario inputs={inputs2}></Formulario>
            </Seccion>
            </>
        )
    }
    
    return (
        
        <LayoutAceptarCancelarEnPanel
        onAceptar={onAceptar}
        onCancelar={onCancelar}
        titulo={getLiteral("formularioSubirPlantillaTitulo")}
        panel={getFormularios()}
        ></LayoutAceptarCancelarEnPanel>
    )

}

PanelSubirPlantillas.defautProps = {
    nuevo : true
}


export default PanelSubirPlantillas
