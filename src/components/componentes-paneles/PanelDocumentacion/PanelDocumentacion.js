import React, {useState, useEffect, useMemo} from 'react'
import { useLiterales } from '../../../hooks/useLiterales'
import Logo from '../../componentes-iconos/Logo/Logo'
import PanelInformativo from '../PanelInformativo/PanelInformativo';

const PanelDocumentacion = (props) =>  {
   const {getLiteral} = useLiterales();

   return <PanelInformativo
      logo = {<Logo size={100}  modo="2"></Logo>}
      titulo = {getLiteral("panelHomeBienvenido", {nombreApp:getLiteral("nombreApp")})}
      textos = {[getLiteral("panelHomeTexto1"),getLiteral("panelHomeTexto2")]}
   ></PanelInformativo>
}

export default PanelDocumentacion
