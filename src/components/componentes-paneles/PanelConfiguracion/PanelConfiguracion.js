import React, {useState, useEffect, useMemo} from 'react'
import { useLiterales } from '../../../hooks/useLiterales'
import Separador from '../../componentes-atomicos/Separador/Separador';
import Titulo from '../../componentes-atomicos/Titulo/Titulo';
import SelectorIdioma from '../../componentes-configuracion/SelectorIdioma/SelectorIdioma';
import { SelectorTema } from '../../componentes-configuracion/SelectorTema/SelectorTema';
import Logo from '../../componentes-iconos/Logo/Logo'
import LayoutLimitaAncho from '../../componentes-layout/LayoutLimitaAncho/LayoutLimitaAncho';

const PanelConfiguracion = (props) =>  {
   const {getLiteral} = useLiterales();

    const style = {
      display : "flex",
      alignItems : "center",
      justifyContent: "flex-start", 
      flexDirection: "column",
      height: "100%",
      width: "100%",
      overflow : "auto"
    }

    const styleTitulo = {
      fontSize : "30px",
      marginBottom : "50px"
    }

    const styleTexto = {
      fontSize : "18px",
      maxWidth : "500px",
    }

    return (
          <LayoutLimitaAncho>
            <Separador altura={10}></Separador>
            <Titulo texto={getLiteral("panelConfiguracionConfiguracion")}  tamanyo={0}></Titulo>

            <Separador altura={30}></Separador>
            <Titulo texto={getLiteral("panelConfiguracionIdioma")} alineamiento="flex-start" tamanyo={2}></Titulo>
            <SelectorIdioma></SelectorIdioma>

            <Separador altura={30}></Separador>
            <Titulo texto={getLiteral("panelConfiguracionTema")} alineamiento="flex-start" tamanyo={2}></Titulo>
            <SelectorTema></SelectorTema>

          </LayoutLimitaAncho>
    )
}

export default PanelConfiguracion
