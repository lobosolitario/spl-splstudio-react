import React, {useState, useEffect, useMemo} from 'react'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'
import { useTema } from '../../../hooks/useTema'
import { useUsuario } from '../../../hooks/useUsuario';
import Separador from '../../componentes-atomicos/Separador/Separador';
import Titulo from '../../componentes-atomicos/Titulo/Titulo';
import BotonGenerico from '../../componentes-botones/BotonGenerico/BotonGenerico';
import BarraIdioma from '../../componentes-configuracion/BarraIdioma/BarraIdioma';
import SelectorIdioma from '../../componentes-configuracion/SelectorIdioma/SelectorIdioma';
import Logo from '../../componentes-iconos/Logo/Logo';
import BarraLinksRegistro from '../../componentes-login/BarraLinksRegistro/BarraLinksRegistro';
import FormularioRegistro from '../../componentes-login/FormularioRegistro/FormularioRegistro';


const  PanelRegistro = (props) => {
    const {getColor} = useTema();

    
    const style = {
        display: "flex",
        alignItems : "center",
        flexDirection:'column',
        height : "100%",
        width : "100%",
        color: getColor("textos"),
        paddingBottom : "60px",
        boxSizing: "border-box"
    }
    
 

    return(<div className="pantallaLogin" style={style}>
        <BarraIdioma></BarraIdioma>
        <Titulo
            texto="Titulo1"
        ></Titulo>
        <Separador altura="20px"></Separador>
        <FormularioRegistro></FormularioRegistro>
        <BarraLinksRegistro></BarraLinksRegistro>
        </div>)
}

PanelRegistro.defaultProps = {
}





export default PanelRegistro;