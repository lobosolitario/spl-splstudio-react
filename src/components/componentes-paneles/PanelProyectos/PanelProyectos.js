import React, {Component, useState, useEffect, useMemo} from 'react'
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles';
import ServiceSelector from '../../../services/ServiceSelector'
import Cargando from '../../componentes-comunes/Cargando/Cargando';
import {useTema} from '../../../hooks/useTema';
import {useProyecto} from '../../../hooks/useProyecto';
import { useHistory } from 'react-router-dom';
import Icono from '../../componentes-iconos/Icono/Icono';
import PARAMETROS from '../../../config/PARAMETROS';
import BotoneraPanelProyectos from '../../componentes-botones/BotoneraPanelProyectos/BotoneraPanelProyectos';
import Intersticial from '../../componentes-comunes/Intersticial/Intersticial';
import PanelNuevoProyecto from '../PanelNuevoProyecto/PanelNuevoProyecto';
import { useLiterales } from '../../../hooks/useLiterales';

const PanelProyectos = (props) => {
   
    const {getLiteral} = useLiterales()
    const {getColor} = useTema({});
    const history = useHistory();
    const {listaProyectos, listarProyectos, cargaProyecto, crearProyecto, proyecto} = useProyecto();

    //-1 Muestra simbolo de carga
    //0 No se muestra ninguna pantalla adiccional
    //1 Muestra el dialogo de nuevo proyecto

    const [modo, setModo] = useState(-1);

    useEffect(() => {
        if(proyecto && proyecto != null){
            
            history.push('/editor')
        }
    }, [proyecto])


    useEffect(() => {
        listarProyectos();  
    }, [])


    useEffect(() => {
        console.log(listaProyectos)
        if(listaProyectos){
            setModo(0)
        }else{
            setModo(-1)
        }
    }, [listaProyectos])



    const onClickAbrirProyectoHandler = (idproy) => {
        cargaProyecto(idproy)
    }
    
    const onNuevoProyectoHandler = () => {
        setModo(1)
    }

    const onCrearNuevoProyecto = (nombre) => {
        setModo(-1)
        crearProyecto(nombre)
    }

    const onCloseIntersticialHandler = () => {
        setModo(0)
        //setNodoSeleccionado(null)
    }

    const getListadoProyectos = () => {
        const style = {
            height: "100%",
            width: "100%",
            
        }

        const styleTable = {
            //height: "100%",
            width: "100%",
            textAlign: "left",
            padding: "20px"
        }

        const styleContenedor = {
            height :  "calc(100% - " +  PARAMETROS.editor.altoBarraMenu + "px)",
            width: "100%",
            overflow : "auto"
        }


    

       
       const tablaProyectos = listaProyectos.map((proyecto, index) =>
          <ProyectoItem color={getColor("textos")} colorh={getColor("iconos2")} index={index} onClick={onClickAbrirProyectoHandler} key={"proyecto" + index} proyecto={proyecto}></ProyectoItem>
       );

           const onCloseIntersticialHandler = () => {
        setModo(0)
        //setNodoSeleccionado(null)
    }


        switch (modo) {
            case 1:
                return(
                <div style={style}>
                    <BarraSuperiorPaneles>
                        <BotoneraPanelProyectos></BotoneraPanelProyectos>
                    </BarraSuperiorPaneles>
                    <div style={styleContenedor}>
                    <table style={styleTable} className="nonSelectable">
                        <thead>
                            <tr>
                                <th>{getLiteral("formularioProyectosNombre")}</th>
                                <th>{getLiteral("formularioProyectosUsuario")}</th>
                                <th>{getLiteral("formularioProyectosCreacion")}</th>
                                <th>{getLiteral("formularioProyectosModificacion")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tablaProyectos}
                        </tbody>
                    </table >
                    </div>
                    <Intersticial 
                    onClose={onCloseIntersticialHandler} 
                    visible={true}
                    color={getColor("color3")}
                    colorInterior={getColor("color2")}
                    >
                        <PanelNuevoProyecto
                            onAceptar={onCrearNuevoProyecto}
                            onCancelar={onCloseIntersticialHandler}
                        ></PanelNuevoProyecto>
                </Intersticial>
                </div>

                )
            default:
                return(
                    <div style={style}>
                        <BarraSuperiorPaneles>
                            <BotoneraPanelProyectos onNuevoProyecto={onNuevoProyectoHandler}></BotoneraPanelProyectos>
                        </BarraSuperiorPaneles>
                        <div style={styleContenedor}>
                        <table style={styleTable} className="nonSelectable">
                            <thead>
                                <tr>
                                    <th>{getLiteral("formularioProyectosNombre")}</th>
                                    <th>{getLiteral("formularioProyectosCreacion")}</th>
                                    <th>{getLiteral("formularioProyectosModificacion")}</th>
                                    <th>{getLiteral("formularioProyectosUsuario")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {tablaProyectos}
                            </tbody>
                        </table >
                        </div>
                    </div>
                )
        }
    }


    
    

    if(modo >= 0){
        return  getListadoProyectos();
        
    }

    return <Cargando></Cargando>;
    
}


const ProyectoItem = (props) => {
    const {getColor} = useTema();
    const {getLiteral} = useLiterales()

    const [color, setColor] = useState("textos");

    
    const hoverIn = () =>{
        setColor("iconos2")
    }

    const hoverOut = () =>{
        setColor("textos")
    }

        
 

    const style = {
        border: "solid 1px red",
        fontSize: "small",
        //backgroundColor:"red",
        cursor:"pointer",
        marginBottom:"5px",
        color:getColor(color),

    }

    const styleTd = {
        //backgroundColor:"red",  
   
    }

    const getUsuario = () =>{
        return(
            getLiteral("panelTipoUsuarioPersonal")
        )
    }

    return(
        <tr className="filaProyecto" onMouseEnter={hoverIn} onMouseLeave={hoverOut}  style={style} onClick={() => props.onClick(props.proyecto.id)}>
            <td style={styleTd}>{props.proyecto.nombre}</td>
            <td style={styleTd}>{props.proyecto.createAt}</td>
            <td style={styleTd}>{props.proyecto.modifiedAt}</td>
            <td style={styleTd}>{getUsuario()}</td>
        </tr>
    )
    
}



export default PanelProyectos;

