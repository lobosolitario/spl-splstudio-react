import React, {useState, useEffect, useMemo} from 'react'
import { useHistory } from 'react-router-dom';

const PanelError = (props) =>  {
    const history = useHistory();

    const style = {
        height: "100%",
       width: "100%",
    }

    const onHomeClick = () =>{
        history.push('/gestion/home')
    }

    return (
       <div style={style}>
           Se ha producido un error
           <button type="button" onClick={onHomeClick}>
            Go home
            </button>
       </div>
    )
}

export default PanelError
