import React, {Component, useState, useEffect, useMemo} from 'react'
import Editor from '../../Editor/Editor'
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles';
import PARAMETROS from '../../../config/PARAMETROS.js'
import PanelEditorCodigo from '../PanelEditorCodigo/PanelEditorCodigo';

import {Link, useParams} from 'react-router-dom';
import {useProyecto} from '../../../hooks/useProyecto'
import Lenguaje from '../../../model/Proyecto/Lenguaje';
import VisorLenguaje from '../../componentes-comunes/VisorLenguaje/VisorLenguaje';
import BotoneraEditorLenguaje from '../../componentes-botones/BotoneraEditorLenguaje/BotoneraEditorLenguaje';


const PanelEditorLenguaje = (props) => {
    
    const style = {
        height: "100%",
        width: "100%"
    }
    

    return (
        <div style={style}>
            <BarraSuperiorPaneles>
                <BotoneraEditorLenguaje></BotoneraEditorLenguaje>
            </BarraSuperiorPaneles>
            <VisorLenguaje height={"calc(100% - " + PARAMETROS.editor.altoBarraMenu + "px)"}></VisorLenguaje>
        </div>
    )
    
}
export default PanelEditorLenguaje