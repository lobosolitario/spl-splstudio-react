import React, {useState, useEffect, useMemo} from 'react'

import { useLiterales } from '../../../hooks/useLiterales';
import { useProyecto } from '../../../hooks/useProyecto';
import { useTema } from '../../../hooks/useTema'
import Formulario from '../../componentes-formularios/Formulario/Formulario';
import InputText from '../../componentes-formularios/InputText/InputText'

import { useHistory } from 'react-router-dom';
import LayoutAceptarCancelarEnPanel from '../../componentes-layout/LayoutAceptarCancelarEnPanel/LayoutAceptarCancelarEnPanel';
import Seccion from '../../componentes-formularios/Seccion/Seccion';

const PanelNuevaPlantilla = (props) =>  {
    const history = useHistory();
    const {getColor} = useTema();
    const {getLiteral} = useLiterales();
    const {crearPlantilla, seleccionarPlantilla, seleccionPlantilla, updatePlantilla} = useProyecto();


    const esNuevo = () => {
        let modo = props.nuevo;
        if(seleccionPlantilla == null){ 
            //Si no se ha seleccionado una plantilla se coloca en modo nuevo
            return true;
        }
        return modo;
    }

   


    //VALIDADORES

    
    //VALORES DE FORMULARIO
    const [nombre, setNombre] = useState("");
    const onChangeNombre = (key, value) =>{
        setNombre(value)
    }

    const [ancla, setAncla] = useState("");
    const onChangeAncla = (key, value) =>{
        setAncla(value)
    }

    const [grupo, setGrupo] = useState("");
    const onChangeGrupo = (key, value) =>{
        setGrupo(value)
    }


    useEffect(() => {
        if(esNuevo()){
            setNombre("Nueva plantilla");
            setAncla("root");
            if(seleccionPlantilla != null){ 
                setGrupo(seleccionPlantilla.grupo);
            }else{
                setGrupo("");
            }
        }else{
            setNombre(seleccionPlantilla.nombre);
            setAncla(seleccionPlantilla.ancla);
            setGrupo(seleccionPlantilla.grupo);
        }
    }, [props.nuevo, seleccionPlantilla])




    //EVENTOS
    const onAceptar = () => {
        if(esNuevo()){
            let plantilla = crearPlantilla(nombre, ancla, grupo)
            seleccionarPlantilla(plantilla.id)
            history.push('/editor/plantillas/editar')
        }else{
            seleccionPlantilla.nombre = nombre;
            seleccionPlantilla.ancla = ancla;
            seleccionPlantilla.grupo = grupo;
            updatePlantilla(seleccionPlantilla)
            history.push('/editor/plantillas/editar')
        }

    }

    const onCancelar = () => {
        history.push('/editor/plantillas/editar')
    }
    



    //FORMULARIO
    const inputs = [
        <InputText key="0" 
                   onChange={onChangeNombre} 
                   titulo={getLiteral("formularioNuevaPlantillaNombre")}
                   defval={nombre}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>,
        <InputText key="1" 
                   onChange={onChangeAncla} 
                   titulo={getLiteral("formularioNuevaPlantillaAncla")}
                   defval={ancla}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>,
        <InputText key="3" 
                   onChange={onChangeGrupo} 
                   titulo={getLiteral("formularioNuevaPlantillaGrupo")}
                   defval={grupo}
                   colorTexto = {getColor("textos")}
                   colorTitulo = {getColor("textos")}
                   color = {getColor("color3")}
        ></InputText>
    ]


    const getFormularios  = () => {
        return (
            <>
            <Seccion nombre={getLiteral("seccionPropiedades")}>
                <Formulario inputs={inputs}></Formulario>
            </Seccion>
            </>
        )
    }
    
    return (
        
        <LayoutAceptarCancelarEnPanel
        onAceptar={onAceptar}
        onCancelar={onCancelar}
        titulo={getLiteral("formularioNuevaPlantillaTitulo")}
        panel={getFormularios()}
        ></LayoutAceptarCancelarEnPanel>
    )

}

PanelNuevaPlantilla.defautProps = {
    nuevo : true
}


export default PanelNuevaPlantilla
