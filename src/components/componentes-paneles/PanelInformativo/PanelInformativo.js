import React, {useState, useEffect, useMemo} from 'react'
import { useLiterales } from '../../../hooks/useLiterales'
import { useTema } from '../../../hooks/useTema';
import Logo from '../../componentes-iconos/Logo/Logo'

const PanelInformativo = (props) =>  {
   const {getLiteral} = useLiterales();
   const {getColor} = useTema();

    const style = {
      display : "flex",
      alignItems : "center",
      justifyContent: "flex-start", 
      flexDirection: "column",
      height: "100%",
      width: "100%",
      overflow : "auto",
      color : getColor(props.colorTexto)
    }

    const styleTitulo = {
      fontSize : "30px",
      marginBottom : "50px"
    }

    const styleTexto = {
      fontSize : "18px",
      maxWidth : "500px",
      textAlign: "justify"
    }



    let textos = [];
    for (let i = 0; i < props.textos.length; i++) {
      const texto = props.textos[i];
      textos.push(<div key={"txt" + i}  style={styleTexto}>{texto}</div>)
      textos.push(<br  key={"txtsp" + i}/>)
    }

    let titulo = null
    if(props.titulo != null){
      titulo = <div style={styleTitulo}>{props.titulo}</div>
    }



    return (
       <div style={style}>
          {props.logo}
          {titulo}
          {textos}
       </div>
    )
}

PanelInformativo.defaultProps = {
  logo : null,
  titulo : null,
  textos : [],
  colorTexto : "textos"
}

export default PanelInformativo
