import React, {useState, useEffect, useMemo} from 'react'
import { useLiterales } from '../../../hooks/useLiterales';
import { useProyecto } from '../../../hooks/useProyecto';
import { useTema } from '../../../hooks/useTema'
import { useHistory } from 'react-router-dom';
import Titulo from '../../componentes-atomicos/Titulo/Titulo';
import LayoutAceptarCancelarEnPanel from '../../componentes-layout/LayoutAceptarCancelarEnPanel/LayoutAceptarCancelarEnPanel';
import Mensaje from '../../componentes-comunes/Mensaje/Mensaje';

const PanelEliminarProducto = (props) =>  {
    const history = useHistory();
    const {getColor} = useTema();
    const {getLiteral} = useLiterales();
    const {seleccionProducto,  eliminarProducto} = useProyecto();


    const style = {
        height: "100%",
       width: "100%",
    }

    if(seleccionProducto == null){
        history.push('/editor/productos/editar')
        return <></>
    }


    const getPanel = () => {

        const style = {
            display : "flex",
            justifyContent : "center",
            alignItems : "center",
            height: "100%",
            width: "100%"
         }

        return(
            <div style={style}><Mensaje mensaje={getLiteral("eliminarProducto", {nombreProducto:seleccionProducto.nombre})} color={getColor("textos")} ></Mensaje></div>
        )
    } 
    
    //EVENTOS
    const onAceptar = () => {
        eliminarProducto(seleccionProducto)
        history.push('/editor/productos/editar')
    }

    const onCancelar = () => {
        history.push('/editor/productos/editar')
    }

    return (
        <LayoutAceptarCancelarEnPanel
        onAceptar={onAceptar}
        onCancelar={onCancelar}
        titulo=""
        panel={getPanel()}
        ></LayoutAceptarCancelarEnPanel>
    )
}


export default PanelEliminarProducto
