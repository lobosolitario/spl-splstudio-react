import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS';
import { useProyecto } from '../../../hooks/useProyecto'
import BotoneraEditorPlantilla from '../../componentes-botones/BotoneraEditorPlantilla/BotoneraEditorPlantilla';
import BotoneraEditorProductos from '../../componentes-botones/BotoneraEditorProductos/BotoneraEditorProductos';
import BotoneraSoloGuardado from '../../componentes-botones/BotoneraSoloGuardado/BotoneraSoloGuardado';
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles'
import Editor from '../../Editor/Editor';


const PanelEditorProductos = (props) => {
    const {updateProducto, seleccionProducto, proyecto} = useProyecto();
    const [code, setCode] = useState(null);
    const style = {
       display : "flex",
       flexDirection : "column",
       height: "100%",
       width: "100%",
       
    }

    const styleEditor = {
        display : "flex",
        height: "calc(100% - " + PARAMETROS.editor.altoBarraMenu + "px)",
        width: "100%",
        boxSizing : "border-box"
     }

    useEffect(() => {
        
        console.log(seleccionProducto)
        if(seleccionProducto != null){
            setCode(seleccionProducto.code)
        }
    }, [seleccionProducto])


    const onCodeChangeHandler = (editor, data, value) =>{
        
        if(seleccionProducto != null && code != null && value != seleccionProducto.code){
            seleccionProducto.code = value;
            updateProducto(seleccionProducto);
            
        }
        //console.log(value)
        //seleccionProducto.code = value;
        //updateProducto(seleccionProducto);
        //TODO actualizar la plantilla en useProyecto
    }

    let tokens = []
    if(proyecto != null && proyecto.lenguaje != null){
        tokens = proyecto.lenguaje.operaciones().getPalabrasReservadas();
    }

    
    if(seleccionProducto != null){
        return (
            <div className="panelEditorPlantillasContenedor" style={style}>
                <BarraSuperiorPaneles><BotoneraEditorProductos></BotoneraEditorProductos></BarraSuperiorPaneles>
                <div style={styleEditor} className="panelEditorPlantillasEditor">
                <Editor lenguaje="splproductos" tokens={tokens} onChange={onCodeChangeHandler} texto={code}></Editor>
                </div>
            </div>
         )
    }

    return( 
            <div className="panelEditorPlantillasContenedor" style={style}>
                <BarraSuperiorPaneles><BotoneraSoloGuardado></BotoneraSoloGuardado></BarraSuperiorPaneles>
                <div style={styleEditor} className="panelEditorPlantillasEditor">
                    Seleccionar un pruducto
                </div>
            </div>
    )
}
export default PanelEditorProductos