import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'
import { useTema } from '../../../hooks/useTema'
import Proyecto from '../../../model/Proyecto/Proyecto'
import Icono from '../../componentes-iconos/Icono/Icono'



const ExploradorGrupos = (props) =>  {
    const [grupoSeleccionado, setGrupoSeleccionado] = useState(buscarGrupoDeFichero(props.ficheroSeleccionado));
    const [ficheroSeleccionado, setFicheroSeleccionado] = useState(props.ficheroSeleccionado);



    useEffect(() => {
        setFicheroSeleccionado(props.ficheroSeleccionado)
        setGrupoSeleccionado(buscarGrupoDeFichero(props.ficheroSeleccionado))
    }, [props.ficheroSeleccionado])
    

    function buscarGrupoDeFichero(id){
        for(var i = 0; i < props.grupos.length; i++){
            for(var j = 0; j < props.grupos[i].ficheros.length; j++){
                if(props.grupos[i].ficheros[j].id == id){
                    return props.grupos[i].nombre;
                }
            }
        }
        return null;
    }

    const style = {
       display : "flex",
       flexDirection : "column",
       height: "100%",
       width: "100%",
       fontSize : "small",
       overflow : "auto",
       paddingTop : "10px",
       boxSizing : "border-box"
    }

    const onGrupoClickHandler = (nombre) =>{
        setGrupoSeleccionado(nombre)
    }


    const onFicheroClickHandler = (id) =>{
        setFicheroSeleccionado(id)
        props.onSeleccionaFichero(id);
    }
    
    const getGrupos = (gr) =>{

        function compare( a, b ) {
            if ( a.nombre < b.nombre ){
              return -1;
            }
            if ( a.nombre > b.nombre ){
              return 1;
            }
            return 0;
        }

        //Se ordenan los grupos
        gr.sort( compare );

        const elementos = [];


        for(var i = 0; i < gr.length; i++){
            var grupo = gr[i];
            let esGrupoSeleccionado = (grupoSeleccionado == grupo.nombre);
            elementos.push(<ElementoExplorador key={grupo.nombre} seleccionado={esGrupoSeleccionado} onClick={onGrupoClickHandler} id={grupo.nombre} nombre={grupo.nombre} tipo="grupo"></ElementoExplorador>);
            if(esGrupoSeleccionado){
                //Se ordenan los ficheros
                grupo.ficheros.sort( compare );
                for(var j = 0 ; j < grupo.ficheros.length; j++){
                    var fichero = grupo.ficheros[j];
                    let esFicheroSeleccionado = (ficheroSeleccionado == fichero.id);
                    elementos.push(<ElementoExplorador key={fichero.id} seleccionado={esFicheroSeleccionado}  onClick={onFicheroClickHandler} id={fichero.id} nombre={fichero.nombre} tipo="fichero"></ElementoExplorador>);
                }
            }
        }

         return elementos;
    }



    return (
       <div style={style}>
           {getGrupos(props.grupos)}
       </div>
    )
}


const ElementoExplorador = (props) =>  {

    const {getColor} = useTema();
    const [color, setColor]=useState("textosFicheros");

    const onClickHandler = () => {
        props.onClick(props.id)
    }


    const hoverIn = () =>{
        setColor("textosFicherosSeleccionado")
    }

    const hoverOut = () =>{
        setColor("textosFicheros")
    }


    const style = {
       //height: props.alto + "px",
       display : "table",
       width: "fit-content",
       //backgroundColor : "green",
       cursor : "pointer",
       marginLeft : props.indexado + "px",
       marginTop : props.separacion + "px",
       marginBottom : props.separacion + "px",
       color: getColor(color)
    }
    if(props.seleccionado){
        style.color = getColor("textosFicherosSeleccionado")
    }

    const styleInterior = {
        //height: props.alto + "px",
        display : "flex",
        width: "100%",
        height : "100%",

     }

     const styleTexto = {
        //height: props.alto + "px",
        display : "flex",
        width: "max-content",
        height : "100%",
        paddingLeft : props.espaciadoDeTextoIcono
     }

    let icono = <></> 
    if(props.tipo == "grupo"){
        style.marginLeft =  props.indexado + "px";
        let nombreIcono = "folder-closed";
        if(props.seleccionado){
            nombreIcono = "folder-opened";
        }
        icono = <Icono 
                    icon={nombreIcono} 
                    /*onClick={onGuardarHandler}*/  
                    size={props.tamanyoIconos}  
                    color={getColor("colorIconoCarpeta1")} 
                    colorh={getColor("colorIconoCarpeta1")}
                    color2={getColor("colorIconoCarpeta2")} 
                    colorh2={getColor("colorIconoCarpeta2")}
                ></Icono>
    }
    if(props.tipo == "fichero"){
        style.marginLeft =  (props.indexado * 2) + "px";
        icono =  <Icono 
                    icon="file" 
                    /*onClick={onGuardarHandler}*/  
                    size={props.tamanyoIconos}  
                    color={getColor("colorIconoFichero1")} 
                    colorh={getColor("colorIconoFichero1")}
                    color2={getColor("colorIconoFichero2")} 
                    colorh2={getColor("colorIconoFichero2")}
                ></Icono>
    }

    return (
        <div onClick={onClickHandler} style={style}>
            <div style={styleInterior}>
            {icono}
             <div style={styleTexto}>
                {props.nombre}
             </div>
             
            </div>
        </div>
     )

}

ElementoExplorador.defaultProps = {
    alto : PARAMETROS.explorador.altoGruposFicheros,
    separacion :  PARAMETROS.explorador.separacionGruposFicheros,
    indexado : 15,
    tamanyoIconos : 16,
    espaciadoDeTextoIcono: 8,
    seleccionado : false
}




export default ExploradorGrupos
