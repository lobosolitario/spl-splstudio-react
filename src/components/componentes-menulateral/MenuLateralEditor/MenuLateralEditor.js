
import React, { Component } from 'react'
import { Resizable, ResizableBox } from 'react-resizable';
import MenuLateral from '../../componentes-menulateral/MenuLateral/MenuLateral'
import {Link, useParams} from 'react-router-dom';
import {useEnrutador} from '../../../hooks/useEnrutador'
import { useHistory } from 'react-router-dom';
import { useProyecto } from '../../../hooks/useProyecto';


const MenuLateralEditor =  (props) => {

    const {layout, panel, panelLateral, menu, menuLateral, menuLateralSeleccionado, enruta} = useEnrutador()
    const {cerrarProyecto} = useProyecto();
    const history = useHistory();
    const idproyecto = 1; //TODO: Obtener el id del proyecto de useProyecto

    const definicionMenuLatereal =  [
        {   
            enganche:"E-LENG",
            icono:"book",
            accion : function(){
                history.push("/editor")
            }
        },
        {
            enganche:"E-PROD",
            icono:"bag",
            accion : function(){
                //alert("Click 2")
                history.push("/editor/productos/editar")
            }
        },
        {
            enganche:"E-PLAN",
            icono:"bagcode",
            accion : function(){
                history.push("/editor/plantillas/editar")
            }
        },
        {
            icono:"separador"
        },
        {
            enganche:"E-COMP",
            icono:"play",
            accion : function(){
                history.push("/editor/compilando")
            }
        }
        
    ]

    const styleContenedorPrincipal = {
        display: "flex",
        height: "100%",
        width: "100%"
    }
    
   let {id} = useParams()
   
   //console.log(id)
   return(
       <MenuLateral onResize={props.onResize} menu={definicionMenuLatereal}></MenuLateral>
    )
}


export default MenuLateralEditor