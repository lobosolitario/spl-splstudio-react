import React, {Component, useState, useEffect, useMemo} from 'react'

import { Resizable, ResizableBox } from 'react-resizable';
import {useEnrutador} from '../../../hooks/useEnrutador'

import BotoneraMenuLateral from '../../componentes-menulateral/BotoneraMenuLateral/BotoneraMenuLateral';
import SelectorColores from '../../../utils/SelectorColores.js';
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles';
import PARAMETROS from '../../../config/PARAMETROS.js'
import {useTema} from '../../../hooks/useTema'

const MenuLateral  = (props) => {

    const {layout, panel, panelLateral, menu, menuLateral} = useEnrutador()
    const {getColor} = useTema({});

    const minimoExpandido = PARAMETROS.editor.menuLateral.minimoExpandido;
    const maximoExpandido = PARAMETROS.editor.menuLateral.maximoExpandido;
 
    const [ancho, setAncho] = useState(global.anchoMenuLateral);
    const [anchoExpandido, setAnchoExpandido] = useState(minimoExpandido);
    const [expandido, setExpandido] = useState(true);



    useEffect(() => {
        global.anchoMenuLateral = ancho;
        try{
            props.onResize(ancho);
        }catch(error){
            console.log("No se encuentra onResize")
        }
    }, [ancho])

    useEffect(() => {
        if(!global.anchoMenuLateral){
            setAncho(minimoExpandido)
        }
    }, [])
    
    const onMouseDown = ev => {
       window.addEventListener("mousemove", onMouseMove);
       window.addEventListener("mouseup", onMouseUp);
    }

    const onMouseUp = ev => {
        window.removeEventListener("mousemove", onMouseMove);
        window.removeEventListener("mouseup", onMouseUp);
    }

    const onMouseMove = ev => {
        setAncho( ev.clientX)
        
    }

    const onBotonSeleccionado = ev => {
        setExpandido(!expandido )
    }

    const onExpandirHandler = ev => {
        setExpandido(true)
    }

    const onContraerHandler = ev => {
        setExpandido(false)
    }

    const getMenuLateral = () =>{
        var menu = props.menu.filter(menu => menu.enganche == [props.panel])[0];
        
        let colapsable = (panelLateral != undefined && panelLateral != null);
        if(expandido && colapsable){
            return getMenuLateralExpandido();
        }else{
            return getMenuLateralContraido();
        }
    }   
    
    const getMenu = () =>{
        var menu = props.menu.filter(menu => menu.enganche == [props.panel])[0];
        if(menu == undefined){
            return props.menu[0];
            
        }
        return menu;
     }

    
     const getMenuLateralExpandido = () =>{
        const style = {
            display:"flex",
            height: "100%",
            
        }
        const stylecontenedor = {
            display: "flex",
            height: "100%",
            width: ancho + "px",
            maxWidth : "70vw",
            minWidth : minimoExpandido + "px",
        }

        const stylecontenido = {
            width: "calc(100% - "+ PARAMETROS.editor.menuLateral.anchoBotonera +"px)"
        }

        const styleContenedorResize = {
            display: "flex",
            flexDirection: "column",
            width :  "3px"
        }

        const styleresize = {
            height: "100%",
            width:  "3px",
            backgroundColor : getColor("color3"),
            cursor: "w-resize"
        }

        return (
            <div className="nonSelectable" id="visor" style={style}>
                <div className="menuLateralBotonera" id="contedorPanelLateral" style={stylecontenedor}>
                    <BotoneraMenuLateral menu={props.menu} expandido={true} onExpandir={onExpandirHandler} onContraer={onContraerHandler}>

                    </BotoneraMenuLateral>
                    <div className="menuLateralContenedorPanel" style={stylecontenido}>
                        
                        {panelLateral}
                    </div>
                </div>
                <div  style={styleContenedorResize}>
                    
                    <div className="menuLateralBarraResizable" style={styleresize}
                        onMouseDown={e => onMouseDown(e)}
                    ></div>
                     
                </div>

            </div>
        )
    
    }
    
    MenuLateral.defaultProps = {
        anchoBarraResize : 3
    }
    
    const getMenuLateralContraido = () => {
        return <BotoneraMenuLateral menu={props.menu} expandido={false}  onExpandir={onExpandirHandler} onContraer={onContraerHandler} onClick={onBotonSeleccionado}>

        </BotoneraMenuLateral>
    }
    
   

    return (
        getMenuLateral()
    )
    
    /* 
    componentWillUnmount(){
        window.removeEventListener("mousemove", onMouseMove);
        window.removeEventListener("mouseup", onMouseUp);
    }
    */
}


export default MenuLateral;