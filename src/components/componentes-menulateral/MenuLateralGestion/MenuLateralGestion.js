
import React, { Component } from 'react'
import { Resizable, ResizableBox } from 'react-resizable';
import MenuLateral from '../../componentes-menulateral/MenuLateral/MenuLateral'
import {Link, useParams} from 'react-router-dom';
import {useEnrutador} from '../../../hooks/useEnrutador'
import { useHistory } from 'react-router-dom';


const MenuLateralGestion =  (props) => {

    const {layout, panel, panelLateral, menu, menuLateral, menuLateralSeleccionado, enruta} = useEnrutador()
    const history = useHistory();

    const definicionMenuLatereal =  [
        {   
            enganche:"G-HOME",
            icono:"home",
            accion : function(){
                history.push("/gestion/home")
            }
        },
        {
            enganche:"G-PROY",
            icono:"maleta",
            accion : function(){
                //alert("Click 2")
                history.push("/gestion/proyectos")
            }
        },
        {
            enganche:"G-CONF",
            icono:"settings",
            accion : function(){
                history.push("/gestion/configuracion")
            }
        },
        {
            icono:"separador"
        }
    ]

    const styleContenedorPrincipal = {
        display: "flex",
        height: "100%",
        width: "100%"
    }
    
   let {id} = useParams()
   
 
   return(
       <MenuLateral onResize={props.onResize} menu={definicionMenuLatereal}></MenuLateral>
    )
}


export default MenuLateralGestion