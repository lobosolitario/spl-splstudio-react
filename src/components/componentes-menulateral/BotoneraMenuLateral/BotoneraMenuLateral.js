import React, { Component } from 'react'
import Icono from "../../componentes-iconos/Icono/Icono"
import PARAMETROS from '../../../config/PARAMETROS.js'
import {TemaProvider, useTema} from '../../../hooks/useTema'
import {useEnrutador} from '../../../hooks/useEnrutador'

const BotoneraMenuLateral = (props) => {
    const {getColor} = useTema({});
    const {layout, panel, panelLateral, menu, menuLateral, menuLateralSeleccionado} = useEnrutador()

    const onClickHandler = (ev, params) => {
        try{ params.accion(); }catch(err){}  
        if(params.enganche === menuLateralSeleccionado){
            if(props.expandido){
                try{ props.onContraer(); }catch(err){}    
            }else{
                try{ props.onExpandir(); }catch(err){}      
            }
        } else{
        }
    } 

         

    const style = {
        display:"flex",
        flexDirection: "column",
        height: "100%",
        width:  PARAMETROS.editor.menuLateral.anchoBotonera + "px",
        paddingTop: (PARAMETROS.editor.altoBarraMenu  + 20)+ "px",
        boxSizing: "border-box",
        backgroundColor: getColor("color3")
    }


    const listItems = props.menu.map((menu, index) =>
        <Pestanya key={"iconoLateral" + index} onClick={onClickHandler} menu={menu}></Pestanya>

    );

    return (
        
        <div style={style}>
            {listItems}
        </div>
    )
    
}


function Pestanya(props){
    const {menuLateralSeleccionado} = useEnrutador()

    const {getColor} = useTema({});
    const styleSeparador = {
        height: "100%"
    }

    if(props.menu.icono == "separador"){
        return(
            <div className="separador" style={styleSeparador}></div>
        )
    }

    const styleItem = {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "150px",
        width:  PARAMETROS.editor.menuLateral.anchoBotonera + "px"
    }

    var color1=getColor("iconos1")
    var color2=getColor("iconos2")
    var colorFondoActivo=getColor("color2")

    if(menuLateralSeleccionado == props.menu.enganche){
        styleItem.backgroundColor = colorFondoActivo
        color1 = color2;
    }
    

    return (<div style={styleItem}>
         <Icono icon={props.menu.icono} onClick={props.onClick} onClickParams={props.menu} size={PARAMETROS.editor.menuLateral.tamanyoIconos}  color={color1} colorh={color2}></Icono>
    </div>)
}

export default BotoneraMenuLateral;
