import React, {useState, useEffect, useMemo} from 'react'

const MenuContextual = (props) =>  {

    const [visible, setVisible] = useState(false);
    const [coordenadas, setCoordenadas] = useState({ x: 0, y: 0 })

    let abrir = false;

    const style = {
       display : "none",
       position : "fixed",
       top : coordenadas.y,
       left : coordenadas.x,
       height: "200px",
       width: "200px",
       backgroundColor : "blue"
    }

    if(visible){
        style.display = "flex"
    }


    const asignarEventos = () =>{
        window.addEventListener("click", onExternalClickHandler);
        window.addEventListener("contextmenu", onExternalClickHandler);
        window.addEventListener("resize", onExternalClickHandler);
    }

    const eliminarEventos = () =>{
        window.removeEventListener("click", onExternalClickHandler);
        window.removeEventListener("contextmenu", onExternalClickHandler);
        window.removeEventListener("resize", onExternalClickHandler);
    }

    let onRightClickHandler = (e) =>{

        global.sel = props.id;

        setCoordenadas({ x: e.clientX, y: e.clientY});
       
        asignarEventos();
        setVisible(true)
        
    }


    const onExternalClickHandler = event => {
        // Se cierra si no ha sido el ultimo menu contextual en ser activado o si no se ha cliackado en un menu contextual
        if(global.sel != props.id || !hasSomeParentTheClass(event.target, "menuContextualEvent")){
            eliminarEventos()
            setVisible(false);
        }
    }

    useEffect(() => {
        return () => {
            eliminarEventos()
        }
    }, [])


    return (
            <>
           <div id={"ctx" + props.id} className="menuContextualEvent" onContextMenu={onRightClickHandler}>

           {props.children}
          
           </div>
           <div style={style} className="menuContextual">
                Menu contextual 1

           </div>
           </>
           
       
    )
}

function hasSomeParentTheClass(element, classname) {
    if(element.className == null){
        return false;
    }
    if (String(element.className).split(' ').indexOf(classname)>=0){
        return true;
    } 
    return element.parentNode && hasSomeParentTheClass(element.parentNode, classname);
}


export default MenuContextual
