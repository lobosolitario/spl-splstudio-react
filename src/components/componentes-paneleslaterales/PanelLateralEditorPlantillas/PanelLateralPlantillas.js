import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'
import ExploradorGrupos from '../../componentes-explorador/ExploradorGrupos/ExploradorGrupos'
import { useHistory } from 'react-router-dom';
import { useProyecto } from '../../../hooks/useProyecto';
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles';
import BotoneraMenuLateralPlantillas from '../../componentes-botones/BotoneraMenuLateralPlantillas/BotoneraMenuLateralPlantillas';

const PanelLateralPlantillas = (props) =>  {
    const history = useHistory();
    const {seleccionPlantilla, seleccionarPlantilla, proyecto} = useProyecto();

    const plantillas =  proyecto.plantillas;
    //console.log(plantillas)

    const style = {
       height: "calc(100% - " + PARAMETROS.editor.altoBarraMenu + "px",
       width: "100%",
    }

    const onSeleccionaFicheroHandler = (id) =>{
        
        seleccionarPlantilla(id)
        history.push('/editor/plantillas/editar')
    }

    const grupos = crearFicherosDePlantillas(plantillas);

    let grupoSeleccionado = null;
    let ficheroSeleccionado = null;
    if(seleccionPlantilla != null){
        ficheroSeleccionado = seleccionPlantilla.id;
        grupoSeleccionado = seleccionPlantilla.grupo;
    }
    return (
       <div className="contenedorPanelLateralPlantillas" style={style}>
           <BarraSuperiorPaneles>
                <BotoneraMenuLateralPlantillas></BotoneraMenuLateralPlantillas>
           </BarraSuperiorPaneles>
           <ExploradorGrupos 
           grupos={grupos}
           onSeleccionaFichero={onSeleccionaFicheroHandler}
           ficheroSeleccionado={ficheroSeleccionado}
           ></ExploradorGrupos>
       </div>
    )
}


function crearFicherosDePlantillas(plantillas){
    var grupos = [];
    for(var i = 0; i < plantillas.length; i++){
        var grupo = asignarGrupoDefault(plantillas[i].grupo);
        
        if(grupos.filter(g => g.nombre === grupo).length == 0){
            grupos.push({
                nombre :grupo,
                ficheros : []
            })
        }
        insertarPlantillaEnGrupo(grupos, plantillas[i] )
        
    }
    return grupos;
}

function insertarPlantillaEnGrupo(grupos, plantilla){
    const grupo = grupos.filter(g => g.nombre === asignarGrupoDefault(plantilla.grupo));
    if(grupo.length > 0){
        grupo[0].ficheros.push({
            id : plantilla.id,
            nombre : plantilla.nombre,
            modificado : plantilla.modificado
        })
    }
}

function asignarGrupoDefault(nombreGrupo){
    if(nombreGrupo == null || nombreGrupo == ""){
        return  "_default"
    }
    return nombreGrupo
}


export default PanelLateralPlantillas
