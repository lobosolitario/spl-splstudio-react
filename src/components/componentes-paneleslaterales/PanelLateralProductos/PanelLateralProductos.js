import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'
import ExploradorGrupos from '../../componentes-explorador/ExploradorGrupos/ExploradorGrupos'
import { useHistory } from 'react-router-dom';
import { useProyecto } from '../../../hooks/useProyecto';
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles';
import BotoneraMenuLateralPlantillas from '../../componentes-botones/BotoneraMenuLateralPlantillas/BotoneraMenuLateralPlantillas';
import BotoneraMenuLateralProductos from '../../componentes-botones/BotoneraMenuLateralProductos/BotoneraMenuLateralProductos';

const PanelLateralProductos = (props) =>  {
    const history = useHistory();
    const {seleccionProducto, seleccionarProducto, proyecto} = useProyecto();

    const productos =  proyecto.productos;


    const style = {
       height: "calc(100% - " + PARAMETROS.editor.altoBarraMenu + "px",
       width: "100%",
    }

    const onSeleccionaFicheroHandler = (id) =>{
        seleccionarProducto(id)
        history.push('/editor/productos/editar')
    }



    const grupos = crearFicherosDeProductos(productos);

    let grupoSeleccionado = null;
    let ficheroSeleccionado = null;
    if(seleccionProducto != null){
        ficheroSeleccionado = seleccionProducto.id;
        grupoSeleccionado = seleccionProducto.grupo;
    }
    return (
       <div className="contenedorPanelLateralPlantillas" style={style}>
           <BarraSuperiorPaneles>
                <BotoneraMenuLateralProductos></BotoneraMenuLateralProductos>
           </BarraSuperiorPaneles>
           <ExploradorGrupos 
           grupos={grupos}
           onSeleccionaFichero={onSeleccionaFicheroHandler}
           ficheroSeleccionado={ficheroSeleccionado}
           ></ExploradorGrupos>
       </div>
    )
}


function crearFicherosDeProductos(productos){
    var grupos = [];
    for(var i = 0; i < productos.length; i++){
        var grupo = asignarGrupoDefault(productos[i].grupo);
        
        if(grupos.filter(g => g.nombre === grupo).length == 0){
            grupos.push({
                nombre :grupo,
                ficheros : []
            })
        }
        insertarPlantillaEnGrupo(grupos, productos[i] )
        
    }
    return grupos;
}

function insertarPlantillaEnGrupo(grupos, producto){
    const grupo = grupos.filter(g => g.nombre === asignarGrupoDefault(producto.grupo));
    if(grupo.length > 0){
        grupo[0].ficheros.push({
            id : producto.id,
            nombre : producto.nombre,
            modificado : producto.modificado
        })
    }
}

function asignarGrupoDefault(nombreGrupo){
    if(nombreGrupo == null || nombreGrupo == ""){
        return  "_default"
    }
    return nombreGrupo
}


export default PanelLateralProductos
