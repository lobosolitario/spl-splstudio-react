import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'
import { useProyecto } from '../../../hooks/useProyecto'
import BotoneraMenuLateralCompilados from '../../componentes-botones/BotoneraMenuLateralCompilados/BotoneraMenuLateralCompilados'
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles'
import ExploradorGrupos from '../../componentes-explorador/ExploradorGrupos/ExploradorGrupos'
import { useHistory } from 'react-router-dom';

const PanelLateralCompilados = (props) =>  {
    const {proyecto, compilacion, seleccionarCompilado, seleccionCompilado } = useProyecto();
    const history = useHistory();
    
    const style = {
        height: "calc(100% - " + PARAMETROS.editor.altoBarraMenu + "px",
        width: "100%",
     }
 
    const onSeleccionaFicheroHandler = (id) =>{
        seleccionarCompilado(id)
        history.push("/editor/compilados")
        //seleccionarProducto(id)
        //history.push('/editor/productos/editar')
    }


    return (
        <div className="contenedorPanelLateralPlantillas" style={style}>
            <BarraSuperiorPaneles>
                <BotoneraMenuLateralCompilados></BotoneraMenuLateralCompilados>
            </BarraSuperiorPaneles>
            <ExploradorGrupos 
            grupos={crearFicherosDeCompilacion(compilacion)}
            onSeleccionaFichero={onSeleccionaFicheroHandler}
            ficheroSeleccionado={seleccionCompilado}
            ></ExploradorGrupos>
        </div>
     )


     function getGrupos(){
        console.log(compilacion) 
        var grupos = []
        if(compilacion != null){
            var grupo = {
                nombre :"grupo",
                ficheros : []
            }
            
            
            for(var i = 0; i < compilacion.compilaciones.length; i++){
                var cp = compilacion.compilaciones[i];
                //console.log(compilacion)
                
                grupo.ficheros.push({
                    id : cp.id,
                    nombre :cp.id,
                    modificado : false
                })
                
            }
            
            grupos.push(grupo)
        }


        return grupos;
     }


     function crearFicherosDeCompilacion(compilacion){
        var grupos = [];
        if(compilacion){
            for(var i = 0; i < compilacion.compilaciones.length; i++){
                var grupo = compilacion.compilaciones[i].configuracion.path;
                
                if(grupos.filter(g => g.nombre === grupo).length == 0){
                    grupos.push({
                        nombre :grupo,
                        ficheros : []
                    })
                }
                insertarCompilacionEnGrupo(grupos, compilacion.compilaciones[i] )
                
            }
        }

        return grupos;
    }
    
    function insertarCompilacionEnGrupo(grupos, compilacion){
        const grupo = grupos.filter(g => g.nombre === compilacion.configuracion.path);
        if(grupo.length > 0){
            grupo[0].ficheros.push({
                id : compilacion.id,
                nombre : compilacion.configuracion.name,
                modificado : false
            })
        }
    }


    function getNombreCompilacion(){
        //TODO: Checkear errores de tipo para mostrar el nombre junto a un error
    }
}

export default PanelLateralCompilados
