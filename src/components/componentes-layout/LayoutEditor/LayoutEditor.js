import React, {useState, useEffect, useMemo} from 'react'
import {TemaProvider, useTema} from '../../../hooks/useTema'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'

import PARAMETROS from '../../../config/PARAMETROS';
import SelectorColores from '../../../utils/SelectorColores';
import LayoutComponent from '../LayoutComponent/LayoutComponent'
import CabeceraConLogo from '../../componentes-comunes/CabeceraConLogo/CabeceraConLogo'
import VisorDoblePanel from '../../componentes-comunes/VisorDoblePanel/VisorDoblePanel';
import SelectorIdioma from '../../componentes-configuracion/SelectorIdioma/SelectorIdioma';
import {SelectorTema} from '../../componentes-configuracion/SelectorTema/SelectorTema.js'
import Cargando from '../../componentes-comunes/Cargando/Cargando'


import {useProyecto} from '../../../hooks/useProyecto'
import PanelError from '../../componentes-paneles/PanelError/PanelError';
import { useEnrutador } from '../../../hooks/useEnrutador';
import OperacionesCompilado from '../../../model/Operaciones/OperacionesCompilado';

 const LayoutEditor  = (props) => {

    const {proyecto, seleccionarProducto, seleccionProducto, seleccionarPlantilla, seleccionPlantilla, compilacion, seleccionCompilado, seleccionarCompilado} = useProyecto();

    const {menu} = useEnrutador();

    useEffect(() => {
        //Seleccion de producto y plantilla por defecto
        if(proyecto != null){
            if(seleccionProducto == null && proyecto.productos.length > 0){
                seleccionarProducto(proyecto.productos[0].id)
            }
            if(seleccionPlantilla == null && proyecto.plantillas.length > 0){
                seleccionarPlantilla(proyecto.plantillas[0].id)
            }
        }

    }, [proyecto])


    useEffect(() => {

        var operaciones = new OperacionesCompilado(compilacion);
        //Seleccion del compilado por defecto
        if(!operaciones.correspondeCompiladoProductoPlantilla(seleccionCompilado,seleccionProducto, seleccionPlantilla )){
            //Si el compilado no corresponde con el producto y la plantilla seleccionada se hace la seleccion
            var primerCompilado = operaciones.getPrimerProductoPlantilla(seleccionProducto, seleccionPlantilla);
            if(primerCompilado == null){
                //Si no existe un compilado que corresponda con el producto y la plantilla seleccionada se selecciona el primero
                var defCompilado = operaciones.getPrimerCompiladoSinSeleccion();
                if(defCompilado != null){
                    seleccionarCompilado(defCompilado.id)
                }
                seleccionarCompilado(null)
            }else{
                //Se setea el primer compilado
                seleccionarCompilado(primerCompilado.id)
            }
        }
    }, [seleccionPlantilla, seleccionProducto, compilacion])



    const crearLayout = () =>{
        var altoCabecera = PARAMETROS.editor.altoCabecera;
        var altoPie = PARAMETROS.editor.altoPie;
    
        var topCentral = altoCabecera;
        var bottomCentral = altoPie;
         
        var sc = new SelectorColores();
    
        const style = {
            height: "100%",
            width: "100%",
            minWidth: "500px",
            color : sc.getColor("textos", props.tema)
        }
        
        return <div className="layoutEditor" style={style}>
            <LayoutComponent  name="general" top="0" left="0" right="0" bottom="0"> 
                <LayoutComponent name="cabecera" color="color1" top="0" left="0" right="0" height={altoCabecera + "px"}>
                    <CabeceraConLogo>
                        {menu}
                    </CabeceraConLogo>
                </LayoutComponent>
                <LayoutComponent name="central" color="color2" top={topCentral + "px"} bottom={bottomCentral + "px"} left="0" right="0">
                    <VisorDoblePanel></VisorDoblePanel>
                </LayoutComponent>
                <LayoutComponent name="pie" color="color4" bottom="0" left="0" right="0" height={altoPie + "px"}></LayoutComponent>
        </LayoutComponent>
        </div>
    }



    
    const crearLayoutError = () =>{
        var altoCabecera = PARAMETROS.editor.altoCabecera;
        var altoPie = PARAMETROS.editor.altoPie;
    
        var topCentral = altoCabecera;
        var bottomCentral = altoPie;
         
        var sc = new SelectorColores();
    
        const style = {
            height: "100%",
            width: "100%",
            minWidth: "500px",
            color : sc.getColor("textos", props.tema)
        }
        console.log("layour principal")
        return <div style={style}><LayoutComponent name="general" top="0" left="0" right="0" bottom="0"> 
                <LayoutComponent name="cabecera" color="color1" top="0" left="0" right="0" height={altoCabecera + "px"}>
                    <CabeceraConLogo></CabeceraConLogo>
                </LayoutComponent>
                <LayoutComponent name="central" color="color2" top={topCentral + "px"} bottom={bottomCentral + "px"} left="0" right="0">
                    <PanelError></PanelError>
                </LayoutComponent>
                <LayoutComponent name="pie" color="color4" bottom="0" left="0" right="0" height={altoPie + "px"}></LayoutComponent>
        </LayoutComponent>
        </div>
    }

  
    
    if(proyecto != null){
        return crearLayout();
    }
    return crearLayoutError();
    
}


export default LayoutEditor