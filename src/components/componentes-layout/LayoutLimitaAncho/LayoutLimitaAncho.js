import React, {useState, useEffect, useMemo} from 'react'
import PARAMETROS from '../../../config/PARAMETROS'

const LayoutLimitaAncho = (props) =>  {
    const style = {
       display : "flex",
       justifyContent : "center",
       height: "100%",
       width: "100%",
    }

    const styleInterno = {
       height: "100%",
       width: "100%",
       maxWidth : props.limite + "px",
       //backgroundColor : "red"
    }

    return (
       <div style={style}>
           <div style={styleInterno}>
                {props.children}
           </div>
       </div>
    )
}

LayoutLimitaAncho.defaultProps = {
    limite : 600
}

export default LayoutLimitaAncho
