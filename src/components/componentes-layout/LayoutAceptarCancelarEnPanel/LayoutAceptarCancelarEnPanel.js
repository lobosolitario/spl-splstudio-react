import LayoutAceptarCancelar from '../../componentes-layout/LayoutAceptarCancelar/LayoutAceptarCancelar';
import { useLiterales } from '../../../hooks/useLiterales';
import { useTema } from '../../../hooks/useTema'
import Titulo from '../../componentes-atomicos/Titulo/Titulo';
import BarraSuperiorPaneles from '../../componentes-comunes/BarraSuperiorPaneles/BarraSuperiorPaneles';
import PARAMETROS from '../../../config/PARAMETROS';
import LayoutLimitaAncho from '../LayoutLimitaAncho/LayoutLimitaAncho';

const LayoutAceptarCancelarEnPanel = (props) =>  {
    const {getColor} = useTema();
    const {getLiteral} = useLiterales();

    const style = {
       height: "100%",
       width: "100%",
    }

    
    const styleContenedorFormulario = {
       height: "calc(100% - " + PARAMETROS.editor.altoBarraMenu + "px)",
       width: "100%",
       padding : "20px",
       boxSizing : "border-box"
    }

    const divSeparador = {
        width : "100%",
        height : "25px"
    }



    return (

        
        <div style={style}>
            <BarraSuperiorPaneles></BarraSuperiorPaneles>
            <div style={styleContenedorFormulario}>
                <LayoutLimitaAncho>
            <LayoutAceptarCancelar
            panel={props.panel}
            colorBotones = {getColor("textos")}
            onAceptar={props.onAceptar}    
            titulo={<Titulo texto={props.titulo} tamanyo={2}></Titulo>}
            onCancelar={props.onCancelar} 
        ></LayoutAceptarCancelar>
        </LayoutLimitaAncho>
        </div>
            
        </div>
    )
}

export default LayoutAceptarCancelarEnPanel