import React, {useState, useEffect, useMemo} from 'react'
import { useEnrutador } from '../../../hooks/useEnrutador'
import { useTema } from '../../../hooks/useTema';

const LayoutPantallaCompleta = (props) =>  {
    const {panel} = useEnrutador()
    const {getColor} = useTema();

    const style = {
       display : "flex",
       justifyContent : "center",
       height: "100%",
       width: "100%",
       overflow: "auto",
       backgroundColor: getColor("color2"),
    }


    return (
       <div className="layoutPantallaCompleta" style={style}>
           {panel}
       </div>
    )
}

LayoutPantallaCompleta.defaultProps = {
    panel : <></>
}

export default LayoutPantallaCompleta