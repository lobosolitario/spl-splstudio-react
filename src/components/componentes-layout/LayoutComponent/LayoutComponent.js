import React, {useState, useEffect, useMemo} from 'react'
import {TemaProvider, useTema} from '../../../hooks/useTema'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'

const LayoutComponent = (props) =>{
    const {getColor} = useTema({});

    const style = {
        position: "absolute",
        left: props.left,
        right: props.right,
        top: props.top,
        bottom: props.bottom,
        height: props.height,
        width: props.width,
        backgroundColor :  getColor(props.color)
    }

    let onRightClickHandler = (e) =>{
        e.preventDefault();
        
        return false;
    }


    return (
        <div id={"layout" + props.name} onContextMenu={onRightClickHandler} style={style}>
            {props.children}
        </div>
    )
    
}

export default LayoutComponent


