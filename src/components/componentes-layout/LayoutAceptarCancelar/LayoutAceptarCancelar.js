import React, {useState, useEffect, useMemo} from 'react'
import Titulo from '../../componentes-atomicos/Titulo/Titulo'
import BotoneraInferior from '../../componentes-botones/BotoneraInferior/BotoneraInferior'
import LayoutContenedorBotoneras from '../LayoutContenedorBotoneras/LayoutContenedorBotoneras'
import {useLiterales} from '../../../hooks/useLiterales'
import Separador from '../../componentes-atomicos/Separador/Separador'

const LayoutAceptarCancelar = (props) =>  {
    const{getLiteral} = useLiterales();



    const onBotonHandler = (accion) => {

        switch (accion) {
            case "Aceptar":
                props.onAceptar();
                break;
            case "Cancelar":
                props.onCancelar();
                break;
            default:
                break;
        }
    }

    
    const botonesInferiores = [
        {
            id : "Cancelar",
            icono: "",
            texto : getLiteral("commonsCancelar"),
        },
        {
            id : "Aceptar",
            icono: "",
            texto : getLiteral("commonsAceptar"),
        },
        
    ]

    return (
        <LayoutContenedorBotoneras
        titulo={props.titulo}
        separadorSuperior={ <Separador altura={props.separacionSuperior} color="none"></Separador>}
        sepraadorInferior={<Separador altura={props.separacionInferior} color="none"></Separador>}
        botoneraInferior={<BotoneraInferior colorBoton={props.colorBotones} botones={botonesInferiores} onClick={onBotonHandler}></BotoneraInferior>}
        panel={props.panel}
    >
        
    </LayoutContenedorBotoneras>
    )
}

LayoutAceptarCancelar.defaultProps = {
    colorBotones : "blue",
    tiulo : <></>
}


export default LayoutAceptarCancelar
