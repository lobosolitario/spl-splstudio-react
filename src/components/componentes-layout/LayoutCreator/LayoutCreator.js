import React, {useState, useEffect, useMemo} from 'react'

import LayoutEditor from '../LayoutEditor/LayoutEditor.js'
import LayoutGestion from '../LayoutGestion/LayoutGestion.js'
import {useEnrutador, Enrutador} from '../../../hooks/useEnrutador'

 const LayoutCreator = (props) => {
    const {layout} = useEnrutador()

    const style = {
        height: "100%",
        width: "100%"
    }

    return (
        <div style={style} className="layoutCreator">
           {layout}
        </div>
    )
}   


export default LayoutCreator
