import React, {useState, useEffect, useMemo} from 'react'
import {TemaProvider, useTema} from '../../../hooks/useTema'
import {LiteralesProvider, useLiterales} from '../../../hooks/useLiterales'

import PARAMETROS from '../../../config/PARAMETROS';
import SelectorColores from '../../../utils/SelectorColores';
import LayoutComponent from '../../componentes-layout/LayoutComponent/LayoutComponent'
import CabeceraConLogo from '../../componentes-comunes/CabeceraConLogo/CabeceraConLogo'
import VisorDoblePanel from '../../componentes-comunes/VisorDoblePanel/VisorDoblePanel';
import SelectorIdioma from '../../componentes-configuracion/SelectorIdioma/SelectorIdioma';
import {SelectorTema} from '../../../components/componentes-configuracion/SelectorTema/SelectorTema.js'
import Cargando from '../../componentes-comunes/Cargando/Cargando'


const LayoutGestion = (props) => {

    const {getColor} = useTema({});

    
    const altoCabecera = PARAMETROS.editor.altoCabecera;
    const altoPie = PARAMETROS.editor.altoPie;

    const topCentral = altoCabecera;
    const bottomCentral = altoPie;


    const style = {
        height: "100%",
        width: "100%",
        minWidth: "500px",
        color : getColor("textos")
    }
    
    return <div id="layoutGestion" style={style}>      
        <LayoutComponent name="general" top="0" left="0" right="0" bottom="0"> 
            <LayoutComponent name="cabecera" color="color1" top="0" left="0" right="0" height={altoCabecera + "px"}>
                <CabeceraConLogo>
                    
                </CabeceraConLogo>
            </LayoutComponent>
            <LayoutComponent name="central" color="color2" top={topCentral + "px"} bottom={bottomCentral + "px"} left="0" right="0">
                <VisorDoblePanel></VisorDoblePanel>
            </LayoutComponent>
            <LayoutComponent name="pie" color="color4" bottom="0" left="0" right="0" height={altoPie + "px"}></LayoutComponent>
    </LayoutComponent>
    </div>
}


export default LayoutGestion