
const LayoutContenedorBotoneras = (props) =>  {
    
    const style = {
        //display:"flex",
        flexDirection:"column",
        height: "calc(100% - " +(props.altoBotoneraInferior + props.separacionInferior + props.altoContenedorTitulo) +"px)",
        width: "100%",
    }

    const styleContenedorInterior = {
        display:"flex",
        height: "calc(100% - " + (props.altoBotoneraInferior + props.separacionInferior) + "px)",
        width: "100%",
    }

    const styleContenedorSuperior = {
        display:"flex",
        height: props.altoContenedorTitulo + "px",
        width: "100%",
    }


    const styleContenedorBotonera = {
        display:"flex",
        height: "100%",
        width:  props.anchoBotonera + "px",
        //backgroundColor : "orange"
    }

    const styleContenedorPrincipal = {
        display:"flex",
        height: "100%",
        width:  "calc(100% - " + props.anchoBotonera + "px)",
        backgroundColor : props.color,
        overflow :"auto",
        flexDirection :"column"
    }

    const styleContenedorBotoneraInferior = {
        display:"flex",
        height: (props.altoBotoneraInferior + props.separacionInferior) + "px",
        width: "100%",
    }

    if(props.botoneraLateral){
        return (
            <div className="layoutContenedorConBotoneras"  style={style}>
                <div className="contenedorSuperior" style={styleContenedorSuperior}>
                    {props.titulo}
                </div>
                <div className="contenedorInterior" style={styleContenedorInterior}>
                   
                    <div style={styleContenedorBotonera}>
                       {props.botoneraLateral}
                    </div>
                    <div className="contenedorPrincipal" style={styleContenedorPrincipal}>
                        {props.panel}
                    </div>
                </div>
                {props.sepraadorInferior}
                <div style={styleContenedorBotoneraInferior}>
                     {props.botoneraInferior}
                </div>
            </div>
        )
    }else{
        styleContenedorPrincipal.width = "100%"
        return (
            <div  className="layoutContenedorConBotoneras"  style={style}>
                <div className="contenedorSuperior" style={styleContenedorSuperior}>
                    {props.titulo}
                </div>
                <div className="contenedorInterior" style={styleContenedorInterior}>
                    <div className="contenedorPrincipal" style={styleContenedorPrincipal}>
                        {props.panel}
                    </div>
                </div>
                {props.sepraadorInferior}
                <div style={styleContenedorBotoneraInferior}>
                     {props.botoneraInferior}
                </div>
            </div>
        )
    }


    
}


LayoutContenedorBotoneras.defaultProps = {
    color : "none",
    anchoBotonera : 60,
    altoBotoneraInferior: 40,
    altoContenedorTitulo: 40,
    separacionInferior : 0,
    separacionSuperior : 10
};


export default LayoutContenedorBotoneras