import React, {Component, useState, useEffect, useMemo} from 'react'
import {UnControlled as CodeMirror} from 'react-codemirror2'
import {useTema} from '../../hooks/useTema';

import './Editor.css';
import 'codemirror/lib/codemirror.css';

import '../../config/EditorTemas/drk.css'
import '../../config/EditorTemas/lth.css'
import '../../config/EditorTemas/rdt.css'

require('codemirror/mode/htmlmixed/htmlmixed');
require('codemirror/addon/mode/multiplex');


//require('codemirror/mode/splproductos/splproductos');


const Editor = (props) => {
    try{
        console.log('codemirror/mode/'+props.lenguaje+'/'+props.lenguaje)
        require('codemirror/mode/'+props.lenguaje+'/'+props.lenguaje);
    }catch(error){ 
        //console.log(error)
    }

  

    const {tema} = useTema({});
    const [texto, setTexto] = useState(props.texto);

    useEffect(() => {
        setTexto(props.texto)
    }, [props.texto])

    //Creacion de tokens
    if(global.controllerSplProductos){
        console.log("Creando toekns")
        global.controllerSplProductos(props.tokens);
    }


    if(global.controllerSplFamilias){
        console.log("Creando toekns para familias")
        global.controllerSplFamilias(props.tokens, props.lenguajeExterno);
    }
    
    let mode = props.lenguaje;
    if(props.lenguajeExterno != ""){
        mode = mode + "/" + props.lenguajeExterno
    }
    

    return (        
    <CodeMirror
    
    value={texto}
    editorDidMount={editor => { }}
    options={{
        mode: mode,
        theme: tema,
        lineNumbers: true,
        scrollbarStyle:'native'
        
    }}
    onChange={(editor, data, value) => {      
        props.onChange(null, null, value)
    }}
    />
    )
    
}

Editor.defaultProps= {
    lenguaje : "",
    tokens : [],
    lenguajeExterno : ""
}

export default Editor;