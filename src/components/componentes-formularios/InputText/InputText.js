import React, {useState, useEffect, useMemo} from 'react'

const InputText = (props) => {
   
    const [valor ,setValor] = useState(props.defval)
    
    const handleChange = (event) => {
        setValor( event.target.value);
        try{
            props.onChange(props.nombre, event.target.value);
        }catch(err){

        }
    }

    useEffect(() => {
        setValor(props.defval)
    }, [props.defval])

    const styleTitulo = {
        color : props.colorTitulo
    }
    const styleInput = {
        backgroundColor : props.color,
        color : props.colorTexto
    }

    if(props.titulo){
        return (
            <label className="formularioInput">
              <div style={styleTitulo} className="tituloFormulario tituloInputText">{props.titulo}:</div>
              <input style={styleInput} className="formularioInputText" type="text" name="props" autoComplete={props.autocompletable} value={valor} onChange={handleChange}/>
            </label>
        )
    }else{
        return (
            <label className="formularioInput">
              <input style={styleInput} className="formularioInputText" type="text" name="props" autoComplete={props.autocompletable} value={valor} onChange={handleChange}/>
            </label>
        )
    }


    
}

InputText.defaultProps = {
    color: "transparent",
    colorTexto : "green",
    colorTitulo : "blue",
    autocompletable : "off"
}



export default InputText