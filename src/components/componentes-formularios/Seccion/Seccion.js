import React, {useState, useEffect, useMemo} from 'react'
import { useTema } from '../../../hooks/useTema'
import Separador from '../../componentes-atomicos/Separador/Separador'
import Titulo from '../../componentes-atomicos/Titulo/Titulo'
import Icono from '../../componentes-iconos/Icono/Icono'

const Seccion = (props) =>  {
    const [desplegado, setDesplegado] = useState(props.inicioDesplegado)
    const {getColor} = useTema();
    const [color, setColor] = useState(0);

    const onClickHandler = () => {
        setDesplegado(!desplegado)
    }  

    const hoverIn = () =>{
        setColor(1)
    }

    const hoverOut = () =>{
        setColor(0)
    }

    const style = {
       width: "100%",
       border: "1px solid transparent",
       boxSizing:" border-box"
    }

    const styleBarraSuperior = {
        display:"flex",
        height: props.altoBarra + "px",
        width: "100%",
        backgroundColor : getColor(props.colorBarra),
        justifyContent: "space-between",
        alignItems : "center",
        paddingLeft : props.padding + "px",
        paddingRight : props.padding + "px",
        boxSizing:" border-box",
        cursor : "pointer"
     }

     const styleBarraSuperiorDerecha = {
        
     }

     const styleBarraSuperiorIzquierda = {
   
     }

     const colores = [];
     colores[0] = {
        colorTexto : props.colorTexto,
        colorIcono : props.colorIconos
     }
     colores[1] = {
        colorTexto : props.colorIconosh,
        colorIcono : props.colorTextoh
     }

     const styleContenido = {
        paddingLeft : props.padding + "px",
        paddingRight : props.padding + "px",
        boxSizing:" border-box",
        paddingBottom : props.paddingBottom + "px",
        border : "solid 1px " + getColor(props.colorBarra)
     }
     
     var icon = "expandless";
     if(!desplegado){
        styleContenido.display = "none"
        icon = "expandmore";
     }

     return (
       <div style={style} className="nonSelectable">
           <div  style={styleBarraSuperior} onClick={onClickHandler} onMouseEnter={hoverIn} onMouseLeave={hoverOut}>
               <div style={styleBarraSuperiorIzquierda}>
                    <Titulo color={ colores[color].colorTexto} texto={props.nombre} alineamiento="flex-start" tamanyo={props.tamanyo}></Titulo>
               </div>
               <div  style={styleBarraSuperiorDerecha}>
                    <Icono icon={icon}   size={props.size}  color={getColor( colores[color].colorIcono)} colorh={getColor(props.colorIconosh)}></Icono>
                </div>

           </div>
           <div style={styleContenido}>
             {props.children}
           </div>
       </div>
    )
}

Seccion.defaultProps = {
    inicioDesplegado : true,
    nombre : "",
    color : "iconos1",
    colorh : "iconos2",
    size : 20,
    tamanyo : 2,
    colorBarra : "color1",
    colorContenido : "color2",
    altoBarra : 40,
    padding : 10,
    paddingBottom : 20,
    colorIconos : "iconos1",
    colorIconosh : "iconos2",
    colorTexto : "textos",
    colorTextoh : "iconos2",
}

export default Seccion
