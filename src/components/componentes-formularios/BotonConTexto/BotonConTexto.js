import React, {useState, useEffect, useMemo} from 'react'
import { useTema } from '../../../hooks/useTema'
import BotonGenerico from '../../componentes-botones/BotonGenerico/BotonGenerico'

const BotonConTexto = (props) =>  {
    const {getColor} = useTema();

    const style = {
       display : "flex",
       justifyContent: "space-between",
       alignItems : "center",
       paddingTop : props.paddingTop + "px"
    }

    const styleTexto = {
        height: "100%",
        color : getColor(props.colorTexto)
     }
     
     const styleBoton = {
        height: "100%",
        width: "100%",
     }

     const boton = {
        id : "Cancelar",
        icono: "",
        texto : props.textoBoton,
    }

    return (
       <div style={style}>
           <div style={styleTexto}>
               {props.texto}
           </div>
           <div>
                <BotonGenerico
                    onClick={props.onClick} 
                    boton={boton}
                    color={getColor(props.color)}
                ></BotonGenerico>
           </div>
       </div>
    )
}



BotonConTexto.defaultProps = {
    texto : "",
    textoBoton : "",
    tamanyo : 2,
    color : "iconos1",
    colorTexto : "textos",
    paddingTop : 20
}

export default BotonConTexto
