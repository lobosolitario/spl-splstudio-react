import React, {useState, useEffect, useMemo} from 'react'
import { useTema } from '../../../hooks/useTema'
import BotonGenerico from '../../componentes-botones/BotonGenerico/BotonGenerico'

const SeleccionFichero = (props) =>  {
    const {getColor} = useTema();

    const [valor ,setValor] = useState(props.defval)
    
    const handleChange = (event) => {
        let file = event.target.files[0]
        setValor( file);
        try{
            props.onChange(props.nombre, file);
        }catch(err){

        }
    }

    const style = {
       display : "flex",
       justifyContent: "space-between",
       alignItems : "center",
       paddingTop : props.paddingTop + "px"
    }

 
     
     const styleBoton = {
        color : props.colorTitulo,
        cursor: "pointer"
     }


    const styleTitulo = {
        color : props.colorTitulo
    }
 


    return (
       <label className="formularioInput">
        <div style={styleTitulo} className="tituloFormulario tituloInputText">{props.titulo}:</div>
        <input style={styleBoton} className="formularioInputText" type="file" name="props" onChange={handleChange}/>
      </label>

    )
}



SeleccionFichero.defaultProps = {
    texto : "",
    textoBoton : "",
    tamanyo : 2,
    color : "iconos1",
    colorTexto : "textos",
    paddingTop : 20,
    colorTitulo : "textos",
}

export default SeleccionFichero
