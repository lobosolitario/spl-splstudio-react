import React, {useState, useEffect, useMemo} from 'react'

const TextArea = (props) => {
   
    const [valor ,setValor] = useState(props.defval)
    
    const handleChange = (event) => {
        setValor( event.target.value);
        try{
            props.onChange(props.nombre, event.target.value);
        }catch(err){

        }
    }

    const styleTitulo = {
        color : props.colorTitulo
    }
    const styleInput = {
        backgroundColor : props.color,
        color : props.colorTexto
    }



    return (
        <label className="formularioInput">
          <div style={styleTitulo} className="tituloFormulario tituloTextArea">{props.titulo}:</div>
          <textarea style={styleInput} className="formularioTextArea"  name="props"  defaultValue={valor}  onChange={handleChange}>
            
         </textarea>
        </label>
    )
    
}

TextArea.defaultProps = {
    color: "transparent",
    colorTexto : "green",
    colorTitulo : "blue",
    defval : ""
}



export default TextArea