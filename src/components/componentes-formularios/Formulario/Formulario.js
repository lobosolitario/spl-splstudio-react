import React, {useState, useEffect, useMemo} from 'react'


const Formulario = (props) =>  {
    const style = {
        fontSize : props.fontSize,
        fontWeight: props.fontWeight
    }


    require('./formularios.css');
     

    return (
       <div className={"formulario" + props.estilo} style={style}>
           {props.inputs}
       </div>
    )
}

Formulario.defaultProps = {
    estilo : 0,
    color: "none",
    colorTexto : "green",
    colorTitulo : "blue",
    fontSize : "15px",
    fontWeight: "bold"
}

export default Formulario;
