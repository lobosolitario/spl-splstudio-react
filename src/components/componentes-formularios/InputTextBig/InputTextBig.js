import React, {useState, useEffect, useMemo} from 'react'
import './InputTextBig.css';

const InputTextBig = (props) => {
   
    const [valor ,setValor] = useState(props.defval)
    
    const handleChange = (event) => {
        setValor( event.target.value);
        try{
            props.onChange(props.nombre, event.target.value);
        }catch(err){

        }
    }

    useEffect(() => {
        setValor(props.defval)
    }, [props.defval])

    const styleTitulo = {
        color : props.colorTitulo,
        marginBottom : "5px",
        width: props.ancho + (2 * props.padding) + "px"
    }
    const styleInput = {
        backgroundColor: props.color,
        borderColor: props.borderColor,
        color: props.colorTexto,
        width:props.ancho + "px",
        padding:props.padding + "px"
    }

    const styleError = {
        backgroundColor: props.color,
        borderColor: props.borderColor,
        color: props.errorColor,
        width:props.ancho + "px",
        padding:props.padding + "px",
        fontStyle: "italic",
        fontWeight: "lighter"
    }

    let error = <></>
    if(props.error){
        styleInput.borderColor = props.errorColor;
        if(props.error.trim() != ""){
            error = <div style={styleError}>
                {props.error}
            </div>
        }

    }

    if(props.titulo){
        return (
            <>
              <div style={styleTitulo} className="tituloFormulario tituloInputText">{props.titulo}:</div>
              <input className="inputTextBigFormulario" style={styleInput}  type="text" name="props" autoComplete={props.autocompletable} value={valor} onChange={handleChange}/>
              {error}  
            </>
        )
    }else{
        return (
            <>
              <input className="inputTextBigFormulario" style={styleInput}  type="text" name="props" autoComplete={props.autocompletable} value={valor} onChange={handleChange}/>
              {error}  
            </>
        )
    }


    
}

InputTextBig.defaultProps = {
    color: "white",
    colorTexto : "black",
    colorTitulo : "black",
    borderColor : "black",
    errorColor: "red",
    autocompletable : "off",
    ancho : 300,
    padding : 7
}



export default InputTextBig