import PARAMETROS from "../../../config/PARAMETROS";
import { useTema } from "../../../hooks/useTema";

const Titulo = (props) =>  {
    const {getColor} = useTema();

    var tamanyos = [
        PARAMETROS.textos.tamanyoTextos1, 
        PARAMETROS.textos.tamanyoTextos2, 
        PARAMETROS.textos.tamanyoTextos3, 
        PARAMETROS.textos.tamanyoTextos4]

    const style = {
        display:"flex",
        width: "100%",
        color: props.color,
        justifyContent: props.alineamiento,
        color : getColor(props.color),
        fontSize : tamanyos[props.tamanyo] + "px"
    }

    if(props.maxHeight > 0){
        style.maxHeight = props.maxHeight + "px";
    }

    if(props.negrita){
        style.fontWeight = "bold";
    }

    var className = "titulo";
    if(!props.selectable){
        className = "titulo nonSelectable";
    }

    return (
        <div style={style} className={className}>
            {props.texto}
        </div>
    )
    
}



Titulo.defaultProps = {
    color : "textos",
    alineamiento: "center", //flex-start, center, flex-end
    texto : "",
    selectable : true,
    maxHeight : 0,
    tamanyo : 1,
    negrita : false
};

export default Titulo