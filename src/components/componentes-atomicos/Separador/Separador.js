const Separador = (props) =>  {


    const style = {
        height: props.altura,
        width: "100%",
        backgroundColor: props.color,
        marginTop : props.margenSuperior,
        marginBottom : props.margenInferior
    }

    const styleHorizontal = {
        width: props.altura,
        height: "100%",
        backgroundColor: props.color,
        marginLeft : props.margenSuperior,
        marginRight : props.margenInferior
    }

    if(props.horizontal){
        return (
            <div style={styleHorizontal}>
                
            </div>
        )
    }else{
        return (
            <div style={style}>
                
            </div>
        )
    }

    
}

Separador.defaultProps = {
    color : "none",
    altura: "2px",
    margenSuperior : "5px",
    margenInferior : "2px",
    horizontal : false
};

export default Separador