import React, {useState, useEffect, useMemo} from 'react'



 const Link = (props) => {
    const [color, setColor] = useState(props.color);

    const style = {
        cursor:"pointer",
        color : color
    }

    const onClickHandler = event => {
        props.accion();
    }

    const hoverIn = () =>{
        setColor(props.colorh)
    }

    const hoverOut = () =>{
        setColor(props.color)
    }

    return (
        <a style={style} onClick={onClickHandler} onMouseEnter={hoverIn} onMouseLeave={hoverOut}>
            {props.texto}
        </a>
    )
    
}

Link.defaultProps = {
    accion: function(){alert("Tocado")},
    texto: "mi link",
    color: "white",
    colorh: "white"
}

export default Link