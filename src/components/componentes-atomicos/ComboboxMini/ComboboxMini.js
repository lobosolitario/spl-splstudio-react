import React, {useState, useEffect, useMemo} from 'react'
import { useTema } from '../../../hooks/useTema';


 const ComboboxMini = (props) => {

    const [value , setValue] = useState(props.valor);
    const {getColor} = useTema();

    const style = {
        color: getColor(props.colorTexto),
        width: "100px",
        height: "25px",
        fontSize: "12px",
        backgroundColor: getColor(props.color),
        border :"none"
    }

    const onChangeHandler = event => {
        //setState({value:  event.target.value});
        if(props.onChange != undefined){
            props.onChange( event.target.value);
        }
        //setValue( event.target.value)
    }

  
    return (
        <select style={style} onChange={onChangeHandler} name={props.nombre}  value={props.valor}>
            {props.opciones.map(function(opcion, index){
                return <option key={ index } value={opcion.valor} >{opcion.nombre}</option>;
                })}
        </select>
    )
    
}

ComboboxMini.defaultProps = {
    color: "color2",
    colorTexto: "textos",
}

export default ComboboxMini