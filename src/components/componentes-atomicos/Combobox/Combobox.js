import React, {useState, useEffect, useMemo} from 'react'
import { useTema } from '../../../hooks/useTema';


 const Combobox = (props) => {

    const [value , setValue] = useState(props.valor);
    const {getColor} = useTema();

    const style = {
        color: getColor(props.colorTexto),
        width: "400px",
        height: "35px",
        fontSize: "15px",
        backgroundColor: getColor(props.color)
    }

    const onChangeHandler = event => {
        //setState({value:  event.target.value});
        if(props.onChange != undefined){
            props.onChange( event.target.value);
        }
        //setValue( event.target.value)
    }

  
    return (
        <select style={style} onChange={onChangeHandler} name={props.nombre}  value={props.valor}>
            {props.opciones.map(function(opcion, index){
                return <option key={ index } value={opcion.valor} >{opcion.nombre}</option>;
                })}
        </select>
    )
    
}

Combobox.defaultProps = {
    color: "color1",
    colorTexto: "textos",
}

export default Combobox