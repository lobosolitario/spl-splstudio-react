import ApiProyecto from './ApiProyecto'

export default class ServiceSelector {

    apiProyecto = new ApiProyecto();

    cargarProyecto(callbackOk, callbackNok) {
        return this.apiProyecto.cargarProyecto(callbackOk, callbackNok);
    }


    listarProyectos(callbackOk, callbackNok) {
        return this.apiProyecto.listarProyectos(callbackOk, callbackNok);
    }
    

    nuevoProyecto(callbackOk, callbackNok) {
        return this.apiProyecto.nuevoProyecto(callbackOk, callbackNok);
    }


}