export default class ApiProyecto {
    conexion = "http://127.0.0.1:5101"

    cargarProyecto(callbackOk, callbackNok) {
      fetch(this.conexion + "/listar")
      .then(res => res.json())
      .then(data => {
          callbackOk(data);
      }).catch(function(e) {
          callbackNok(e);
      });
      
      return "";
    }


    listarProyectos(callbackOk, callbackNok) {
        fetch(this.conexion + "/listar")
        .then(res => res.json())
        .then(data => {
            callbackOk(data);
        }).catch(function(e) {
            callbackNok(e);
        });
        
        return "";
      }

}