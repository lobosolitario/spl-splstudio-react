import React, {Component, useState, useEffect, useMemo} from 'react'
import ServiceSelector from '../services/ServiceSelector';
import SelectorColores from '../utils/SelectorColores';
import { BrowserRouter as Router, Route, useLocation, useParams, Switch, Redirect } from 'react-router-dom';
import {useProyecto} from './useProyecto'
import { useHistory } from 'react-router-dom';
import LayoutCreator from '../components/componentes-layout/LayoutCreator/LayoutCreator'
import LayoutEditor from '../components/componentes-layout/LayoutEditor/LayoutEditor'
import LayoutGestion from '../components/componentes-layout/LayoutGestion/LayoutGestion'
import MenuLateralGestion from '../components/componentes-menulateral/MenuLateralGestion/MenuLateralGestion';
import PanelProyectos from '../components/componentes-paneles/PanelProyectos/PanelProyectos';
import MenuLateralEditor from '../components/componentes-menulateral/MenuLateralEditor/MenuLateralEditor';
import PanelNuevoProyecto from '../components/componentes-paneles/PanelNuevoProyecto/PanelNuevoProyecto';
import PanelEditorCodigo from '../components/componentes-paneles/PanelEditorCodigo/PanelEditorCodigo';
import PanelEditorProductos from '../components/componentes-paneles/PanelEditorProductos/PanelEditorProductos';
import PanelEditorLenguaje from '../components/componentes-paneles/PanelEditorLenguaje/PanelEditorLenguaje';
import PanelLateralPlantillas from '../components/componentes-paneleslaterales/PanelLateralEditorPlantillas/PanelLateralPlantillas';
import PanelEditorPlantillas from '../components/componentes-paneles/PanelEditorPlantillas/PanelEditorPlantillas';
import PanelNuevaPlantilla from '../components/componentes-paneles/PanelNuevaPlantilla/PanelNuevaPlantilla';
import PanelHome from '../components/componentes-paneles/PanelHome/PanelHome';
import Menu from '../components/componentes-menu/Menu/Menu';
import MenuEditor from '../components/componentes-menu/MenuEditor/MenuEditor';
import PanelLateralProductos from '../components/componentes-paneleslaterales/PanelLateralProductos/PanelLateralProductos';
import PanelLateralCompilados from '../components/componentes-paneleslaterales/PanelLateralCompilados/PanelLateralCompilados';
import PanelError from '../components/componentes-paneles/PanelError/PanelError';
import PanelEliminarPlantilla from '../components/componentes-paneles/PanelEliminarPlantilla/PanelEliminarPlantilla';
import PanelPreferenciasProyecto from '../components/componentes-paneles/PanelPreferenciasProyecto/PanelPreferenciasProyecto';
import PanelEliminarProyecto from '../components/componentes-paneles/PanelEliminarProyecto/PanelEliminarProyecto';
import PanelNuevoProducto from '../components/componentes-paneles/PanelNuevoProducto/PanelNuevoProducto';
import PanelEliminarProducto from '../components/componentes-paneles/PanelEliminarProducto/PanelEliminarProducto';
import PanelCompilado from '../components/componentes-paneles/PanelCompilado/PanelCompilado';
import PanelCompilando from '../components/componentes-paneles/PanelCompilando/PanelCompilando';
import PanelConfiguracion from '../components/componentes-paneles/PanelConfiguracion/PanelConfiguracion';
import PanelSubirPlantillas from '../components/componentes-paneles/PanelSubirPlantillas/PanelSubirPlantillas';
import PantallaLogin from '../components/componentes-login/PantallaLogin/PantallaLogin';
import LayoutPantallaCompleta from '../components/componentes-layout/LayoutPantallaCompleta/LayoutPantallaCompleta';
import { useUsuario } from './useUsuario';
import PanelRegistro from '../components/componentes-paneles/PanelRegistro/PanelRegistro';
const EnrutadorContext = React.createContext();

//Provider del contexto
function EnrutadorProvider(props){
    const [layout, setLayout] = useState(undefined)
    const [panel, setPanel] = useState(undefined)
    const [panelLateral, setPanelLateral] = useState(undefined)
    const [menu, setMenu] = useState(undefined)
    const [menuLateral, setMenuLateral] = useState(undefined)
    const [menuLateralSeleccionado, setMenuLateralSeleccionado] = useState(undefined)
    
    


    const value = useMemo(() => {

        return({
            layout,
            panel,
            panelLateral,
            menu,
            menuLateral,
            menuLateralSeleccionado,
            setLayout,
            setPanel,
            setPanelLateral,
            setMenu,
            setMenuLateral,
            setMenuLateralSeleccionado
        })
    }, [layout, panel, panelLateral, menu, menuLateral]);

    return <EnrutadorContext.Provider value={value} {...props}/>
}



//Se retorna el hook para el contexto
export function useEnrutador(){
    const contexto = React.useContext(EnrutadorContext);
    if(!contexto){
        throw new Error("useEnrutador debe estar dentor del proveedor EnrutadorContext")
    }
    return contexto;
}


function ReglasEnrutamiento(props){
    const {configuracionUsuario,usuario,login} = useUsuario();
    const {proyecto} = useProyecto();


    if(props.requiereProyecto && proyecto == null){
        //console.log("Se requiere proyecto")
        //const history = useHistory();
        //history.location("/")
    }

    
    if(props.requiereUsuario && usuario == null){
        return (  <Redirect  to="/login" />)
    }

    return(
        <> {props.children} </>
    )
}

function RutaCompuesta(props){

    const {layout, panel,panelLateral, menu,menuLateral, menuLateralSeleccionado, setLayout,setPanel,setPanelLateral,setMenu,setMenuLateral, setMenuLateralSeleccionado} = useEnrutador()

    useEffect(() => {
        
        if(typeof props.layout !== 'undefined'){
            setLayout(props.layout)
        }
    },[props.layout])

    useEffect(() => {
        
        if(typeof props.panel !== 'undefined'){
            setPanel(props.panel)
        } 
    },[props.layout])

    useEffect(() => {
        
        if(typeof props.panelLateral !== 'undefined'){
            setPanelLateral(props.panelLateral)
        }
    },[props.panelLateral])

    useEffect(() => {
        
        if(typeof props.panelLateral !== 'undefined'){
            setMenu(props.menu)
        }
    },[props.menu])

    useEffect(() => {
        
        if(typeof props.menuLateral !== 'undefined'){
            setMenuLateral(props.menuLateral)
        }
    },[props.menuLateral])
    

    useEffect(() => {
       
        if(typeof props.menuLateralSeleccionado !== 'undefined'){
            setMenuLateralSeleccionado(props.menuLateralSeleccionado)
        }
    },[props.menuLateralSeleccionado])

    return(
        
        <ReglasEnrutamiento requiereProyecto={props.requiereProyecto}  requiereUsuario={props.requiereUsuario}>
            <LayoutCreator></LayoutCreator>
        </ReglasEnrutamiento>
    )
}

function DefaultRoute(props){
    const {usuario} = useUsuario();

        if(usuario != null){
            return(
                <Route  path="/" >
                    <RutaCompuesta
                        layout={<LayoutGestion></LayoutGestion>} 
                        panel={<PanelHome></PanelHome>} 
                        panelLateral={null}
                        menu={<>MENU</>} 
                        menuLateral={<MenuLateralGestion></MenuLateralGestion>}
                        menuLateralSeleccionado="G-HOME"
                        requiereUsuario
                        >
                    </RutaCompuesta>
                </Route>
            )
        }
        return (
         <Route path="/">
            <RutaCompuesta
                layout={ <LayoutPantallaCompleta></LayoutPantallaCompleta>} 
                panel={<PantallaLogin></PantallaLogin>}
                >
            </RutaCompuesta>
        </Route>)
}



export function Enrutador (){
    
        
    
        return(
            <EnrutadorProvider>
                <Router>

                <Switch>


      

                    <Route exact path="/gestion/proyectos" >
                        <RutaCompuesta
                            layout={<LayoutGestion></LayoutGestion>} 
                            panel={<PanelProyectos/>} 
                            panelLateral={null} 
                            menu={<>MENU</>} 
                            menuLateral={<MenuLateralGestion></MenuLateralGestion>}
                            menuLateralSeleccionado="G-PROY"
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/gestion/proyectos/nuevo" >
                        <RutaCompuesta
                            layout={<LayoutGestion></LayoutGestion>} 
                            panel={<PanelNuevoProyecto/>} 
                            panelLateral={null} 
                            menu={<>MENU</>} 
                            menuLateral={<MenuLateralGestion></MenuLateralGestion>}
                            menuLateralSeleccionado="G-PROY"
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/gestion/configuracion" >
                        <RutaCompuesta
                            layout={<LayoutGestion></LayoutGestion>} 
                            panel={<PanelConfiguracion></PanelConfiguracion>} 
                            panelLateral={null} 
                            menu={<>MENU</>} 
                            menuLateral={<MenuLateralGestion></MenuLateralGestion>}
                            menuLateralSeleccionado="G-CONF"
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>
                    


                    <Route exact path="/editor" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelEditorLenguaje>PAN</PanelEditorLenguaje>} 
                            panelLateral={null} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-LENG"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

 





                    <Route exact path="/editor/productos/editar" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelEditorProductos></PanelEditorProductos>} 
                            panelLateral={<PanelLateralProductos></PanelLateralProductos>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-PROD"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/editor/productos/nueva" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelNuevoProducto nuevo={true}></PanelNuevoProducto>} 
                            panelLateral={<PanelLateralProductos></PanelLateralProductos>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-PROD"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/editor/productos/opciones" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelNuevoProducto nuevo={false}></PanelNuevoProducto>} 
                            panelLateral={<PanelLateralProductos></PanelLateralProductos>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-PROD"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>    

                    <Route exact path="/editor/productos/eliminar" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelEliminarProducto ></PanelEliminarProducto>} 
                            panelLateral={<PanelLateralProductos></PanelLateralProductos>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-PROD"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>







                    <Route exact path="/editor/plantillas/editar" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelEditorPlantillas></PanelEditorPlantillas>} 
                            panelLateral={<PanelLateralPlantillas></PanelLateralPlantillas>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-PLAN"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/editor/plantillas/nueva" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelNuevaPlantilla nuevo={true}></PanelNuevaPlantilla>} 
                            panelLateral={<PanelLateralPlantillas></PanelLateralPlantillas>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-PLAN"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>


                    
                    <Route exact path="/editor/plantillas/subir" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelSubirPlantillas nuevo={true}></PanelSubirPlantillas>} 
                            panelLateral={<PanelLateralPlantillas></PanelLateralPlantillas>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-PLAN"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/editor/plantillas/opciones" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelNuevaPlantilla nuevo={false}></PanelNuevaPlantilla>} 
                            panelLateral={<PanelLateralPlantillas></PanelLateralPlantillas>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-PLAN"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    
                    <Route exact path="/editor/plantillas/eliminar" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelEliminarPlantilla ></PanelEliminarPlantilla>} 
                            panelLateral={<PanelLateralPlantillas></PanelLateralPlantillas>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-PLAN"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>
                    






                    <Route exact path="/editor/compilando" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelCompilando destino="/editor/compilados"></PanelCompilando>} 
                            panelLateral={null} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-COMP"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/editor/compilados" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelCompilado vista="compilados"></PanelCompilado>} 
                            panelLateral={<PanelLateralCompilados></PanelLateralCompilados>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-COMP"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/editor/bugs" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelCompilado vista="bugs"></PanelCompilado>} 
                            panelLateral={<PanelLateralCompilados></PanelLateralCompilados>} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado="E-COMP"
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>


                    <Route exact path="/editor/proyecto/preferencias" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelPreferenciasProyecto ></PanelPreferenciasProyecto>} 
                            panelLateral={null} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado=""
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/editor/proyecto/eliminar" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelEliminarProyecto ></PanelEliminarProyecto>} 
                            panelLateral={null} 
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado=""
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>



                    <Route exact path="/editor/proyecto/documentacion" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelHome></PanelHome>} 
                            panelLateral={null}
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado=""
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/editor/proyecto/notas" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelHome></PanelHome>} 
                            panelLateral={null}
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado=""
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/editor/proyecto/acercade" >
                        <RutaCompuesta
                            layout={ <LayoutEditor></LayoutEditor>} 
                            panel={<PanelHome></PanelHome>} 
                            panelLateral={null}
                            menu={<MenuEditor></MenuEditor>} 
                            menuLateral={<MenuLateralEditor></MenuLateralEditor>}
                            menuLateralSeleccionado=""
                            requiereProyecto
                            requiereUsuario
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/login" >
                        <RutaCompuesta
                            layout={ <LayoutPantallaCompleta></LayoutPantallaCompleta>} 
                            panel={<PantallaLogin></PantallaLogin>}
                            >
                        </RutaCompuesta>
                    </Route>

                    <Route exact path="/registro" >
                        <RutaCompuesta
                            layout={ <LayoutPantallaCompleta></LayoutPantallaCompleta>} 
                            panel={<PanelRegistro></PanelRegistro>}
                            >
                        </RutaCompuesta>
                    </Route>
                    
                    <DefaultRoute></DefaultRoute>


                    </Switch>
                </Router>
            </EnrutadorProvider>
        )
}
