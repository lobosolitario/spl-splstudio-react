import React, {useState, useEffect, useMemo} from 'react'
import UtilidadLiterales from '../utils/UtilidadLiterales';
import { useCookies } from 'react-cookie';

const LiteralesContext = React.createContext();

//Provider del contexto
export function LiteralesProvider(props){
    const ul = new UtilidadLiterales();

    const [cookies, setCookie, removeCookie] = useCookies(['cookie-name']);
    let cookieIdioma = "ID500AFC1290D515CA06D25532ACB917BAF69C2813D9A43399F8F5C5E8A7254568"

    let cookie = cookies[cookieIdioma];
    if(typeof cookie === 'undefined'){
        setCookie(cookieIdioma, ul.getIdiomaDefecto())
        cookie =  ul.getIdiomaDefecto();
        
    }

    const [idioma, setStateIdioma] = useState(cookie);
    
    useEffect(() => {
        removeCookie(cookieIdioma)
        setCookie(cookieIdioma, idioma)
    }, [idioma])

    function getLiteral(literal, variables){
        var literal = ul.getLiteral(literal, idioma)
        if(variables){
            literal = reemplazaLiterales(literal, variables)
        }
        return literal;
    }

    function reemplazaLiterales(literal, variables){
        const keys = Object.keys(variables);
        for(var i = 0; i <  keys.length; i++){
            let key = keys[i];
            literal = literal.replace('{'+ key +'}', variables[key])
        }
        return literal;
    }

    function setIdioma(idioma){
        setStateIdioma(idioma)
    }

    function getIdiomas(){
        return ul.getIdiomas();
    }

    const value = useMemo(() => {
        return({
            getLiteral,
            idioma,
            setIdioma,
            getIdiomas
        })
    }, [idioma]);

    return <LiteralesContext.Provider value={value} {...props}/>
}

//Se retorna el hook para el contexto
export function useLiterales(){
    const contexto = React.useContext(LiteralesContext);
    if(!contexto){
        throw new Error("useLiterales debe estar dentor del proveedor LiteralesContext")
    }
    return contexto;
}

