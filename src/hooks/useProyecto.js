import React, {useState, useEffect, useMemo} from 'react'
import ServiceSelector from '../services/ServiceSelector';
import SelectorColores from '../utils/SelectorColores';
import {useApi} from '../hooks/useApi';
import Proyecto from '../model/Proyecto/Proyecto';
import Lenguaje from '../model/Proyecto/Lenguaje';
import ProyectoConversor from '../model/Almacenamiento/ProyectoConverosr';
import Inicializador from '../utils/Inicializador';
import Plantilla from '../model/Proyecto/Plantilla';
import Compilado from '../model/Proyecto/Compilado';



const ProyectoContext = React.createContext();

//Provider del contexto
export function ProyectoProvider(props){
    

    const proyectoConversor = new ProyectoConversor();
    const inicializador = new Inicializador();

    const {cargarElementos : cargarProyectoApi, crearOActualizarProyecto, listarProyectos : listarProyectosApi, compilarProyecto, crearUrlDescarga, eliminarProyecto: eliminarProyectoApi} = useApi();
     
    //Errores
    //null : Sin errores
    //
    const [error, setError] = useState(null);


    //Proyecto especifico
    const [proyectoSeleccionado, setStateProyectoSeleccionado] = useState(null);
    const [proyectoModificado, setStateProyectoModificado] = useState(0); //0:Guardado, 1:Guardando, 2:Modificado, 3:Error de guardado
    const [proyecto, setStateProyecto] = useState(null);
    const [listaProyectos, setListaProyectos] = useState(null);


    const [seleccionIdProducto, setseleccionIdProducto] = useState(null);
    const [seleccionProducto, setseleccionProducto] = useState(null);
    const [seleccionIdPlantilla, setseleccionIdPlantilla] = useState(null);
    const [seleccionPlantilla, setseleccionPlantilla] = useState(null);

    const [compilacion, setCompilacion] = useState(null);
    const [seleccionCompilado, setSeleccionCompilado] = useState(null);

    useEffect(() => {
        if(proyecto != null){
            setseleccionPlantilla(proyecto.getPlantilla(seleccionIdPlantilla))
        }
    }, [seleccionIdPlantilla])

    useEffect(() => {
        if(proyecto != null){
            setseleccionProducto(proyecto.getProducto(seleccionIdProducto))
        }
    }, [seleccionIdProducto])



    useEffect(() => {
        
        setCompilacion(null)
        
    }, [proyectoModificado, seleccionPlantilla , seleccionIdProducto, proyectoSeleccionado])


    useEffect(() => {
        if(proyecto == null){
            setseleccionProducto(null)
            setseleccionIdProducto(null)
            setseleccionIdPlantilla(null)
            setseleccionPlantilla(null)
            setStateProyectoSeleccionado(null)
            setStateProyectoModificado(0)
            setCompilacion(null)
        }
    }, [proyecto])



    useEffect(() => {
        if(proyectoSeleccionado != null){
            cargarProyectoApi(
                function(elementos){
                    //Se obtiene el proyecto seleccinado
                    let psel = listaProyectos.filter(proyecto => proyecto.id == proyectoSeleccionado)[0]
                    if(psel){
                        var proyectoCargado = proyectoConversor.setInfo(psel, elementos);
                        setStateProyecto(proyectoCargado);
                    }
                },
                function(error){
                    console.log(error)
                    setStateProyecto(null)
                },
                proyectoSeleccionado
            ); 
        }
 
    }, [proyectoSeleccionado]);


    /*************************************************************************************************************************/
    /*                                                  PROYECTOS                                                            */
    /*************************************************************************************************************************/
    /*
    * Se asigna como proyecto seleccionado pero no cargado aun
    */
    const listarProyectos = () => {
        listarProyectosApi( 
        function(data){
            setListaProyectos(data)
        },
        function(){
        } )
    }

    /*
    * Se asigna como proyecto seleccionado pero no cargado aun
    */
    const cargaProyecto = (id) => {
        setStateProyectoSeleccionado(id)
    }
    
    /**
     * Crea un nuevo proyecto y lo selecciona
     */
    const crearProyecto = (nombre) => {
        //Inicializar el proyecto
        
        let proy = inicializador.inicializarProyecto(nombre)
        
        let dataProyecto = proyectoConversor.getInfo(proy);
        //Una vez finalizado seleccionar el proyecto
        crearOActualizarProyecto(
            function(data){
               console.log(data.id)
               listaProyectos.push(data)
               setStateProyectoSeleccionado(data.id)
            },
            function(error){
                setStateProyecto(null)
            },
            dataProyecto
        );
    }
    
    
    /**
     * Guarda las modificaciones del proyecto de forma local
     */
    const modificaProyecto = (i_proyecto) => {
        var newp = new Proyecto(i_proyecto);
        setStateProyecto(newp);
        setStateProyectoModificado(2);
    }

    /**
     * Realiza un guardado del proyecto en el servidor
     */
    const guardaProyecto = () => {
        var dataProyecto = proyectoConversor.getInfo(proyecto);
        //Una vez finalizado seleccionar el proyecto
        setStateProyectoModificado(1); //Estado guardando
        crearOActualizarProyecto(
            function(data){
                if(data){
                    //Se ha guardado correctamente
                    setStateProyectoModificado(0);//Estado guardado
                }else{
                    //No se ha guardado
                    setStateProyectoModificado(3); //Estado error de guardado
                }                
            },
            function(error){
                //No se ha guardado
                setStateProyectoModificado(3); //Estado error de guardado
            },
            dataProyecto
        );
    }

    
    /**
     * Cierra el proyecto
     */
     const cerrarProyecto = () => {
        //Desde el useEffect de proyeto se coloca el resto de variables a null
        setStateProyecto(null);
    }


    
    const eliminarProyecto = (callbackOk, callbackNok) => {
        setStateProyecto(null)
        setStateProyectoSeleccionado(null)
        eliminarProyectoApi(
            function(data){
                console.log("Proyecto eliminado")

                setListaProyectos(null)
            },
            function(error){
                console.log("Error")
            },
            proyecto.id
        )

    }




    /*************************************************************************************************************************/
    /*                                                   LENGUAJE                                                            */
    /*************************************************************************************************************************/


    /**
     * Actualiza el lenguaje
     */
    const updateLenguaje = (lenguaje) => {
        var newp = new Proyecto(proyecto);
        newp.lenguaje = lenguaje
        console.log(newp)
        setStateProyecto(newp);
        setStateProyectoModificado(2);

        //setCambio(!cambio)
    }

    
    /*************************************************************************************************************************/
    /*                                                  PLANTILLAS                                                           */
    /*************************************************************************************************************************/

    /**
     * Crea o actualiza una plantilla
     */
     const updatePlantilla = (plantilla) => {
        plantilla.modificado = true;
        setStateProyectoModificado(2);
    }

    const seleccionarPlantilla = (id) => {
        setseleccionIdPlantilla(id);
    }

    const crearPlantilla = (nombre, ancla, grupo, code) => {

        //Se crea una uneva plantilla
        let nuevaPlantilla = inicializador.inicializarPlantilla(proyecto, nombre, ancla, grupo, code);
        
        //Se crea una nueva instancia del proyecto
        var newp = new Proyecto(proyecto);
        newp.plantillas.push(nuevaPlantilla)

        //Se actualiza el proyecto
        setStateProyecto(newp);
        setStateProyectoModificado(2);

        return nuevaPlantilla;
    }

    const eliminarPlantilla = (plantilla) => {
        //Se crea una nueva instancia del proyecto
        var newp = new Proyecto(proyecto);
        newp.operaciones().eliminarPlantilla(plantilla)

        //Se actualiza el proyecto
        setStateProyecto(newp);
        setStateProyectoModificado(2);

        setseleccionIdPlantilla(null)
        setseleccionPlantilla(null)
    }

    /*************************************************************************************************************************/
    /*                                                  PRODUCTOS                                                            */
    /*************************************************************************************************************************/
    /**
     * Crea o actualiza un producto
     */
     const updateProducto = (producto) => {
        producto.modificado = true;
        setStateProyectoModificado(2);
    }


    const seleccionarProducto = (id) => {
        setseleccionIdProducto(id);
    }



    const crearProducto = (nombre,  grupo) => {
        //Se crea una uneva plantilla
        let nuevoProducto = inicializador.inicializarProducto(proyecto, nombre, grupo);

        //Se crea una nueva instancia del proyecto
        var newp = new Proyecto(proyecto);
        newp.productos.push(nuevoProducto)

        //Se actualiza el proyecto
        setStateProyecto(newp);
        setStateProyectoModificado(2);

        return nuevoProducto;
    }


    const eliminarProducto = (producto) => {
        //Se crea una nueva instancia del proyecto
        var newp = new Proyecto(proyecto);
        newp.operaciones().eliminarProducto(producto)

        //Se actualiza el proyecto
        setStateProyecto(newp);
        setStateProyectoModificado(2);

        setseleccionIdProducto(null)
        setseleccionProducto(null)
    }
    


    /*************************************************************************************************************************/
    /*                                                COMPILACION                                                            */
    /*************************************************************************************************************************/
    
    const compilar = () => {
        console.log("Compilndo")
        if(proyecto != null && seleccionPlantilla != null && seleccionProducto != null){
            compilarProyecto(
                function(data){
                    let comp = new Compilado();
                    comp.setData(data)
                    setCompilacion(comp);
                },
                function(error){
                    console.log(error)
                },
                proyecto.id,
                seleccionProducto.id,
                seleccionPlantilla.grupo
                
            )
        }
    }


    const elminarCompilacion = () => {
        setCompilacion(null)
    }


    const seleccionarCompilado = (id) => {
        setSeleccionCompilado(id);
    }

    const getURLDescarga = () => {
        let url = ""
        if(proyecto != null && seleccionPlantilla != null && seleccionProducto != null){
            url = crearUrlDescarga(
                proyecto.id,
                seleccionProducto.id,
                seleccionPlantilla.grupo
            )
        }
        return url;
    }



    /*************************************************************************************************************************/

    const value = useMemo(() => {
        return({
            proyecto,
            listaProyectos,
            seleccionProducto,
            seleccionPlantilla,
            seleccionCompilado,
            proyectoModificado,
            compilacion,
            cargaProyecto,
            modificaProyecto,
            guardaProyecto,
            updateLenguaje,
            updatePlantilla,
            updateProducto,
            listarProyectos,
            crearProyecto,
            cerrarProyecto,
            seleccionarProducto,
            seleccionarPlantilla,
            crearPlantilla,
            crearProducto,
            eliminarPlantilla,
            eliminarProducto,
            compilar,
            elminarCompilacion,
            seleccionarCompilado,
            eliminarProyecto,
            getURLDescarga
        })
    }, [proyecto, 
        proyectoSeleccionado, 
        listaProyectos, 
        seleccionProducto, 
        seleccionPlantilla, 
        proyectoModificado, 
        compilacion, 
        seleccionCompilado]);

    return <ProyectoContext.Provider value={value} {...props}/>
}

//Se retorna el hook para el contexto
export function useProyecto(){
    const contexto = React.useContext(ProyectoContext);
    if(!contexto){
        throw new Error("useProyecto debe estar dentor del proveedor ProyectoContext")
    }
    return contexto;
}
