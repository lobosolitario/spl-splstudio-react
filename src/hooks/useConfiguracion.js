import React, {useState, useEffect, useMemo} from 'react'
import ServiceSelector from '../services/ServiceSelector';
import SelectorColores from '../utils/SelectorColores';

const ProyectoContext = React.createContext();

//Provider del contexto
export function ProyectoProvider(props){


    //Proyecto especifico
    const [proyectoSeleccionado, setStateProyectoSeleccionado] = useState(null);
    const [proyectoModificado, setStateProyectoModificado] = useState(false);
    const [proyecto, setStateProyecto] = useState(null);




    /*
    * Se asigna como proyecto seleccionado pero no cargado aun
    */
    const cargaProyecto = (id) => {
        setStateProyectoSeleccionado(id)
    }
    

    useEffect(() => {
        var ss = new ServiceSelector();
        if(!props.proyectoCargado){
            ss.cargarProyecto( 
                function (data) {
                    console.log("Carga correcta")
                    setStateProyecto(data);
                },
                function (err) {
                    console.log("Carga incorrecta")
                })
        }
    }, [proyectoSeleccionado]);


    /**
     * Indica si el proyecto se encuentra modificado
     */
    const esProyectoModificado = () => {
        
    }
    
    
    /**
     * Guarda las modificaciones del proyecto de forma local
     */
    const modificaProyecto = (i_proyecto) => {
        setStateProyecto(proyecto);
        setStateProyectoModificado(true);
    }

    /**
     * Realiza un guardado del proyecto en el servidor
     */
    const guardaProyecto = () => {
        setStateProyectoModificado(false);
    }


    const value = useMemo(() => {
        return({
            proyecto,
            cargaProyecto,
            esProyectoModificado,
            modificaProyecto,
            guardaProyecto
        })
    }, [proyecto, proyectoSeleccionado]);

    return <ProyectoContext.Provider value={value} {...props}/>
}

//Se retorna el hook para el contexto
export function useProyecto(){
    const contexto = React.useContext(ProyectoContext);
    if(!contexto){
        throw new Error("useProyecto debe estar dentor del proveedor ProyectoContext")
    }
    return contexto;
}
