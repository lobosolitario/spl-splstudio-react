import React, {useState, useEffect, useMemo} from 'react'
import SelectorColores from '../utils/SelectorColores';
import { useCookies } from 'react-cookie';

const TemaContext = React.createContext();

//Provider del contexto
export function TemaProvider(props){
    const sc = new SelectorColores();

    const [cookies, setCookie, removeCookie] = useCookies(['cookie-name']);
    let cookieTema = "TM45180DABDF129F8EA1643B5E1F79DFE2A3899974D6CC141F6E2E231AAC4645"

    let cookie = cookies[cookieTema];
    if(typeof cookie === 'undefined'){
        setCookie(cookieTema, sc.getTemaDefecto())
        cookie =  sc.getTemaDefecto();
    }

    const [tema, setStateTema] = useState(cookie);

    useEffect(() => {
        removeCookie(cookieTema)
        setCookie(cookieTema, tema)
    }, [tema])

    function getColor(color){
        return sc.getColor(color, tema)
    }

    function setTema(tema){
        setStateTema(tema)
    }

    const temas = sc.getTemas()
    

    const value = useMemo(() => {
        return({
            getColor,
            tema,
            setTema,
            temas
        })
    }, [tema]);

    return <TemaContext.Provider value={value} {...props}/>
}

//Se retorna el hook para el contexto
export function useTema(){
    const contexto = React.useContext(TemaContext);
    if(!contexto){
        throw new Error("useTema debe estar dentor del proveedor TemaContext")
    }
    return contexto;
}


function getColores(tema){

}