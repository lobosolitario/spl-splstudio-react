import React, {useState, useEffect, useMemo} from 'react'
import { getVariableEntorno } from '../config/ENTORNO';

const ApiContext = React.createContext();

//Provider del contexto
export function ApiProvider(props){


    const listarProyectos = (callbackOk, callbackNok) => {
        const url = getVariableEntorno("backendhost") + ":" + getVariableEntorno("getwayport") + "/api/proyectos/listar"
        console.log( url)
        fetch(url)
        .then(res => res.json())
        .then(data => {
            callbackOk(data);
        }).catch(e => {
            callbackNok(e);
        });
    }

    const verProyecto = (callbackOk, callbackNok, id) => {
        const url = getVariableEntorno("backendhost") + ":" + getVariableEntorno("getwayport") + "/api/proyectos/ver/" + id
        console.log( url)
        fetch(url)
        .then(res => res.json())
        .then(data => {
            callbackOk(data);
        }).catch(function(e) {
            callbackNok(e);
        });
    }

    /*
    const nuevoProyecto = (callbackOk, callbackNok, nombre) => {
        fetch(conexion + "proyectos/crear/" + nombre)
        .then(res => res.json())
        .then(data => {
            callbackOk(data); 
        }).catch(function(e) {
            callbackNok(e);
        });
    }
    */

    const cargarElementos = (callbackOk, callbackNok, id) => {
        const url = getVariableEntorno("backendhost") + ":" + getVariableEntorno("getwayport") + "/api/elementos/listar/" + id
        console.log( url)
        fetch(url)
        .then(res => res.json())
        .then(data => {
            callbackOk(data);
        }).catch(function(e) {
            callbackNok(e);
        });
    }



    const crearOActualizarProyecto = (callbackOk, callbackNok, dataProyecto) => {
        const url = getVariableEntorno("backendhost") + ":" + getVariableEntorno("getwayport") + "/api/proyectos/actualizar"
        console.log( url)
        fetch(url , {
        method: 'POST',
        headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
        body: JSON.stringify({
                proyecto: dataProyecto
            })
        })
        .then(res => res.json())
        .then(data => {
            //alert("Proyecto creado")
            callbackOk(data);
        }).catch(function(e) {
            //TODO: Hacer que se produzca un error en la vista cuando la api falla
            console.log(e)
            //alert("Proyecto no creado")
            callbackNok(e);
        });
    }


    

    /*    
    const compilarProyecto = (callbackOk, callbackNok, idProyecto, grupoProducto, grupoPlantilla) => {
        console.log("Cargando elementos")
        fetch(conexion + "3101/compila/v1/", {
            mode: 'no-cors',
            method: 'POST',
            headers: new Headers({
                        'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
                }),
            body: JSON.stringify({
                    idProyecto: idProyecto,
                    grupoProducto : grupoProducto,
                    grupoPlantilla : grupoPlantilla
                })
            })
            .then(res => res.json())
            .then(data => {
                //alert("Proyecto creado")
                callbackOk(data);
            }).catch(function(e) {
                console.log(e)
                //alert("Proyecto no creado")
                callbackNok(e);
            });
    }
    */


    const compilarProyecto = (callbackOk, callbackNok, idProyecto, idProducto, grupoPlantilla) => {
        const url = getVariableEntorno("backendhost") + ":" + getVariableEntorno("compilerport") + "/compila/v1?idProyecto=" + idProyecto + "&idProducto=" + idProducto + "&grupoPlantilla=" + grupoPlantilla
        console.log(url)
        fetch(url)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            callbackOk(data);
        }).catch(function(e) {
            console.log(e)
            callbackNok(e);
        });
    }

    const crearUrlDescarga = (idProyecto, idProducto, grupoPlantilla) => {
        const url = getVariableEntorno("backendhost") + ":" + getVariableEntorno("compilerport") + "/descarga/v1?idProyecto=" + idProyecto + "&idProducto=" + idProducto + "&grupoPlantilla=" + grupoPlantilla
        return url;
    }
    

    const value = useMemo(() => {
        return({
            listarProyectos,
            verProyecto,
            cargarElementos,
            crearOActualizarProyecto,
            compilarProyecto,
            crearUrlDescarga
        })
    });

    return <ApiContext.Provider value={value} {...props}/>
}

//Se retorna el hook para el contexto
export function useApi(){
    const contexto = React.useContext(ApiContext);
    if(!contexto){
        throw new Error("useApi debe estar dentor del proveedor ApiContext")
    }
    return contexto;
}



