import React, {useState, useEffect, useMemo} from 'react'
import { useHistory } from 'react-router';
import SelectorColores from '../utils/SelectorColores';

const UsuarioContext = React.createContext();

//Provider del contexto
export function UsuarioProvider(props){
    const history = useHistory();
    const [configuracionUsuario, setConfiguracionUsuario] = useState(
        {   
            //Configuracion por defecto
            idioma : "es",
            tema : "drk"
        }
    ) 

    const [usuario, setUsuario] = useState(null);

    const login = (user, pass) => {
        alert(user + "/" + pass)
        setUsuario(1);
    }

    const value = useMemo(() => {
        return({
            configuracionUsuario,
            usuario,
            login
        })
    }, [configuracionUsuario, usuario]);

    return <UsuarioContext.Provider value={value} {...props}/>
}

//Se retorna el hook para el contexto
export function useUsuario(){
    const contexto = React.useContext(UsuarioContext);
    if(!contexto){
        throw new Error("useUsuario debe estar dentor del proveedor UsuarioContext")
    }
    return contexto;
}

