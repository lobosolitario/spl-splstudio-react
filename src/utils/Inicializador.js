import Lenguaje from "../model/Proyecto/Lenguaje";
import Plantilla from "../model/Proyecto/Plantilla";
import Producto from "../model/Proyecto/Producto";
import Proyecto from "../model/Proyecto/Proyecto";

const Inicializador = function(){
    var self = this;

    //-------------------------------------------- PROYECTO --------------------------------------------

    this.inicializarProyecto = function(nombre){
        var proyecto = new Proyecto();
        proyecto.nombre = nombre;
        proyecto.lenguaje = self.inicializarLenguaje();
        return proyecto;
    }

    //-------------------------------------------- LENGUAJE --------------------------------------------

    this.inicializarLenguaje = function(){
        var lenguaje = new Lenguaje();
        return lenguaje;
    }

    //-------------------------------------------- PLANTILLA --------------------------------------------


    this.inicializarPlantilla = function(proyecto, nombre, ancla, grupo, code){
        var plantilla = new Plantilla();

        if(code){
            plantilla.code = code;
        }else{
            plantilla.code = getCodPlantilla(proyecto)
        }

        if(nombre){
            plantilla.nombre = nombre;
        }else{
            plantilla.nombre = "Nueva plantilla";
        }

        if(ancla){
            plantilla.ancla = ancla;
        }else{
            plantilla.ancla = "root";
        }

        if(grupo){
            plantilla.grupo = grupo;
        }else{
            plantilla.grupo = "";
        }

        return plantilla;
    }

    function getCodPlantilla(proyecto){
        let code = "";
        code = code + "<%\n"
        code = code + "  $cfg.path = \"/\";\n"
        code = code + "  $cfg.name = \"nombrePlantilla\";\n"
        code = code + "%>\n"
        code = code + "New Template\n"
        //code = code + "<%=$self.values[0]%>\n"
        return code;
    }


    //-------------------------------------------- PRODUCTO --------------------------------------------

    this.inicializarProducto= function(proyecto, nombre, grupo){
        var producto = new Producto();
        producto.code = getCodeProducto(proyecto);

        if(nombre){
            producto.nombre = nombre;
        }else{
            producto.nombre = "Nuevp producto";
        }


        if(grupo){
            producto.grupo = grupo;
        }else{
            producto.grupo = "";
        }

        return producto;
    }

    function getCodeProducto(proyecto){
        let nodoRoot = proyecto.lenguaje.nodo;
        return getCodeProductoR(proyecto.lenguaje, nodoRoot, 0);
    }

    function getCodeProductoR(lenguaje, nodo, nivel){

        //Validaciones
        if(lenguaje == null || nodo == null){
            return "";
        }


        //Se calcula el indexado
        let code = "";
        let caracterIndexado = "    ";
        let indexado = "";
        for(var i = 0; i < nivel; i++){
            indexado = indexado + caracterIndexado;
        }

        if(lenguaje.operaciones().isNodoRecursivo(nodo)){
            return indexado + "//Recursion\n";
        }


        if(nodo.hijos.length > 0){
            code = code + indexado + nodo.nombre + "(\n";
            for(var i = 0; i < nodo.hijos.length; i++){

                code = code + getCodeProductoR(lenguaje, nodo.hijos[i], nivel + 1)
                if(i <  nodo.hijos.length - 1){
                    code = code + ",\n"
                }else{
                    code = code + "\n"
                }
            }
            code = code + indexado  + ")";
        }else{
            code = code + indexado + nodo.nombre + "(\"" + nodo.defecto + "\")";
        }

        return code;
    }
}
export default Inicializador;