import LITERALES from "../config/LITERALES.js"
export default class UtilidadLiterales {
    getLiteral(literal, idioma) {
      if(LITERALES[literal] != undefined && LITERALES[literal][idioma] != undefined){
          return  LITERALES[literal][idioma] 
      }

      return "";
    }


    getIdiomas(){
      var idiomas = [
          
          {
              nombre: "English",
              valor: "en"
          },
          {
            nombre: "Spanish",
            valor: "es"
          },
      ]
      return idiomas;
    }

    getIdiomaDefecto(){
      return "es";
    }
}