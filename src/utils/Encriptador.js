const Encriptador = function(){

    this.encriptar = function(decoded){
        console.log("ENCRIPTANDO")
        //console.log(decoded)
        if(typeof decoded === 'undefined' || decoded == null){
            decoded = "";
        }
        const buff = Buffer.from(String(decoded), 'utf-8');
        const b64 = buff.toString('base64')
        //console.log(b64)

        const uriEncoded = encodeURIComponent(b64)
        //console.log(uriEncoded)

        return uriEncoded;
    }

    this.desencriptar = function(encoded){
        console.log("DESENCRIPTANDO")
        //console.log(encoded)
        if(typeof encoded === 'undefined' || encoded == null){
            encoded = "";
        }
        //console.log(encoded)

        const uriDencoded = decodeURIComponent(encoded)
        //console.log(uriDencoded)

        const buff = Buffer.from(String(uriDencoded), 'base64');
        return buff.toString('utf-8');
    }
}

export default Encriptador