import TEMA from "../config/TEMA.js"
export default class SelectorColores {
    getColor(color, tema) {
      if(TEMA[color] != undefined && TEMA[color][tema] != undefined){
          return  TEMA[color][tema]
      }
      console.warn("SPL: No se ha encontrado el color " + color + " para el tema " + tema)
      return "none";
    }


    getTemas(){
      var temas = [
        {
            nombre: "Tema 1",
            valor: "drk"
        },
        {
            nombre: "Tema 2",
            valor: "lth"
        },
        {
            nombre: "Tema 3",
            valor: "rdt"
        },
        /*
        {
          nombre: "Tema 4",
          valor: "wht"
        }
        */
      ]
      return temas;
    }

    getTemaDefecto(){
      return "drk";
    }
}