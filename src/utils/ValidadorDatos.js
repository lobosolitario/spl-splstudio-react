const Validador = function(){
    var self = this;
    var validaciones = [];

    var errores = [];
    
    const listaSoloCaracteres =["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    const listaNumeros =["0","1","2","3","4","5","6","7","8","9"] ;
    const listaAlfanumerica =["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"] 


    /**
     * Valida que no sea vacio
     */
    this.validaNoVacio = function(){
        validaciones.push(function(dato){
            if(!isVacioNulo(dato)){
                return null;
            }
            else{
                return 1;
            }
        });
    }

    /**
     * Valida que este contenido en el array
     * @param {Array de valores para comparar} array 
     */
    this.validaContenidoEnArray = function(array){
        validaciones.push(function(dato){
            if(array.some(v => (v == dato) )){
                return null;
            }
            else{
                return 2;
            }
        });
    }

    /**
     * Valida que no este contenido en el array
     * @param {Array de valores para comparar} array 
     */
    this.validaNoContenidoEnArray = function(array){
        validaciones.push(function(dato){
            if(!array.some(v => (v == dato) )){
                return null;
            }
            else{
                return 3;
            }
        });
    }

    /**
     * Valida que el texto sea exacto
     * @param {Texto a comparar} texto 
     */
    this.validaTextoExacto = function(texto){
        validaciones.push(function(dato){
            if(dato == texto){
                return null;
            }
            else{
                return 4;
            }
        });
    }

    /**
     * Valida que no comience con numeros
     */
    this.validaNoComenzarConNumeros = function(){
       

        validaciones.push(function(dato){

            //Si el primer caracter es un numero
            if(dato.length > 0 && listaNumeros.some(v => (v == dato.charAt(0)) )){
                return 5;
            }
            return null;
            
        });
    }

    /**
     * Valida caracteres alfanumericos
     */
     this.validaAlfanumerico = function(){
        validaciones.push(function(dato){
            for (let i = 0; i < dato.length; i++) {
                //Si algun caracter no se encuentra en el array devuelve el error
                if(!listaAlfanumerica.some(v => (v == dato.charAt(i).toLowerCase()) )){
                    return 6;
                }
            }
            return null;
        });
    }


     /**
     * Valida que tenga un maximo de caracteres
     */
    this.validaMaximoCaracteres = function(max){

        validaciones.push(function(dato){
            if(dato.length > max){
                return 7
            }
            return null;
        });
    }

    
    /**
    * Valida que tenga un minimo de caracteres
    */
      this.validaMinimoCaracteres = function(min){

        validaciones.push(function(dato){
            if(dato.length < min){
                return 8
            }
            return null;
        });
    }
    

    /**
    * Valida que tenga un numero exacto de caracteres
    */
     this.validaNumeroCaracteres = function(n){

        validaciones.push(function(dato){
            if(dato.length != n){
                return 9
            }
            return null;
        });
    }


    /**
    * Valida que no contenga caracteres en mayuscula
    */
    this.validaSinMayusculas = function(){
        validaciones.push(function(dato){
            for (let i = 0; i < dato.length; i++) {
                //Si algun caracter no se encuentra en el array devuelve el error
                if(listaSoloCaracteres.some(v => (v.toUpperCase() == dato.charAt(i)) )){
                    return 10;
                }
            }
            return null;
        });
    }



    
    /**
     * Valida caracteres alfanumericos mas los caracteres extra recibidos por parametro
     */
     this.validaAlfanumericoConExtra= function(extra){
        validaciones.push(function(dato){
            for (let i = 0; i < listaAlfanumerica.length; i++) {
                extra.push(listaAlfanumerica[i])
            }

            for (let i = 0; i < dato.length; i++) {
                //Si algun caracter no se encuentra en el array devuelve el error
                if(!extra.some(v => (v == dato.charAt(i).toLowerCase()) )){
                    return 11;
                }
            }
            return null;
        });
    }



/**------------------------------------------------------------------------------------------------------ */
    this.validar = function(dato){
        errores = []
        var validado = true;
        validaciones.forEach(validacion => {
            var error = validacion(dato);
            if(error != null){
                validado = false;
                errores.push(error)
            }
            
        });
        return validado;
    }

    this.getErrores = function(){
        return errores;
    }


    function isVacioNulo(dato){
        if(dato == null){
            return true;
        }
        if(dato == ""){
            return true;
        }
        return false;
    }
}

export default Validador;
