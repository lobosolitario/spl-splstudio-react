import React from 'react'
import {Route, BrowserRouter as Router } from 'react-router-dom'

import App from './App'

const routes = () => (
    <Router basename={ProcessingInstruction.env.PUBLIC_URL}>
        <div>
            <Route exact path="/" component={App}></Route>
        </div>
    </Router>

);